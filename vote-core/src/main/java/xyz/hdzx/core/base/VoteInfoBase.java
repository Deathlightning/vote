package xyz.hdzx.core.base;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author whq Date: 2018-09-29 Time: 21:18
 */

@Getter
@Setter
@NoArgsConstructor
public class VoteInfoBase extends VoteInfoCommon {

    /**
     * 背景图片url
     */
    private String bgUrl;

    /**
     * 简介
     */
    private String brief;

    /**
     * 说明
     */
    private String content;

    /**
     * 互动模块名
     */
    @JSONField(name = "interactiveModuleId")
    private Integer moduleId;

    /**
     * 互动模块名
     */
    @JSONField(name = "interactiveModuleName")
    private String moduleName;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 投票状态
     */
    private String status;

    /**
     * 类型
     */
    private String type;
}
