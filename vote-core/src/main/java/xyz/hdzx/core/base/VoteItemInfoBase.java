package xyz.hdzx.core.base;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author whq Date: 2018-09-29 Time: 21:37
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class VoteItemInfoBase {

    /**
     * 用户id
     */
    private String id;

    /**
     * 用户头像url
     */
    private String photoUrl;

    /**
     * 用户名
     */
    private String name;

    /**
     * 简介
     */
    private String brief;


}
