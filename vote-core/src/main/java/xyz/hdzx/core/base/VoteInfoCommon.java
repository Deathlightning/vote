package xyz.hdzx.core.base;

import com.alibaba.fastjson.annotation.JSONField;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/** @author whq Date: 2018-10-06 Time: 10:58 */
@Setter
@Getter
public class VoteInfoCommon {

  /** 投票id */
  private String id;

  /** 投票标题 */
  private String title;

  /** 投票开始时间 */
  private Date startTime;

  /** 投票结束时间 */
  private Date endTime;

  /** 投票创建时间 */
  private Date createTime;

  /** 投票更新时间 */
  private Date updateTime;
}
