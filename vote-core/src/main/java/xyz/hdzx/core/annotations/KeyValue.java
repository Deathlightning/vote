package xyz.hdzx.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author whq Date: 2018-10-06 Time: 15:00
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface KeyValue {

    /**
     * 对应的属性
     *
     * @return return
     */
    String key() default "";

    /**
     * 对于属性的限制值, 和type配合使用
     *
     * @return return
     */
    String toValue() default "";

    /**
     * 比较条件
     *
     * @return return
     */
    Type type() default Type.DEFAULT;

    enum Type {
        /**
         * 等于
         */
        EQ,
        /**
         * 大于
         */
        GT,
        /**
         * 小于
         */
        LT,
        /**
         * 大于等于
         */
        GE,
        /**
         * 小于等于
         */
        LE,
        /**
         * 不为空和null
         */
        NOT_EMPTY,
        /**
         * 默认空字符串或者null或者基本类型默认值
         */
        DEFAULT
    }
}
