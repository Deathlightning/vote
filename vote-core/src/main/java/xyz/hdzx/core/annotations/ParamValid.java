package xyz.hdzx.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 校验参数
 *
 * @author whq Date: 2018-10-06 Time: 14:47
 */
@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ParamValid {

    /**
     * 不需要校验的属性名，不区分大小写
     *
     * @return return
     */
    String[] notValidParam() default {"pageSize", "currentPage", "orderByClause", "count", "tmp"};

    /**
     * 错误消息，如果校验失败返回的错误消息
     *
     * @return return
     */
    String message() default "参数为空";

    /**
     * 对属性的限制，默认不限制
     *
     * @return return
     */
    KeyValue[] value() default @KeyValue();

}
