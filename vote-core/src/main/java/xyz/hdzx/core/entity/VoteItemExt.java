package xyz.hdzx.core.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.hdzx.core.model.VoteItem;

/**
 * @author whq
 */
@Setter
@Getter
@ToString
public class VoteItemExt extends VoteItem {

  /**
   * pk的状态
   */
  private int pkStatus;

  /**
   * 描述
   */
  private String desc;

  /**
   * 默认组
   */
  private boolean defaultGroup;
}
