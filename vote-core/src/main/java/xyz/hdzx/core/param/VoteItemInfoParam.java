package xyz.hdzx.core.param;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.hdzx.core.base.VoteItemInfoBase;

/**
 * @author whq Date: 2018-10-06 Time: 10:17
 */
@Getter
@Setter
@ToString
public class VoteItemInfoParam extends VoteItemInfoBase {

    private String uid;
}
