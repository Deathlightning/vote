package xyz.hdzx.core.param;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.hdzx.core.bean.PageHelper;

/**
 * @author whq Date: 2018-10-09 Time: 14:05
 */
@Setter
@Getter
@ToString
public class SelectVoteParam extends PageHelper {

    /**
     * 当前栏目id
     */
    private Integer programId;

    /**
     * 当前频率id
     */
    private Integer frequencyId;

    /**
     * 当前模块id
     */
    private Integer moduleId;

    /**
     * 根据投票开始时间查询
     */
    private Date startTime;

    /**
     * 根据投票结束时间查询
     */
    private Date endTime;

    /**
     * 根据投票名称查询
     */
    private String name;

    /**
     * 根据状态查询
     */
    private String status;
}
