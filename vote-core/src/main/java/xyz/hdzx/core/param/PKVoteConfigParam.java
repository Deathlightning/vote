package xyz.hdzx.core.param;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author whq
 */
@Setter
@Getter
@ToString
public class PKVoteConfigParam {

  private int id;

  private String title;

  private String bgUrl;

  private String content;

  private String brief;

  private String type;

  private Date startTime;

  private Date endTime;

  // /**
  //  * 进行分组，里面的 list 的 size 只能是 2
  //  */
  // private List<List<VoteItemInfoParam>>  users;

  private String interactiveModuleName;

  private int interactiveModuleId;

  /**
   * 每人最高投票次数
   */
  private int voteAmount;
}
