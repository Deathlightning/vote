package xyz.hdzx.core.param;

import com.alibaba.fastjson.annotation.JSONField;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.hdzx.core.base.VoteInfoCommon;

/**
 * @author whq Date: 2018-10-06 Time: 10:06
 */
@Setter
@Getter
@ToString
public class ConfigVoteParam extends VoteInfoCommon {

    /**
     * 背景图片url
     */
    private String bgUrl;

    /**
     * 投票简介
     */
    private String brief;

    /**
     * 投票说明
     */
    private String content;

    /**
     * 每人最高投票次数
     */
    private int voteAmount;

    /**
     * 类型
     */
    private Byte type;

    /**
     * 投票人选
     */
    private List<VoteItemInfoParam> users;

    /**
     * 互动模块名称
     */
    private String interactiveModuleName;

    /**
     * 互动模块id
     */
    private int interactiveModuleId;
}
