package xyz.hdzx.core.param;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author whq
 */
@Setter
@Getter
@ToString
public class OneGroupParam {

  private List<PKVoteItemParam> params;
}
