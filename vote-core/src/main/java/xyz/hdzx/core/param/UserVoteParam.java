package xyz.hdzx.core.param;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author whq
 */
@Setter
@Getter
@ToString
public class UserVoteParam {

    private String code;

    private String voteItemId;
}
