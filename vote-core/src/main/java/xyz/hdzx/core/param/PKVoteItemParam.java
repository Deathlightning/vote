package xyz.hdzx.core.param;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author whq
 */
@Setter
@Getter
@ToString
public class PKVoteItemParam extends PKVoteConfigParam {

  private String name;

  private String photoUrl;

  private String groupId;

  public String getTitle() {
    return name;
  }

  public String getBgUrl() {
    return photoUrl;
  }
}
