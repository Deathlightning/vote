package xyz.hdzx.core.bean;

import lombok.Getter;
import lombok.Setter;

/**
 * @author whq Date: 2018-10-09 Time: 14:35
 */
@Setter
@Getter
public class PageHelper {

    /**
     * 当前页
     */
    private int currentPage = 1;

    /**
     * 偏移量
     */
    private int offset;

    /**
     * 每页的数量
     */
    private int pageNum = 10;
}
