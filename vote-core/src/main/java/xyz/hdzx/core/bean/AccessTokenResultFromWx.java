package xyz.hdzx.core.bean;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author whq
 */
@Setter
@Getter
@ToString
public class AccessTokenResultFromWx {

    /**
     * access_token : ACCESS_TOKEN expires_in : 7200 refresh_token : REFRESH_TOKEN openid : OPENID
     * scope : SCOPE
     */
    @JSONField(name = "access_token")
    private String accessToken;

    @JSONField(name = "expires_in")
    private int expiresIn;

    @JSONField(name = "refresh_token")
    private String refreshToken;

    @JSONField(name = "openid")
    private String openId;

    private String scope;

    @JSONField(name = "errcode")
    private String errCode;

    @JSONField(name = "errmsg")
    private String errMsg;

}