package xyz.hdzx.core.bean;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author whq Date: 2018-10-11 Time: 11:52
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CacheUserInfo {

    private Long id;

    private String name;

    private int type;

    private Integer currentFrequencyId;

    private Integer currentProgramId;

    String nickname;

    String email;

    String mobile;

    String union_id;     //微信用户多个公众号统一ID

    public Long getId() {
        return id;
    }

    public CacheUserInfo setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CacheUserInfo setName(String name) {
        this.name = name;
        return this;
    }

    public int getType() {
        return type;
    }

    public CacheUserInfo setType(int type) {
        this.type = type;
        return this;
    }

    public Integer getCurrentFrequencyId() {
        return currentFrequencyId;
    }

    public CacheUserInfo setCurrentFrequencyId(Integer currentFrequencyId) {
        this.currentFrequencyId = currentFrequencyId;
        return this;
    }

    public Integer getCurrentProgramId() {
        return currentProgramId;
    }

    public CacheUserInfo setCurrentProgramId(Integer currentProgramId) {
        this.currentProgramId = currentProgramId;
        return this;
    }

    public String getNickname() {
        return nickname;
    }

    public CacheUserInfo setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public CacheUserInfo setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getMobile() {
        return mobile;
    }

    public CacheUserInfo setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public String getUnion_id() {
        return union_id;
    }

    public CacheUserInfo setUnion_id(String union_id) {
        this.union_id = union_id;
        return this;
    }
}

