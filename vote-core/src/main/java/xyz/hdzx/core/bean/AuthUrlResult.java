package xyz.hdzx.core.bean;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author whq Date: 2018-10-11 Time: 9:37
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AuthUrlResult {

    private boolean isSuccess;

    private String errorMsg;

    /**
     * 用户id
     */
    @JSONField(serialzeFeatures = {SerializerFeature.WriteNonStringValueAsString,})
    private Long id;

    private String name;

    /**
     * 用户类型id
     */
    private int type;

    private String authentication;

    private Integer currentFrequencyId;

    private Integer currentProgramId;

    private String programIds;

    private String pluginStatus;
}
