package xyz.hdzx.core.bean;

import lombok.Data;

/**
 * @author whq Date: 2018-10-11 Time: 9:39
 */
@Data
public class RestResult {

    /**
     * 代表请求是否成功
     */
    private boolean success;
    /**
     * 请求状态码
     */
    private Integer code;
    /**
     * 请求返回信息描述
     */
    private String message;
    /**
     * 响应数据
     */
    private Object data;

    public RestResult() {
    }

    public RestResult(boolean success) {
        this(success, null, null);
    }

    public RestResult(boolean success, String message) {
        this(success, message, null);
    }

    public RestResult(boolean success, String message, Object data) {
        this.success = success;
        this.message = message;
        this.data = data;
    }
}
