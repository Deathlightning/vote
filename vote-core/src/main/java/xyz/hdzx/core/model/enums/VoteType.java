package xyz.hdzx.core.model.enums;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author whq Date: 2018-09-27 Time: 16:50
 */
@Getter
@AllArgsConstructor
public enum VoteType {

    /**
     * 普通投票
     */
    COMMON_VOTE(1),

    /**
     * pk投票
     */
    PK_VOTE(2),

    /**
     * 用户自定义投票
     */
    USER_DEFINITION_VOTE(3),

    /**
     * other
     */
    DEFAULT(4);

    private int value;

    private static Map<String, Integer> map = new HashMap<>();

    public static Map<String, Integer> getMap() {
        Arrays.stream(VoteType.values()).forEach(type -> map.put(type.name(), type.getValue()));
        return map;
    }

    public static VoteType getType(int value) {
        for (VoteType voteType : values()) {
            if (voteType.getValue() == value) {
                return voteType;
            }
        }

        return DEFAULT;
  }
}
