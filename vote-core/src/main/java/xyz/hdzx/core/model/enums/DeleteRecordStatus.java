package xyz.hdzx.core.model.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author whq Date: 2018-09-29 Time: 17:23
 */
@Getter
@AllArgsConstructor
public enum DeleteRecordStatus {

    /**
     * 正常
     */
    COMMON(new Byte("1")),

    /**
     * 删除
     */
    DELETE(new Byte("2")),

    /**
     * 禁用
     */
    FORBIDDEN(new Byte("3"));

    private byte value;

    private static Map<String, Byte> map = new ConcurrentHashMap<>();

    public static Map<String, Byte> getMap() {
        Arrays.stream(DeleteRecordStatus.values()).forEach(type -> map.put(type.name(), type.value));

        return map;
    }


}
