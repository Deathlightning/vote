package xyz.hdzx.core.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VoteInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public VoteInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andTitleIsNull() {
            addCriterion("title is null");
            return (Criteria) this;
        }

        public Criteria andTitleIsNotNull() {
            addCriterion("title is not null");
            return (Criteria) this;
        }

        public Criteria andTitleEqualTo(String value) {
            addCriterion("title =", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotEqualTo(String value) {
            addCriterion("title <>", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThan(String value) {
            addCriterion("title >", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThanOrEqualTo(String value) {
            addCriterion("title >=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThan(String value) {
            addCriterion("title <", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThanOrEqualTo(String value) {
            addCriterion("title <=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLike(String value) {
            addCriterion("title like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotLike(String value) {
            addCriterion("title not like", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleIn(List<String> values) {
            addCriterion("title in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotIn(List<String> values) {
            addCriterion("title not in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleBetween(String value1, String value2) {
            addCriterion("title between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotBetween(String value1, String value2) {
            addCriterion("title not between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("content is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("content is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("content =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("content <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("content >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("content >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("content <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("content <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("content like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("content not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("content in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("content not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("content between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("content not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andBriefIsNull() {
            addCriterion("brief is null");
            return (Criteria) this;
        }

        public Criteria andBriefIsNotNull() {
            addCriterion("brief is not null");
            return (Criteria) this;
        }

        public Criteria andBriefEqualTo(String value) {
            addCriterion("brief =", value, "brief");
            return (Criteria) this;
        }

        public Criteria andBriefNotEqualTo(String value) {
            addCriterion("brief <>", value, "brief");
            return (Criteria) this;
        }

        public Criteria andBriefGreaterThan(String value) {
            addCriterion("brief >", value, "brief");
            return (Criteria) this;
        }

        public Criteria andBriefGreaterThanOrEqualTo(String value) {
            addCriterion("brief >=", value, "brief");
            return (Criteria) this;
        }

        public Criteria andBriefLessThan(String value) {
            addCriterion("brief <", value, "brief");
            return (Criteria) this;
        }

        public Criteria andBriefLessThanOrEqualTo(String value) {
            addCriterion("brief <=", value, "brief");
            return (Criteria) this;
        }

        public Criteria andBriefLike(String value) {
            addCriterion("brief like", value, "brief");
            return (Criteria) this;
        }

        public Criteria andBriefNotLike(String value) {
            addCriterion("brief not like", value, "brief");
            return (Criteria) this;
        }

        public Criteria andBriefIn(List<String> values) {
            addCriterion("brief in", values, "brief");
            return (Criteria) this;
        }

        public Criteria andBriefNotIn(List<String> values) {
            addCriterion("brief not in", values, "brief");
            return (Criteria) this;
        }

        public Criteria andBriefBetween(String value1, String value2) {
            addCriterion("brief between", value1, value2, "brief");
            return (Criteria) this;
        }

        public Criteria andBriefNotBetween(String value1, String value2) {
            addCriterion("brief not between", value1, value2, "brief");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Byte value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Byte value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Byte value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Byte value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Byte value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Byte> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Byte> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Byte value1, Byte value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("start_time is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("start_time is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(Date value) {
            addCriterion("start_time =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(Date value) {
            addCriterion("start_time <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(Date value) {
            addCriterion("start_time >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("start_time >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(Date value) {
            addCriterion("start_time <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("start_time <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<Date> values) {
            addCriterion("start_time in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<Date> values) {
            addCriterion("start_time not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(Date value1, Date value2) {
            addCriterion("start_time between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("start_time not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNull() {
            addCriterion("end_time is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNotNull() {
            addCriterion("end_time is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeEqualTo(Date value) {
            addCriterion("end_time =", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotEqualTo(Date value) {
            addCriterion("end_time <>", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThan(Date value) {
            addCriterion("end_time >", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("end_time >=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThan(Date value) {
            addCriterion("end_time <", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("end_time <=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIn(List<Date> values) {
            addCriterion("end_time in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotIn(List<Date> values) {
            addCriterion("end_time not in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeBetween(Date value1, Date value2) {
            addCriterion("end_time between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("end_time not between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andFrequencyIdIsNull() {
            addCriterion("frequency_id is null");
            return (Criteria) this;
        }

        public Criteria andFrequencyIdIsNotNull() {
            addCriterion("frequency_id is not null");
            return (Criteria) this;
        }

        public Criteria andFrequencyIdEqualTo(Integer value) {
            addCriterion("frequency_id =", value, "frequencyId");
            return (Criteria) this;
        }

        public Criteria andFrequencyIdNotEqualTo(Integer value) {
            addCriterion("frequency_id <>", value, "frequencyId");
            return (Criteria) this;
        }

        public Criteria andFrequencyIdGreaterThan(Integer value) {
            addCriterion("frequency_id >", value, "frequencyId");
            return (Criteria) this;
        }

        public Criteria andFrequencyIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("frequency_id >=", value, "frequencyId");
            return (Criteria) this;
        }

        public Criteria andFrequencyIdLessThan(Integer value) {
            addCriterion("frequency_id <", value, "frequencyId");
            return (Criteria) this;
        }

        public Criteria andFrequencyIdLessThanOrEqualTo(Integer value) {
            addCriterion("frequency_id <=", value, "frequencyId");
            return (Criteria) this;
        }

        public Criteria andFrequencyIdIn(List<Integer> values) {
            addCriterion("frequency_id in", values, "frequencyId");
            return (Criteria) this;
        }

        public Criteria andFrequencyIdNotIn(List<Integer> values) {
            addCriterion("frequency_id not in", values, "frequencyId");
            return (Criteria) this;
        }

        public Criteria andFrequencyIdBetween(Integer value1, Integer value2) {
            addCriterion("frequency_id between", value1, value2, "frequencyId");
            return (Criteria) this;
        }

        public Criteria andFrequencyIdNotBetween(Integer value1, Integer value2) {
            addCriterion("frequency_id not between", value1, value2, "frequencyId");
            return (Criteria) this;
        }

        public Criteria andProgramIdIsNull() {
            addCriterion("program_id is null");
            return (Criteria) this;
        }

        public Criteria andProgramIdIsNotNull() {
            addCriterion("program_id is not null");
            return (Criteria) this;
        }

        public Criteria andProgramIdEqualTo(Integer value) {
            addCriterion("program_id =", value, "programId");
            return (Criteria) this;
        }

        public Criteria andProgramIdNotEqualTo(Integer value) {
            addCriterion("program_id <>", value, "programId");
            return (Criteria) this;
        }

        public Criteria andProgramIdGreaterThan(Integer value) {
            addCriterion("program_id >", value, "programId");
            return (Criteria) this;
        }

        public Criteria andProgramIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("program_id >=", value, "programId");
            return (Criteria) this;
        }

        public Criteria andProgramIdLessThan(Integer value) {
            addCriterion("program_id <", value, "programId");
            return (Criteria) this;
        }

        public Criteria andProgramIdLessThanOrEqualTo(Integer value) {
            addCriterion("program_id <=", value, "programId");
            return (Criteria) this;
        }

        public Criteria andProgramIdIn(List<Integer> values) {
            addCriterion("program_id in", values, "programId");
            return (Criteria) this;
        }

        public Criteria andProgramIdNotIn(List<Integer> values) {
            addCriterion("program_id not in", values, "programId");
            return (Criteria) this;
        }

        public Criteria andProgramIdBetween(Integer value1, Integer value2) {
            addCriterion("program_id between", value1, value2, "programId");
            return (Criteria) this;
        }

        public Criteria andProgramIdNotBetween(Integer value1, Integer value2) {
            addCriterion("program_id not between", value1, value2, "programId");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleNameIsNull() {
            addCriterion("interactive_module_name is null");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleNameIsNotNull() {
            addCriterion("interactive_module_name is not null");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleNameEqualTo(String value) {
            addCriterion("interactive_module_name =", value, "interactiveModuleName");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleNameNotEqualTo(String value) {
            addCriterion("interactive_module_name <>", value, "interactiveModuleName");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleNameGreaterThan(String value) {
            addCriterion("interactive_module_name >", value, "interactiveModuleName");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleNameGreaterThanOrEqualTo(String value) {
            addCriterion("interactive_module_name >=", value, "interactiveModuleName");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleNameLessThan(String value) {
            addCriterion("interactive_module_name <", value, "interactiveModuleName");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleNameLessThanOrEqualTo(String value) {
            addCriterion("interactive_module_name <=", value, "interactiveModuleName");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleNameLike(String value) {
            addCriterion("interactive_module_name like", value, "interactiveModuleName");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleNameNotLike(String value) {
            addCriterion("interactive_module_name not like", value, "interactiveModuleName");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleNameIn(List<String> values) {
            addCriterion("interactive_module_name in", values, "interactiveModuleName");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleNameNotIn(List<String> values) {
            addCriterion("interactive_module_name not in", values, "interactiveModuleName");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleNameBetween(String value1, String value2) {
            addCriterion("interactive_module_name between", value1, value2, "interactiveModuleName");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleNameNotBetween(String value1, String value2) {
            addCriterion("interactive_module_name not between", value1, value2, "interactiveModuleName");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleIdIsNull() {
            addCriterion("interactive_module_id is null");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleIdIsNotNull() {
            addCriterion("interactive_module_id is not null");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleIdEqualTo(Integer value) {
            addCriterion("interactive_module_id =", value, "interactiveModuleId");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleIdNotEqualTo(Integer value) {
            addCriterion("interactive_module_id <>", value, "interactiveModuleId");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleIdGreaterThan(Integer value) {
            addCriterion("interactive_module_id >", value, "interactiveModuleId");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("interactive_module_id >=", value, "interactiveModuleId");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleIdLessThan(Integer value) {
            addCriterion("interactive_module_id <", value, "interactiveModuleId");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleIdLessThanOrEqualTo(Integer value) {
            addCriterion("interactive_module_id <=", value, "interactiveModuleId");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleIdIn(List<Integer> values) {
            addCriterion("interactive_module_id in", values, "interactiveModuleId");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleIdNotIn(List<Integer> values) {
            addCriterion("interactive_module_id not in", values, "interactiveModuleId");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleIdBetween(Integer value1, Integer value2) {
            addCriterion("interactive_module_id between", value1, value2, "interactiveModuleId");
            return (Criteria) this;
        }

        public Criteria andInteractiveModuleIdNotBetween(Integer value1, Integer value2) {
            addCriterion("interactive_module_id not between", value1, value2, "interactiveModuleId");
            return (Criteria) this;
        }

        public Criteria andViewNumIsNull() {
            addCriterion("view_num is null");
            return (Criteria) this;
        }

        public Criteria andViewNumIsNotNull() {
            addCriterion("view_num is not null");
            return (Criteria) this;
        }

        public Criteria andViewNumEqualTo(Integer value) {
            addCriterion("view_num =", value, "viewNum");
            return (Criteria) this;
        }

        public Criteria andViewNumNotEqualTo(Integer value) {
            addCriterion("view_num <>", value, "viewNum");
            return (Criteria) this;
        }

        public Criteria andViewNumGreaterThan(Integer value) {
            addCriterion("view_num >", value, "viewNum");
            return (Criteria) this;
        }

        public Criteria andViewNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("view_num >=", value, "viewNum");
            return (Criteria) this;
        }

        public Criteria andViewNumLessThan(Integer value) {
            addCriterion("view_num <", value, "viewNum");
            return (Criteria) this;
        }

        public Criteria andViewNumLessThanOrEqualTo(Integer value) {
            addCriterion("view_num <=", value, "viewNum");
            return (Criteria) this;
        }

        public Criteria andViewNumIn(List<Integer> values) {
            addCriterion("view_num in", values, "viewNum");
            return (Criteria) this;
        }

        public Criteria andViewNumNotIn(List<Integer> values) {
            addCriterion("view_num not in", values, "viewNum");
            return (Criteria) this;
        }

        public Criteria andViewNumBetween(Integer value1, Integer value2) {
            addCriterion("view_num between", value1, value2, "viewNum");
            return (Criteria) this;
        }

        public Criteria andViewNumNotBetween(Integer value1, Integer value2) {
            addCriterion("view_num not between", value1, value2, "viewNum");
            return (Criteria) this;
        }

        public Criteria andTotalClickIsNull() {
            addCriterion("total_click is null");
            return (Criteria) this;
        }

        public Criteria andTotalClickIsNotNull() {
            addCriterion("total_click is not null");
            return (Criteria) this;
        }

        public Criteria andTotalClickEqualTo(Integer value) {
            addCriterion("total_click =", value, "totalClick");
            return (Criteria) this;
        }

        public Criteria andTotalClickNotEqualTo(Integer value) {
            addCriterion("total_click <>", value, "totalClick");
            return (Criteria) this;
        }

        public Criteria andTotalClickGreaterThan(Integer value) {
            addCriterion("total_click >", value, "totalClick");
            return (Criteria) this;
        }

        public Criteria andTotalClickGreaterThanOrEqualTo(Integer value) {
            addCriterion("total_click >=", value, "totalClick");
            return (Criteria) this;
        }

        public Criteria andTotalClickLessThan(Integer value) {
            addCriterion("total_click <", value, "totalClick");
            return (Criteria) this;
        }

        public Criteria andTotalClickLessThanOrEqualTo(Integer value) {
            addCriterion("total_click <=", value, "totalClick");
            return (Criteria) this;
        }

        public Criteria andTotalClickIn(List<Integer> values) {
            addCriterion("total_click in", values, "totalClick");
            return (Criteria) this;
        }

        public Criteria andTotalClickNotIn(List<Integer> values) {
            addCriterion("total_click not in", values, "totalClick");
            return (Criteria) this;
        }

        public Criteria andTotalClickBetween(Integer value1, Integer value2) {
            addCriterion("total_click between", value1, value2, "totalClick");
            return (Criteria) this;
        }

        public Criteria andTotalClickNotBetween(Integer value1, Integer value2) {
            addCriterion("total_click not between", value1, value2, "totalClick");
            return (Criteria) this;
        }

        public Criteria andMaxClickIsNull() {
            addCriterion("max_click is null");
            return (Criteria) this;
        }

        public Criteria andMaxClickIsNotNull() {
            addCriterion("max_click is not null");
            return (Criteria) this;
        }

        public Criteria andMaxClickEqualTo(Integer value) {
            addCriterion("max_click =", value, "maxClick");
            return (Criteria) this;
        }

        public Criteria andMaxClickNotEqualTo(Integer value) {
            addCriterion("max_click <>", value, "maxClick");
            return (Criteria) this;
        }

        public Criteria andMaxClickGreaterThan(Integer value) {
            addCriterion("max_click >", value, "maxClick");
            return (Criteria) this;
        }

        public Criteria andMaxClickGreaterThanOrEqualTo(Integer value) {
            addCriterion("max_click >=", value, "maxClick");
            return (Criteria) this;
        }

        public Criteria andMaxClickLessThan(Integer value) {
            addCriterion("max_click <", value, "maxClick");
            return (Criteria) this;
        }

        public Criteria andMaxClickLessThanOrEqualTo(Integer value) {
            addCriterion("max_click <=", value, "maxClick");
            return (Criteria) this;
        }

        public Criteria andMaxClickIn(List<Integer> values) {
            addCriterion("max_click in", values, "maxClick");
            return (Criteria) this;
        }

        public Criteria andMaxClickNotIn(List<Integer> values) {
            addCriterion("max_click not in", values, "maxClick");
            return (Criteria) this;
        }

        public Criteria andMaxClickBetween(Integer value1, Integer value2) {
            addCriterion("max_click between", value1, value2, "maxClick");
            return (Criteria) this;
        }

        public Criteria andMaxClickNotBetween(Integer value1, Integer value2) {
            addCriterion("max_click not between", value1, value2, "maxClick");
            return (Criteria) this;
        }

        public Criteria andMaxUserIdIsNull() {
            addCriterion("max_user_id is null");
            return (Criteria) this;
        }

        public Criteria andMaxUserIdIsNotNull() {
            addCriterion("max_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andMaxUserIdEqualTo(Integer value) {
            addCriterion("max_user_id =", value, "maxUserId");
            return (Criteria) this;
        }

        public Criteria andMaxUserIdNotEqualTo(Integer value) {
            addCriterion("max_user_id <>", value, "maxUserId");
            return (Criteria) this;
        }

        public Criteria andMaxUserIdGreaterThan(Integer value) {
            addCriterion("max_user_id >", value, "maxUserId");
            return (Criteria) this;
        }

        public Criteria andMaxUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("max_user_id >=", value, "maxUserId");
            return (Criteria) this;
        }

        public Criteria andMaxUserIdLessThan(Integer value) {
            addCriterion("max_user_id <", value, "maxUserId");
            return (Criteria) this;
        }

        public Criteria andMaxUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("max_user_id <=", value, "maxUserId");
            return (Criteria) this;
        }

        public Criteria andMaxUserIdIn(List<Integer> values) {
            addCriterion("max_user_id in", values, "maxUserId");
            return (Criteria) this;
        }

        public Criteria andMaxUserIdNotIn(List<Integer> values) {
            addCriterion("max_user_id not in", values, "maxUserId");
            return (Criteria) this;
        }

        public Criteria andMaxUserIdBetween(Integer value1, Integer value2) {
            addCriterion("max_user_id between", value1, value2, "maxUserId");
            return (Criteria) this;
        }

        public Criteria andMaxUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("max_user_id not between", value1, value2, "maxUserId");
            return (Criteria) this;
        }

        public Criteria andMaxNameIsNull() {
            addCriterion("max_name is null");
            return (Criteria) this;
        }

        public Criteria andMaxNameIsNotNull() {
            addCriterion("max_name is not null");
            return (Criteria) this;
        }

        public Criteria andMaxNameEqualTo(String value) {
            addCriterion("max_name =", value, "maxName");
            return (Criteria) this;
        }

        public Criteria andMaxNameNotEqualTo(String value) {
            addCriterion("max_name <>", value, "maxName");
            return (Criteria) this;
        }

        public Criteria andMaxNameGreaterThan(String value) {
            addCriterion("max_name >", value, "maxName");
            return (Criteria) this;
        }

        public Criteria andMaxNameGreaterThanOrEqualTo(String value) {
            addCriterion("max_name >=", value, "maxName");
            return (Criteria) this;
        }

        public Criteria andMaxNameLessThan(String value) {
            addCriterion("max_name <", value, "maxName");
            return (Criteria) this;
        }

        public Criteria andMaxNameLessThanOrEqualTo(String value) {
            addCriterion("max_name <=", value, "maxName");
            return (Criteria) this;
        }

        public Criteria andMaxNameLike(String value) {
            addCriterion("max_name like", value, "maxName");
            return (Criteria) this;
        }

        public Criteria andMaxNameNotLike(String value) {
            addCriterion("max_name not like", value, "maxName");
            return (Criteria) this;
        }

        public Criteria andMaxNameIn(List<String> values) {
            addCriterion("max_name in", values, "maxName");
            return (Criteria) this;
        }

        public Criteria andMaxNameNotIn(List<String> values) {
            addCriterion("max_name not in", values, "maxName");
            return (Criteria) this;
        }

        public Criteria andMaxNameBetween(String value1, String value2) {
            addCriterion("max_name between", value1, value2, "maxName");
            return (Criteria) this;
        }

        public Criteria andMaxNameNotBetween(String value1, String value2) {
            addCriterion("max_name not between", value1, value2, "maxName");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNull() {
            addCriterion("creator is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNotNull() {
            addCriterion("creator is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorEqualTo(String value) {
            addCriterion("creator =", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotEqualTo(String value) {
            addCriterion("creator <>", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThan(String value) {
            addCriterion("creator >", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThanOrEqualTo(String value) {
            addCriterion("creator >=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThan(String value) {
            addCriterion("creator <", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThanOrEqualTo(String value) {
            addCriterion("creator <=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLike(String value) {
            addCriterion("creator like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotLike(String value) {
            addCriterion("creator not like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorIn(List<String> values) {
            addCriterion("creator in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotIn(List<String> values) {
            addCriterion("creator not in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorBetween(String value1, String value2) {
            addCriterion("creator between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotBetween(String value1, String value2) {
            addCriterion("creator not between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdaterIsNull() {
            addCriterion("updater is null");
            return (Criteria) this;
        }

        public Criteria andUpdaterIsNotNull() {
            addCriterion("updater is not null");
            return (Criteria) this;
        }

        public Criteria andUpdaterEqualTo(String value) {
            addCriterion("updater =", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterNotEqualTo(String value) {
            addCriterion("updater <>", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterGreaterThan(String value) {
            addCriterion("updater >", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterGreaterThanOrEqualTo(String value) {
            addCriterion("updater >=", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterLessThan(String value) {
            addCriterion("updater <", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterLessThanOrEqualTo(String value) {
            addCriterion("updater <=", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterLike(String value) {
            addCriterion("updater like", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterNotLike(String value) {
            addCriterion("updater not like", value, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterIn(List<String> values) {
            addCriterion("updater in", values, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterNotIn(List<String> values) {
            addCriterion("updater not in", values, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterBetween(String value1, String value2) {
            addCriterion("updater between", value1, value2, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdaterNotBetween(String value1, String value2) {
            addCriterion("updater not between", value1, value2, "updater");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andDelIsNull() {
            addCriterion("del is null");
            return (Criteria) this;
        }

        public Criteria andDelIsNotNull() {
            addCriterion("del is not null");
            return (Criteria) this;
        }

        public Criteria andDelEqualTo(Byte value) {
            addCriterion("del =", value, "del");
            return (Criteria) this;
        }

        public Criteria andDelNotEqualTo(Byte value) {
            addCriterion("del <>", value, "del");
            return (Criteria) this;
        }

        public Criteria andDelGreaterThan(Byte value) {
            addCriterion("del >", value, "del");
            return (Criteria) this;
        }

        public Criteria andDelGreaterThanOrEqualTo(Byte value) {
            addCriterion("del >=", value, "del");
            return (Criteria) this;
        }

        public Criteria andDelLessThan(Byte value) {
            addCriterion("del <", value, "del");
            return (Criteria) this;
        }

        public Criteria andDelLessThanOrEqualTo(Byte value) {
            addCriterion("del <=", value, "del");
            return (Criteria) this;
        }

        public Criteria andDelIn(List<Byte> values) {
            addCriterion("del in", values, "del");
            return (Criteria) this;
        }

        public Criteria andDelNotIn(List<Byte> values) {
            addCriterion("del not in", values, "del");
            return (Criteria) this;
        }

        public Criteria andDelBetween(Byte value1, Byte value2) {
            addCriterion("del between", value1, value2, "del");
            return (Criteria) this;
        }

        public Criteria andDelNotBetween(Byte value1, Byte value2) {
            addCriterion("del not between", value1, value2, "del");
            return (Criteria) this;
        }

        public Criteria andVoteAmountIsNull() {
            addCriterion("vote_amount is null");
            return (Criteria) this;
        }

        public Criteria andVoteAmountIsNotNull() {
            addCriterion("vote_amount is not null");
            return (Criteria) this;
        }

        public Criteria andVoteAmountEqualTo(Byte value) {
            addCriterion("vote_amount =", value, "voteAmount");
            return (Criteria) this;
        }

        public Criteria andVoteAmountNotEqualTo(Byte value) {
            addCriterion("vote_amount <>", value, "voteAmount");
            return (Criteria) this;
        }

        public Criteria andVoteAmountGreaterThan(Byte value) {
            addCriterion("vote_amount >", value, "voteAmount");
            return (Criteria) this;
        }

        public Criteria andVoteAmountGreaterThanOrEqualTo(Byte value) {
            addCriterion("vote_amount >=", value, "voteAmount");
            return (Criteria) this;
        }

        public Criteria andVoteAmountLessThan(Byte value) {
            addCriterion("vote_amount <", value, "voteAmount");
            return (Criteria) this;
        }

        public Criteria andVoteAmountLessThanOrEqualTo(Byte value) {
            addCriterion("vote_amount <=", value, "voteAmount");
            return (Criteria) this;
        }

        public Criteria andVoteAmountIn(List<Byte> values) {
            addCriterion("vote_amount in", values, "voteAmount");
            return (Criteria) this;
        }

        public Criteria andVoteAmountNotIn(List<Byte> values) {
            addCriterion("vote_amount not in", values, "voteAmount");
            return (Criteria) this;
        }

        public Criteria andVoteAmountBetween(Byte value1, Byte value2) {
            addCriterion("vote_amount between", value1, value2, "voteAmount");
            return (Criteria) this;
        }

        public Criteria andVoteAmountNotBetween(Byte value1, Byte value2) {
            addCriterion("vote_amount not between", value1, value2, "voteAmount");
            return (Criteria) this;
        }

        public Criteria andPhotoUrlIsNull() {
            addCriterion("photo_url is null");
            return (Criteria) this;
        }

        public Criteria andPhotoUrlIsNotNull() {
            addCriterion("photo_url is not null");
            return (Criteria) this;
        }

        public Criteria andPhotoUrlEqualTo(String value) {
            addCriterion("photo_url =", value, "photoUrl");
            return (Criteria) this;
        }

        public Criteria andPhotoUrlNotEqualTo(String value) {
            addCriterion("photo_url <>", value, "photoUrl");
            return (Criteria) this;
        }

        public Criteria andPhotoUrlGreaterThan(String value) {
            addCriterion("photo_url >", value, "photoUrl");
            return (Criteria) this;
        }

        public Criteria andPhotoUrlGreaterThanOrEqualTo(String value) {
            addCriterion("photo_url >=", value, "photoUrl");
            return (Criteria) this;
        }

        public Criteria andPhotoUrlLessThan(String value) {
            addCriterion("photo_url <", value, "photoUrl");
            return (Criteria) this;
        }

        public Criteria andPhotoUrlLessThanOrEqualTo(String value) {
            addCriterion("photo_url <=", value, "photoUrl");
            return (Criteria) this;
        }

        public Criteria andPhotoUrlLike(String value) {
            addCriterion("photo_url like", value, "photoUrl");
            return (Criteria) this;
        }

        public Criteria andPhotoUrlNotLike(String value) {
            addCriterion("photo_url not like", value, "photoUrl");
            return (Criteria) this;
        }

        public Criteria andPhotoUrlIn(List<String> values) {
            addCriterion("photo_url in", values, "photoUrl");
            return (Criteria) this;
        }

        public Criteria andPhotoUrlNotIn(List<String> values) {
            addCriterion("photo_url not in", values, "photoUrl");
            return (Criteria) this;
        }

        public Criteria andPhotoUrlBetween(String value1, String value2) {
            addCriterion("photo_url between", value1, value2, "photoUrl");
            return (Criteria) this;
        }

        public Criteria andPhotoUrlNotBetween(String value1, String value2) {
            addCriterion("photo_url not between", value1, value2, "photoUrl");
            return (Criteria) this;
        }

        public Criteria andDataIsNull() {
            addCriterion("data is null");
            return (Criteria) this;
        }

        public Criteria andDataIsNotNull() {
            addCriterion("data is not null");
            return (Criteria) this;
        }

        public Criteria andDataEqualTo(String value) {
            addCriterion("data =", value, "data");
            return (Criteria) this;
        }

        public Criteria andDataNotEqualTo(String value) {
            addCriterion("data <>", value, "data");
            return (Criteria) this;
        }

        public Criteria andDataGreaterThan(String value) {
            addCriterion("data >", value, "data");
            return (Criteria) this;
        }

        public Criteria andDataGreaterThanOrEqualTo(String value) {
            addCriterion("data >=", value, "data");
            return (Criteria) this;
        }

        public Criteria andDataLessThan(String value) {
            addCriterion("data <", value, "data");
            return (Criteria) this;
        }

        public Criteria andDataLessThanOrEqualTo(String value) {
            addCriterion("data <=", value, "data");
            return (Criteria) this;
        }

        public Criteria andDataLike(String value) {
            addCriterion("data like", value, "data");
            return (Criteria) this;
        }

        public Criteria andDataNotLike(String value) {
            addCriterion("data not like", value, "data");
            return (Criteria) this;
        }

        public Criteria andDataIn(List<String> values) {
            addCriterion("data in", values, "data");
            return (Criteria) this;
        }

        public Criteria andDataNotIn(List<String> values) {
            addCriterion("data not in", values, "data");
            return (Criteria) this;
        }

        public Criteria andDataBetween(String value1, String value2) {
            addCriterion("data between", value1, value2, "data");
            return (Criteria) this;
        }

        public Criteria andDataNotBetween(String value1, String value2) {
            addCriterion("data not between", value1, value2, "data");
            return (Criteria) this;
        }

        public Criteria andCreatoridIsNull() {
            addCriterion("creatorId is null");
            return (Criteria) this;
        }

        public Criteria andCreatoridIsNotNull() {
            addCriterion("creatorId is not null");
            return (Criteria) this;
        }

        public Criteria andCreatoridEqualTo(String value) {
            addCriterion("creatorId =", value, "creatorid");
            return (Criteria) this;
        }

        public Criteria andCreatoridNotEqualTo(String value) {
            addCriterion("creatorId <>", value, "creatorid");
            return (Criteria) this;
        }

        public Criteria andCreatoridGreaterThan(String value) {
            addCriterion("creatorId >", value, "creatorid");
            return (Criteria) this;
        }

        public Criteria andCreatoridGreaterThanOrEqualTo(String value) {
            addCriterion("creatorId >=", value, "creatorid");
            return (Criteria) this;
        }

        public Criteria andCreatoridLessThan(String value) {
            addCriterion("creatorId <", value, "creatorid");
            return (Criteria) this;
        }

        public Criteria andCreatoridLessThanOrEqualTo(String value) {
            addCriterion("creatorId <=", value, "creatorid");
            return (Criteria) this;
        }

        public Criteria andCreatoridLike(String value) {
            addCriterion("creatorId like", value, "creatorid");
            return (Criteria) this;
        }

        public Criteria andCreatoridNotLike(String value) {
            addCriterion("creatorId not like", value, "creatorid");
            return (Criteria) this;
        }

        public Criteria andCreatoridIn(List<String> values) {
            addCriterion("creatorId in", values, "creatorid");
            return (Criteria) this;
        }

        public Criteria andCreatoridNotIn(List<String> values) {
            addCriterion("creatorId not in", values, "creatorid");
            return (Criteria) this;
        }

        public Criteria andCreatoridBetween(String value1, String value2) {
            addCriterion("creatorId between", value1, value2, "creatorid");
            return (Criteria) this;
        }

        public Criteria andCreatoridNotBetween(String value1, String value2) {
            addCriterion("creatorId not between", value1, value2, "creatorid");
            return (Criteria) this;
        }

        public Criteria andUpdateridIsNull() {
            addCriterion("updaterId is null");
            return (Criteria) this;
        }

        public Criteria andUpdateridIsNotNull() {
            addCriterion("updaterId is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateridEqualTo(String value) {
            addCriterion("updaterId =", value, "updaterid");
            return (Criteria) this;
        }

        public Criteria andUpdateridNotEqualTo(String value) {
            addCriterion("updaterId <>", value, "updaterid");
            return (Criteria) this;
        }

        public Criteria andUpdateridGreaterThan(String value) {
            addCriterion("updaterId >", value, "updaterid");
            return (Criteria) this;
        }

        public Criteria andUpdateridGreaterThanOrEqualTo(String value) {
            addCriterion("updaterId >=", value, "updaterid");
            return (Criteria) this;
        }

        public Criteria andUpdateridLessThan(String value) {
            addCriterion("updaterId <", value, "updaterid");
            return (Criteria) this;
        }

        public Criteria andUpdateridLessThanOrEqualTo(String value) {
            addCriterion("updaterId <=", value, "updaterid");
            return (Criteria) this;
        }

        public Criteria andUpdateridLike(String value) {
            addCriterion("updaterId like", value, "updaterid");
            return (Criteria) this;
        }

        public Criteria andUpdateridNotLike(String value) {
            addCriterion("updaterId not like", value, "updaterid");
            return (Criteria) this;
        }

        public Criteria andUpdateridIn(List<String> values) {
            addCriterion("updaterId in", values, "updaterid");
            return (Criteria) this;
        }

        public Criteria andUpdateridNotIn(List<String> values) {
            addCriterion("updaterId not in", values, "updaterid");
            return (Criteria) this;
        }

        public Criteria andUpdateridBetween(String value1, String value2) {
            addCriterion("updaterId between", value1, value2, "updaterid");
            return (Criteria) this;
        }

        public Criteria andUpdateridNotBetween(String value1, String value2) {
            addCriterion("updaterId not between", value1, value2, "updaterid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}