package xyz.hdzx.core.model;

import java.util.Date;
import lombok.ToString;

@ToString
public class VoteInfo {

    private String id;

    private String title;

    private String content;

    private String brief;

    private Byte type;

    private Date startTime;

    private Date endTime;

    private Integer frequencyId;

    private Integer programId;

    private String interactiveModuleName;

    private Integer interactiveModuleId;

    private Integer viewNum;

    private Integer totalClick;

    private Integer maxClick;

    private Integer maxUserId;

    private String maxName;

    private String creator;

    private Date createTime;

    private String updater;

    private Date updateTime;

    private Byte status;

    private Byte del;

    private Byte voteAmount;

    private String photoUrl;

    private String data;

    private String creatorid;

    private String updaterid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief == null ? null : brief.trim();
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getFrequencyId() {
        return frequencyId;
    }

    public void setFrequencyId(Integer frequencyId) {
        this.frequencyId = frequencyId;
    }

    public Integer getProgramId() {
        return programId;
    }

    public void setProgramId(Integer programId) {
        this.programId = programId;
    }

    public String getInteractiveModuleName() {
        return interactiveModuleName;
    }

    public void setInteractiveModuleName(String interactiveModuleName) {
        this.interactiveModuleName = interactiveModuleName == null ? null : interactiveModuleName.trim();
    }

    public Integer getInteractiveModuleId() {
        return interactiveModuleId;
    }

    public void setInteractiveModuleId(Integer interactiveModuleId) {
        this.interactiveModuleId = interactiveModuleId;
    }

    public Integer getViewNum() {
        return viewNum;
    }

    public void setViewNum(Integer viewNum) {
        this.viewNum = viewNum;
    }

    public Integer getTotalClick() {
        return totalClick;
    }

    public void setTotalClick(Integer totalClick) {
        this.totalClick = totalClick;
    }

    public Integer getMaxClick() {
        return maxClick;
    }

    public void setMaxClick(Integer maxClick) {
        this.maxClick = maxClick;
    }

    public Integer getMaxUserId() {
        return maxUserId;
    }

    public void setMaxUserId(Integer maxUserId) {
        this.maxUserId = maxUserId;
    }

    public String getMaxName() {
        return maxName;
    }

    public void setMaxName(String maxName) {
        this.maxName = maxName == null ? null : maxName.trim();
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdater() {
        return updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater == null ? null : updater.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Byte getDel() {
        return del;
    }

    public void setDel(Byte del) {
        this.del = del;
    }

    public Byte getVoteAmount() {
        return voteAmount;
    }

    public void setVoteAmount(Byte voteAmount) {
        this.voteAmount = voteAmount;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl == null ? null : photoUrl.trim();
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data == null ? null : data.trim();
    }

    public String getCreatorid() {
        return creatorid;
    }

    public void setCreatorid(String creatorid) {
        this.creatorid = creatorid == null ? null : creatorid.trim();
    }

    public String getUpdaterid() {
        return updaterid;
    }

    public void setUpdaterid(String updaterid) {
        this.updaterid = updaterid == null ? null : updaterid.trim();
    }
}