package xyz.hdzx.core.model.enums;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.util.CollectionUtils;

/**
 * @author whq Date: 2018-09-29 Time: 17:15
 */
@Getter
@AllArgsConstructor
public enum VoteStatus {
    /**
     * 即将开始
     */
    COMING(1, "即将开始"),

    /**
     * 已经开始
     */
    STARTING(2, "正在进行中"),

    /**
     * 已经结束
     */
    OVER(3, "已结束"),

    /**
     * 待发布
     */
    PENDING(4, "待发布"),

    /**
     * 已经发布
     */
    PENDED(5, "已经发布"),

    /**
     * 保存状态
     */
    SAVE(6, "临时保存");

    private byte value;

    private String desc;

    VoteStatus(Integer value, String desc) {
        this.value = Byte.valueOf(value.toString());
        this.desc = desc;
    }

    private static Map<Byte, String> map = new ConcurrentHashMap<>(6);
    private static Map<Byte, VoteStatus> m = new ConcurrentHashMap<>(6);

    public static Map<Byte, String> getMap() {
        Arrays.stream(VoteStatus.values()).forEach(type -> map.put(type.value, type.desc));

        return map;
    }

    public static VoteStatus getStatus(byte value) {

        final List<VoteStatus> collect = Arrays.stream(values()).filter(v -> v.getValue() == value)
            .collect(Collectors.toList());

        if (CollectionUtils.isEmpty(collect)) {
            return OVER;
        } else {
            return collect.get(0);
        }
    }

}
