package xyz.hdzx.core.vo;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import xyz.hdzx.core.base.VoteInfoBase;

/**
 * 用来查看投票的配置
 *
 * @author whq Date: 2018-09-29 Time: 21:12
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class VoteConfigVo extends VoteInfoBase {

    /**
     * 投票次数
     */
    private int voteAmount;

    /**
     * 被投票用户信息
     */
    private List<VoteItemInfoVo> users;
}
