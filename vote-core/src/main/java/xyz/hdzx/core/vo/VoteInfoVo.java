package xyz.hdzx.core.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import xyz.hdzx.core.base.VoteInfoBase;

/**
 * 用来展示投票信息和搜索结果信息
 *
 * @author whq Date: 2018-09-29 Time: 20:48
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class VoteInfoVo extends VoteInfoBase {

    /**
     * 总票数
     */
    private int totalNumber;

    /**
     * 最高数据
     */
    private int maxNumber;

    /**
     * 最高数据的用户名
     */
    private String maxNumberName;

    /**
     * 总浏览量
     */
    private int viewNumber;
}
