package xyz.hdzx.core.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author whq
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PluginInfo {

  private String id;
  //所查询的插件的结果所对应的数据库中的Id
  private String url;
  //该ID对应的名称
  private String pluginName;

  //如果是和互动模块的信息  则: 0 代表是音频类型  4 代表是视频类型
  //如果是问卷投票的话,则:
  private int pluginType;
  //开始时间
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Date startTime;
  //结束时间
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Date endTime;

  private String moduleName;
}