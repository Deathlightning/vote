package xyz.hdzx.core.vo;

import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author whq
 */
@ToString
@Getter
@Builder
public class AnalyzeVo {

  private String title;

  private Date startTime;

  private Date endTime;

  private int viewNum;

  private int ClickNum;

  private List<Item> items;

  private List<List<Item>> lists;

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class Item {

    private String name;

    private int poll;

    private double scale;
  }
}
