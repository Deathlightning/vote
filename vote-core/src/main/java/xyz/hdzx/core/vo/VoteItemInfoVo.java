package xyz.hdzx.core.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.hdzx.core.base.VoteItemInfoBase;

/**
 * @author whq Date: 2018-09-29 Time: 21:13
 */
@Getter
@Setter
@ToString
public class VoteItemInfoVo extends VoteItemInfoBase {

    /**
     * 所得票数
     */
    private int poll;

    /**
     * 状态
     */
    private int status;

    /**
     * 所占比
     */
    private double scala;

    /**
     * 是否pk过
     */
    private boolean isPk;

}
