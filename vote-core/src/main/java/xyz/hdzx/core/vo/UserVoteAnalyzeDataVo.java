package xyz.hdzx.core.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author whq
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserVoteAnalyzeDataVo {

  /**
   * 名字
   */
  @Excel(name = "投票项名称")
  private String name;
  /**
   * 占用比例
   */
  @Excel(name = "比例(%)")
  private double scale;

  /**
   * 当前得票数（可能相对于VoteClickNumberService记录的数据较小）
   */
  @Excel(name = "总点击量")
  private int currentNumber;
}
