package xyz.hdzx.core.vo;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import xyz.hdzx.core.base.VoteInfoBase;

/**
 * 用来展示修改投票数据和具体所有投票信息数据
 *
 * @author whq Date: 2018-09-29 Time: 20:58
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class VoteAllInfoVo extends VoteInfoBase {

    /**
     * 投票总数
     */
    private int totalNumber;

    private int voteAmount;

    /**
     * 被投票用户信息
     */
    private List<VoteItemInfoVo> users;

}
