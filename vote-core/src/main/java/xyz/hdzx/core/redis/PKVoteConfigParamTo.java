package xyz.hdzx.core.redis;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author whq
 */
@Setter
@Getter
@ToString
public class PKVoteConfigParamTo {

  private String id;

  private String title;

  private String brief;

  private String content;

  private String bgUrl;

  private String interactiveModuleName;

  private int interactiveModuleId;

  /**
   * 每人最高投票次数
   */
  private int voteAmount;

  private String groupId;

  private double scala;

  private int poll;
}
