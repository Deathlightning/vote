package xyz.hdzx.core.redis;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import xyz.hdzx.core.base.VoteInfoBase;
import xyz.hdzx.core.vo.VoteItemInfoVo;

/**
 * 用于储存投票的所有需要在前端展示的信息
 *
 * @author whq Date: 2018-09-29 Time: 21:32
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class VoteConfigTo extends VoteInfoBase {

    /**
     * 投票次数
     */
    private int voteAmount;

    /**
     * 投票用户数据
     */
    private List<VoteItemInfoVo> users;
}
