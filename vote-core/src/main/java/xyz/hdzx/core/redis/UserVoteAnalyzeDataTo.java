package xyz.hdzx.core.redis;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author whq Date: 2018-09-29 Time: 21:58
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserVoteAnalyzeDataTo {

    /**
     * 用户id
     */
    private String id;

    /**
     * 占用比例
     */
    private double scale;

    /**
     * 当前得票数（可能相对于VoteClickNumberService记录的数据较小）
     */
    private int currentNumber;

    /**
     * 是否为最高
     */
    private boolean isMaximum;
}
