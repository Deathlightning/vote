package xyz.hdzx.core.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import xyz.hdzx.core.bean.RestResult;
import xyz.hdzx.core.bean.ResultBean;

/**
 * @author whq Date: 2018-10-06 Time: 17:37
 */
@Slf4j
@Aspect
@Component
public class ResultBeanAspect {

    public ResultBeanAspect() {
        System.out.println("ResultBeanAspect");
    }

    @Around("execution(public xyz.hdzx.core.bean.ResultBean *(..))")
    public Object handlerControllerMethod(ProceedingJoinPoint pjp) {
        long startTime = System.currentTimeMillis();

        ResultBean<?> result = null;
        try {
            result = (ResultBean<?>) pjp.proceed();
            log.info("{} use time: {}", pjp.getSignature(), System.currentTimeMillis() - startTime);
        } catch (Throwable e) {
            log.error("e: {}", e);
            // 处理异常
            // result = handleException(pjp, e);
          result = new ResultBean<>();
          result.setMsg(e.getMessage());
        }
        return result;
    }

}
