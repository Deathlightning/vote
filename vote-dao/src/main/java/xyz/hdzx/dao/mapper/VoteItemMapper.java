package xyz.hdzx.dao.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xyz.hdzx.core.model.VoteItem;
import xyz.hdzx.core.model.VoteItemExample;

@Mapper
public interface VoteItemMapper {
    int countByExample(VoteItemExample example);

    int deleteByExample(VoteItemExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(VoteItem record);

    int insertSelective(VoteItem record);

    List<VoteItem> selectByExample(VoteItemExample example);

    VoteItem selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") VoteItem record,
        @Param("example") VoteItemExample example);

    int updateByExample(@Param("record") VoteItem record,
        @Param("example") VoteItemExample example);

    int updateByPrimaryKeySelective(VoteItem record);

    int updateByPrimaryKey(VoteItem record);
}