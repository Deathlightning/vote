package xyz.hdzx.dao.mapper;


import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xyz.hdzx.core.model.VoteInfo;
import xyz.hdzx.core.model.VoteInfoExample;

@Mapper
public interface VoteInfoMapper {
    int countByExample(VoteInfoExample example);

    int deleteByExample(VoteInfoExample example);

    int deleteByPrimaryKey(String id);

    int insert(VoteInfo record);

    int insertSelective(VoteInfo record);

    List<VoteInfo> selectByExample(VoteInfoExample example);

    VoteInfo selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") VoteInfo record,
        @Param("example") VoteInfoExample example);

    int updateByExample(@Param("record") VoteInfo record, @Param("example") VoteInfoExample example);

    int updateByPrimaryKeySelective(VoteInfo record);

    int updateByPrimaryKey(VoteInfo record);
}