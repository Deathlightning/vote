/*
 Navicat Premium Data Transfer

 Source Server         : 309-2
 Source Server Type    : MySQL
 Source Server Version : 50723
 Source Host           : 10.32.6.133:3306
 Source Schema         : vote

 Target Server Type    : MySQL
 Target Server Version : 50723
 File Encoding         : 65001

 Date: 09/10/2018 22:12:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for vote_info
-- ----------------------------
DROP TABLE IF EXISTS `vote_info`;
CREATE TABLE `vote_info` (
  `id`                      int(11)      NOT NULL AUTO_INCREMENT
  COMMENT '投票id',
  `title`                   varchar(50) CHARACTER SET utf8
  COLLATE utf8_bin                       NOT NULL
  COMMENT '投票名称',
  `content`                 varchar(2048) CHARACTER SET utf8
  COLLATE utf8_bin                       NULL     DEFAULT NULL
  COMMENT '投票说明内容',
  `brief`                   varchar(512) CHARACTER SET utf8
  COLLATE utf8_bin                       NULL     DEFAULT NULL
  COMMENT '投票简介',
  `type`                    tinyint(2)   NOT NULL
  COMMENT '投票类型，1普通投票，2pk投票，3 自定义投票',
  `start_time`              timestamp(0) NULL     DEFAULT NULL
  COMMENT '开始时间',
  `end_time`                timestamp(0) NULL     DEFAULT NULL
  COMMENT '结束时间',
  `frequency_id`            int(11)      NOT NULL
  COMMENT '管理员所属的频率id',
  `program_id`              int(11)      NOT NULL
  COMMENT '管理员所属的栏目id',
  `interactive_module_name` varchar(50) CHARACTER SET utf8
  COLLATE utf8_bin                       NULL     DEFAULT NULL
  COMMENT '互动模块名',
  `interactive_module_id`   int(11)      NULL     DEFAULT NULL
  COMMENT '互动模块id',
  `view_num`                int(11)      NULL     DEFAULT NULL
  COMMENT '浏览量',
  `total_click`             int(11)      NULL     DEFAULT NULL
  COMMENT '总点击量',
  `max_click`               int(11)      NULL     DEFAULT NULL
  COMMENT '最高点击量',
  `max_user_id`             int(11)      NULL     DEFAULT NULL
  COMMENT '最高点击量的用户id',
  `max_name`                varchar(50) CHARACTER SET utf8
  COLLATE utf8_bin                       NULL     DEFAULT NULL
  COMMENT '最高点击量的用户名',
  `creator`                 varchar(50) CHARACTER SET utf8
  COLLATE utf8_bin                       NOT NULL
  COMMENT '创建人',
  `create_time`             timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP
  COMMENT '创建时间',
  `updater`                 varchar(50) CHARACTER SET utf8
  COLLATE utf8_bin                       NULL     DEFAULT NULL
  COMMENT '更新人',
  `update_time`             timestamp(0) NULL     DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP(0)
  COMMENT '更新时间',
  `status`                  tinyint(2)   NOT NULL DEFAULT 1
  COMMENT '状态，1 即将开始，2已经开始 ，3 已经结束',
  `del`                     tinyint(2)   NULL     DEFAULT 1
  COMMENT '是否删除，1 正常 2 删除',
  `vote_amount`             tinyint(2)   NULL     DEFAULT 1
  COMMENT '投票次数',
  `photo_url`               varchar(64) CHARACTER SET utf8
  COLLATE utf8_bin                       NULL     DEFAULT NULL
  COMMENT '背景图片url',
  `data`                    varchar(1024) CHARACTER SET utf8
  COLLATE utf8_bin                       NULL     DEFAULT NULL
  COMMENT '冗余数据处理',
  PRIMARY KEY (`id`) USING BTREE
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8
  COLLATE = utf8_bin
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for vote_item
-- ----------------------------
DROP TABLE IF EXISTS `vote_item`;
CREATE TABLE `vote_item` (
  `id`          int(11)      NOT NULL
  COMMENT 'id',
  `vote_id`     int(11)      NOT NULL
  COMMENT '投票id',
  `name`        varchar(50) CHARACTER SET utf8
  COLLATE utf8_bin           NOT NULL
  COMMENT '投票项名称',
  `photo_url`   varchar(64) CHARACTER SET utf8
  COLLATE utf8_bin           NOT NULL
  COMMENT '图片url',
  `uid`         varchar(25) CHARACTER SET utf8
  COLLATE utf8_bin           NULL DEFAULT NULL
  COMMENT '用户id',
  `brief`       varchar(512) CHARACTER SET utf8
  COLLATE utf8_bin           NULL DEFAULT NULL
  COMMENT '简介',
  `vote_num`    int(11)      NULL DEFAULT NULL
  COMMENT '得到票数',
  `status`      tinyint(2)   NULL DEFAULT 1
  COMMENT '状态 1 正常 2 删除 3 禁用',
  `creator`     varchar(50) CHARACTER SET utf8
  COLLATE utf8_bin           NULL DEFAULT NULL
  COMMENT '创建人',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP
  COMMENT '创建时间',
  `updater`     varchar(50) CHARACTER SET utf8
  COLLATE utf8_bin           NULL DEFAULT NULL
  COMMENT '更新人',
  `update_time` timestamp(0) NULL DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP(0)
  COMMENT '更新时间',
  `data`        varchar(1024) CHARACTER SET utf8
  COLLATE utf8_bin           NULL DEFAULT NULL
  COMMENT '冗余数据储存',
  PRIMARY KEY (`id`) USING BTREE
)
  ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_bin
  ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
