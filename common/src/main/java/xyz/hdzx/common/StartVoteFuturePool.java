package xyz.hdzx.common;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

/**
 * @author whq Date: 2018-09-27 Time: 22:08
 */
public class StartVoteFuturePool {

    /**
     * 用来指定存储的集合
     */
    public enum ScheduledType{
        /**
         * 代表开始map
         */
        START,

        /**
         * 代表结束map
         */
        END
    }

    private static final Map<String, ScheduledFuture> START = new ConcurrentHashMap<>();
    private static final Map<String, ScheduledFuture> END = new ConcurrentHashMap<>();

    public static void push(String voteId, ScheduledFuture scheduledFuture, ScheduledType type) {

        if (ScheduledType.START == type){
            START.put(voteId, scheduledFuture);
        } else if (ScheduledType.END == type){
            END.put(voteId, scheduledFuture);
        }
    }

    public static ScheduledFuture get(String voteId, ScheduledType type) {
        ScheduledFuture scheduledFuture = null;

        if (ScheduledType.START == type){
            scheduledFuture = START.get(voteId);
        } else if (ScheduledType.END == type){
            scheduledFuture = END.get(voteId);
        }

        return scheduledFuture;
    }

    public static ScheduledFuture remove(String voteId, ScheduledType type) {

        ScheduledFuture scheduledFuture = null;
        if (ScheduledType.START == type){
            scheduledFuture = START.remove(voteId);
        } else if (ScheduledType.END == type){
            scheduledFuture = END.remove(voteId);
        }

        return scheduledFuture;
    }
}
