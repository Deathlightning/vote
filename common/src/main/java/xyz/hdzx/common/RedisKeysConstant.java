package xyz.hdzx.common;

/**
 * @author whq Date: 2018-09-27 Time: 20:39
 */
public class RedisKeysConstant {


    /**
     * 投票id列表
     */
    public static final String VOTE_ID_LIST = "VoteIdList";

    /**
     * 用来存储投票页面信息, 存入一个map
     * url：key
     * page：value
     */
    public static final String VOTE_PAGE = "VotePageMap";

    /**
     * 投票配置map
     */
    public static final String VOTE_CONFIG = "VoteConfigMap";

    /**
     * 投票的状态
     */
    public static final String VOTE_STATUS_MAP = "VoteStatusMap";

}
