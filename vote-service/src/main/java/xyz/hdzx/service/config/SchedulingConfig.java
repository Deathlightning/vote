package xyz.hdzx.service.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author whq Date: 2018-09-30 Time: 14:43
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "task.pool.schedule")
public class SchedulingConfig {

    private int corePoolSize;

    private int maxPoolSize;

    private int keepAliveSeconds;

    private int queueCapacity;
}
