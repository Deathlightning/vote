package xyz.hdzx.service.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author whq Date: 2018-09-30 Time: 14:47
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "task.pool.async")
public class AsyncThreadConfig {

    private int corePoolSize;

    private int maxPoolSize;

    private int keepAliveSeconds;

    private int queueCapacity;
}
