package xyz.hdzx.service;

import java.util.List;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;

/** @author whq Date: 2018-09-28 Time: 13:11 */
public interface VoteUserClickScoreService {

  /**
   * 添加一个元素
   *
   * @param voteId voteId
   * @param userId userId
   * @param score score
   */
  void add(String voteId, String userId, Long score);

  /**
   * 用于一次添加多个元素，票数默认时0
   *
   * @param voteId voteId
   * @param userIds userIds
   */
  void addAll(String voteId, String... userIds);

  /**
   * 得到所有数据
   *
   * @param voteId voteId
   * @return 返回所有数据
   *     <p>String getValue();
   *     <p>Double getScore();
   */
  List<TypedTuple<String>> getAll(String voteId);

  /**
   * 得到用户的票数
   *
   * @param voteId voteId
   * @param userId userId
   * @return return
   */
  Long getScore(String voteId, String userId);

  /**
   * 得到最高票数的用户信息
   *
   * @param voteId voteId
   * @return return
   */
  TypedTuple<String> getMaxScore(String voteId);

  /**
   * 增加一个用户的票数
   *
   * @param voteId voteId
   * @param userId userId
   */
  void incrScore(String voteId, String userId);

  /**
   * 清理所有数据
   *
   * @param voteId voteId
   */
  void clear(String voteId);

  /**
   * 用于修改票数
   *
   * @param voteId 投票id
   * @param userId 投票项id
   * @param score 修改的数据
   */
  void addScore(String voteId, String userId, Long score);

  void removeOne(String voteId, String userId);

  boolean isExits(String id, Integer userId);
}
