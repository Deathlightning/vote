package xyz.hdzx.service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;

/**
 * K 指以hash结构操作时 键类型
 *  T 为数据实体 应实现序列化接口,并定义serialVersionUID
 *  RedisTemplate 提供了五种数据结构操作类型 hash /
 *  list / set / zset / value
 *  方法命名格式为 数据操作类型 + 操作 如 hashPut 指以hash结构(也就是map)向key添加键值对
 * @author whq Date: 2018-09-27 Time: 17:20
 */
public interface RedisHelper<HK, T> {

    /**
     * key-value键值对，设置key-value
     * @param key key
     * @param value value
     */
    void setValue(String key, T value);

    /**
     * key-value对的增加操作，原子性操作值
     * @param key key
     */
    void incrValue(String key);

    /**
     * key-value对，通过key得到值
     * @param key key
     * @return value
     */
    T getValue(String key);

    /**
     * Hash结构 添加元素
     * @param key key
     * @param hashKey hashKey
     * @param domain 元素
     */
    void hashPut(String key, HK hashKey, T domain);

    /**
     * Hash结构 获取指定key所有键值对
     * @param key key
     * @return return
     */
    Map<HK, T> hashFindAll(String key);

    /**
     * Hash结构 获取单个元素
     * @param key key
     * @param hashKey hashKey
     * @return return
     */
    T hashGet(String key, HK hashKey);

    /**
     * 移除hash中的键值
     * @param key key
     * @param hashKey hashKey
     */
    void hashRemove(String key, HK hashKey);

    /**
     * List结构 向尾部(Right)添加元素
     * @param key key
     * @param domain domain
     * @return return
     */
    Long listPush(String key, T domain);

    /**
     * List结构 向头部(Left)添加元素
     * @param key key
     * @param domain domain
     * @return return
     */
    Long listUnShift(String key, T domain);

    /**
     * List结构 获取所有元素
     * @param key key
     * @return 所有数据
     */
    List<T> listFindAll(String key);

    /**
     * 移除list中的domain
     * @param key key
     * @param count 第几个
     * @param domain domain
     */
    void listRemove(String key, int count, T domain);

    /**
     * List结构 移除并获取数组第一个元素
     * @param key key
     * @return return
     */
    T listLPop(String key);

    /**
     * 移除key
     * @param key key
     */
    void remove(String key);

    /**
     * zSet 里面添加一个数据
     * @param key key
     * @param score score
     * @param value value
     * @return return
     */
    Boolean zSetPush(String key, Long score, T value);

    /**
     * 得到并删除第一个数据
     * @param key key
     * @return data
     */
    T zSetPop(String key);

    /**
     * 移除zSet里的一条数据
     * @param key key
     * @param hKey hKey
     */
    void zSetRemove(String key, String hKey);

    /**
     * 返回一个zSet的所有值
     *
     * @param key key
     * @return return
     */
    Set<TypedTuple<T>> zSetAll(String key);

    /**
     * 增加指定值的分数
     * @param key key
     * @param score score
     * @param value value
     */
    void zSetAddScore(String key, Long score, T value);

    /**
     * 得到zSet的value的分数
     * @param key key
     * @param value value
     * @return score
     */
    long zSetGetScore(String key, T value);

    /**
     * 得到zSet的value的分数
     *
     * @param key key
     * @return 最高数据用户 或者 null
     */
    TypedTuple<T> zSetGetMaxScore(String key);

    /**
     * 判断是否一个key
     * @param key key
     * @return true or false
     */
    Boolean exist(String key);

    /**
     * 添加一个数据
     * @param key key
     * @param value value
     * @return return
     */
    Long setPush(String key, T value);

    /**
     * 得到并删除第一个数据
     * @param key key
     * @return value
     */
    T setPop(String key);

    /**
     * 判断在key里面是否存在value
     * @param key key
     * @param value value
     * @return return
     */
    Boolean isExistInSet(String key, T value);

    /**
     * 设置过期时间
     * @param key 键
     * @param timeout 时间
     * @param timeUnit 时间单位
     * @return return
     */
    Boolean expire(String key, long timeout, TimeUnit timeUnit);

    /**
     * 判断在zKey里面是否存在value
     *
     * @param key 投票id
     * @param openId openId
     */
    boolean isExistInZSet(String key, T openId);
}
