package xyz.hdzx.service;

import xyz.hdzx.core.model.enums.VoteStatus;

/**
 * @author whq Date: 2018-10-10 Time: 15:38
 */
public interface VoteStatusRedisService {

    /**
     * 设置投票的状态
     *
     * @param voteId voteId
     * @param status status
     */
    void put(String voteId, VoteStatus status);

    /**
     * 得到投票的状态
     *
     * @param voteId voteId
     * @return 投票的状态
     */
    VoteStatus get(String voteId);

    /**
     * 删除投票
     *
     * @param voteId 投票id
     */
    void remove(String voteId);

}
