package xyz.hdzx.service;

import java.util.Map;
import xyz.hdzx.core.redis.UserVoteAnalyzeDataTo;

/**
 * @author whq Date: 2018-09-29 Time: 22:04
 */
public interface VoteAnalyzeService {

    /**
     * 读取指定投票得数据并进行分析
     *
     * @param voteId voteId
     */
    void analyze(String voteId);

    /**
     * 得到一个用户的分析数据
     *
     * @param voteId voteId
     * @param openId openId
     * @return 返回一个数据to对象
     */
    UserVoteAnalyzeDataTo getOneResult(String voteId, String openId);

    /**
     * 得到所有用户的分析数据集合
     *
     * @param voteId voteId
     * @return 返回所有to对象
     */
    Map<String, UserVoteAnalyzeDataTo> getAllResult(String voteId);

    /**
     * 清理数据
     *
     * @param voteId voteId
     */
    void clear(String voteId);
}
