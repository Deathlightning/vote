package xyz.hdzx.service;

import java.util.List;

/**
 * @author whq Date: 2018-09-28 Time: 12:49
 *
 * 用来保存投票id的列表
 */
public interface CurrentVoteIdListService {

    /**
     * 添加一个voteId
     * @param voteId voteId
     */
    void add(String voteId);

    /**
     * 翻译列表中的所有值
     * @return return
     */
    List<String> get();

    /**
     * 删除一个voteId
     * @param voteId voteId
     */
    void remove(String voteId);

  void removeAll(List<String> list);

    /**
     * 删除所有值
     */
    void removeAll();
}
