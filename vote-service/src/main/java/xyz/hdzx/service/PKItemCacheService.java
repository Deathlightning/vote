package xyz.hdzx.service;

import java.util.List;
import java.util.Map;
import xyz.hdzx.core.redis.PKVoteConfigParamTo;

/**
 * @author whq
 */
public interface PKItemCacheService {

  void putOneGroup(String key, List<PKVoteConfigParamTo> paramTos);

  void putOneGroup(String key, String groupId, PKVoteConfigParamTo paramTos);

  List<PKVoteConfigParamTo> getOneGroup(String key, String groupId);

  Map<String, List<PKVoteConfigParamTo>> getAllGroup(String key);

  void clear(String key);

  void removeOneGroup(String key, String groupId);
}
