package xyz.hdzx.service;

import java.util.List;
import xyz.hdzx.core.entity.VoteItemExt;
import xyz.hdzx.core.model.VoteItem;
import xyz.hdzx.core.param.PKVoteItemParam;

/**
 * @author whq
 */
public interface VoteItemService {

  void insertOneGroup4PK(List<PKVoteItemParam> params, String voteId);

  List<List<VoteItemExt>> getAllInfoByVoteId(String voteId);

  void updateOneGroup4PK(List<PKVoteItemParam> params, String id, String groupId);

  int deleteOneGroup(String id, String groupId);

  void updateByItemId(VoteItem voteItem);

  List<VoteItem> getAllByVoteId(String id);

  void insertOneUser(VoteItem voteItem);

  String insertOneItem2PK(PKVoteItemParam param, String id);
}
