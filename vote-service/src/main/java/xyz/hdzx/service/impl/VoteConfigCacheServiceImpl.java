package xyz.hdzx.service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import xyz.hdzx.common.RedisKeysConstant;
import xyz.hdzx.core.redis.VoteConfigTo;
import xyz.hdzx.service.VoteConfigCacheService;

/**
 * @author whq Date: 2018-09-29 Time: 21:50
 */
@Service("voteConfigCacheServiceImpl")
public class VoteConfigCacheServiceImpl implements VoteConfigCacheService {

    @Resource
    private RedisHelperImpl<String, VoteConfigTo> redisHelperImpl;

    @Override
    public void put(String key, VoteConfigTo voteConfigTo) {
        redisHelperImpl.hashPut(RedisKeysConstant.VOTE_CONFIG, key, voteConfigTo);
    }

    @Override
    public VoteConfigTo get(String key) {
        return redisHelperImpl.hashGet(RedisKeysConstant.VOTE_CONFIG, key);
    }

    @Override
    public void clear(String key) {
        redisHelperImpl.hashRemove(RedisKeysConstant.VOTE_CONFIG, key);
    }
}
