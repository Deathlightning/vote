package xyz.hdzx.service.impl;

import java.util.concurrent.TimeUnit;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.support.spring.GenericFastJsonRedisSerializer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import xyz.hdzx.service.RedisHelper;

/**
 * @author whq Date: 2018-09-27 Time: 17:22
 */
@Service("redisHelperImpl")
public class RedisHelperImpl<HK, T> implements RedisHelper<HK, T> {

    private RedisTemplate<String, T> redisTemplate;

    private HashOperations<String, HK, T> hashOperations;
    private ListOperations<String, T> listOperations;
    private ZSetOperations<String, T> zSetOperations;
    private SetOperations<String, T> setOperations;
    private ValueOperations<String, T> valueOperations;


    @Autowired
    public RedisHelperImpl(RedisTemplate<String, T> redisTemplate) {

        this.redisTemplate = redisTemplate;

        this.hashOperations = redisTemplate.opsForHash();
        this.listOperations = redisTemplate.opsForList();
        this.zSetOperations = redisTemplate.opsForZSet();
        this.setOperations = redisTemplate.opsForSet();
        this.valueOperations = redisTemplate.opsForValue();

        init();
    }

    /**
     *  处理redis不能使用fastjson处理map对象
     */
    private void init() {
        GenericFastJsonRedisSerializer fastJsonRedisSerializer = new GenericFastJsonRedisSerializer();
        ParserConfig.getGlobalInstance().addAccept("xyz.hdzx.");

        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(fastJsonRedisSerializer);
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(fastJsonRedisSerializer);
        redisTemplate.setHashValueSerializer(fastJsonRedisSerializer);

        redisTemplate.afterPropertiesSet();
    }

    @Override
    public void setValue(String key, T value){
        valueOperations.set(key, value);
    }

    @Override
    public void incrValue(String key){
      valueOperations.increment(key, 1L);
    }

    @Override
    public T getValue(String key){
        return valueOperations.get(key);
    }

    @Override
    public void hashPut(String key, HK hashKey, T domain) {
        hashOperations.put(key, hashKey, domain);
    }

    @Override
    public Map<HK, T> hashFindAll(String key) {
        return hashOperations.entries(key);
    }

    @Override
    public T hashGet(String key, HK hashKey) {
        return hashOperations.get(key, hashKey);
    }

    @Override
    public void hashRemove(String key, HK hashKey) {
        hashOperations.delete(key, hashKey);
    }

    @Override
    public Long listPush(String key, T domain) {
        return listOperations.rightPush(key, domain);
    }

    @Override
    public Long listUnShift(String key, T domain) {
        return listOperations.leftPush(key, domain);
    }

    @Override
    public List<T> listFindAll(String key) {
        if (redisTemplate.hasKey(key)) {
            final Long size = listOperations.size(key);
            if (size >= 0) {
                return listOperations.range(key, 0, size);
            }
        }
        return null;
    }

    @Override
    public void listRemove(String key, int count, T domain) {
        listOperations.remove(key, count, domain);
    }

    @Override
    public T listLPop(String key) {
        return listOperations.leftPop(key);
    }

    @Override
    public void remove(String key) {
        redisTemplate.delete(key);
    }

    @Override
    public Boolean zSetPush(String key, Long score, T value) {
        // 如果存在更新分数，不存在放入
        return zSetOperations.add(key, value, score);
    }

    @Override
    public T zSetPop(String key) {
        Set<T> range = zSetOperations.range(key, 0L, 0L);
        T result = null;

        if (!CollectionUtils.isEmpty(range)) {
            Iterator<T> iterator = range.iterator();
            if (iterator.hasNext()) {
                result = iterator.next();
            }
        }
        // 删除元素
        zSetOperations.remove(key, result);
        return result;
    }

    @Override
    public void zSetRemove(String key, String hKey){
        zSetOperations.remove(key, hKey);
    }

    @Override
    public Set<TypedTuple<T>> zSetAll(String key) {

        Set<TypedTuple<T>> typedTuples = null;

        if (redisTemplate.hasKey(key) != null) {
            Long size = zSetOperations.size(key);

            if (size == null) {
                size = 0L;
            }

            if (size >= 0) {
                typedTuples = zSetOperations.rangeWithScores(key, 0, size);
            }
        }
        return typedTuples;
    }

    @Override
    public void zSetAddScore(String key, Long score, T value) {
        zSetOperations.incrementScore(key, value, score);
    }

    @Override
    public long zSetGetScore(String key, T value){
        Double score = zSetOperations.score(key, value);

        return score != null && score > 0 ? score.longValue() : 0L;
    }

    @Override
    public TypedTuple<T> zSetGetMaxScore(String key) {
        final Set<TypedTuple<T>> typedTuples = zSetOperations.rangeWithScores(key, 0L, 0L);
        if (typedTuples.size() != 0) {
            return typedTuples.iterator().next();
        }

        return null;
    }

    @Override
    public Boolean exist(String key) {

        return redisTemplate.hasKey(key);
    }

    @Override
    public Long setPush(String key,T value) {
        if (value != null && key != null) {
            return setOperations.add(key, value);
        }
        return 0L;
    }

    @Override
    public T setPop(String key) {
        return setOperations.pop(key);
    }

    @Override
    public Boolean isExistInSet(String key, T value) {
        return setOperations.isMember(key, value);
    }

    @Override
    public Boolean expire(String key, long timeout, TimeUnit timeUnit) {

        return redisTemplate.expire(key, timeout, timeUnit);
    }

    @Override
    public boolean isExistInZSet(String key, T openId) {

        final Long isExist = zSetOperations.rank(key, openId);

        return isExist != null;
    }
}
