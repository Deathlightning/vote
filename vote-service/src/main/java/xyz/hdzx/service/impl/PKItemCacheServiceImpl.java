package xyz.hdzx.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import xyz.hdzx.core.redis.PKVoteConfigParamTo;
import xyz.hdzx.service.RedisHelper;
import xyz.hdzx.service.PKItemCacheService;

/**
 * @author whq
 */
@Service("pKItemCacheServiceImpl")
public class PKItemCacheServiceImpl implements PKItemCacheService {

    @Resource
    private RedisHelper<String, List<PKVoteConfigParamTo>> redisHelperImpl;

    @Override
    public void putOneGroup(String key, List<PKVoteConfigParamTo> paramTos) {
      redisHelperImpl.hashPut(key + "-group", paramTos.get(0).getGroupId(), paramTos);
    }

  @Override
  public void putOneGroup(String key, String groupId, PKVoteConfigParamTo paramTos) {
    final List<PKVoteConfigParamTo> pkVoteConfigParamTos = redisHelperImpl
      .hashGet(key + "-group", groupId);
    if (pkVoteConfigParamTos == null){
      final ArrayList<PKVoteConfigParamTo> list = new ArrayList<>();
      list.add(paramTos);
      redisHelperImpl.hashPut(key + "-group", groupId, list);
      return;
    }
    pkVoteConfigParamTos.add(paramTos);
    redisHelperImpl.hashPut(key + "-group", groupId, pkVoteConfigParamTos);
  }

  @Override
    public List<PKVoteConfigParamTo> getOneGroup(String key, String groupId) {
      return redisHelperImpl.hashGet(key + "-group", groupId);
    }

  @Override
  public Map<String, List<PKVoteConfigParamTo>> getAllGroup(String key) {

    return redisHelperImpl.hashFindAll(key + "-group");
  }

    @Override
    public void clear(String key) {
      redisHelperImpl.remove(key + "-group");
    }

  @Override
  public void removeOneGroup(String key, String groupId) {
    redisHelperImpl.hashRemove(key + "-group", groupId);
  }
}
