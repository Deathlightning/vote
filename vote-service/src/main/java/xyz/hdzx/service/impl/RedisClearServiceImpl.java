package xyz.hdzx.service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import xyz.hdzx.service.RedisHelper;

/**
 * @author whq Date: 2018-09-29 Time: 22:17
 */
@Service("redisClearServiceImpl")
public class RedisClearServiceImpl {

    @Resource
    private RedisHelper<String, String> redisHelperImpl;

    public void clear(String key) {
        redisHelperImpl.remove(key);
    }

}
