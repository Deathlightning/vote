package xyz.hdzx.service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import xyz.hdzx.common.RedisKeysConstant;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.service.RedisHelper;
import xyz.hdzx.service.VoteStatusRedisService;

/**
 * @author whq Date: 2018-10-10 Time: 16:21
 */
@Service("voteStatusRedisServiceImpl")
public class VoteStatusRedisServiceImpl implements VoteStatusRedisService {


    @Resource
    private RedisHelper<String, Byte> redisHelperImpl;

    @Override
    public void put(String voteId, VoteStatus status) {

        redisHelperImpl.hashPut(RedisKeysConstant.VOTE_STATUS_MAP, voteId, status.getValue());
    }

    @Override
    public VoteStatus get(String voteId) {

        final Byte status = redisHelperImpl
            .hashGet(RedisKeysConstant.VOTE_STATUS_MAP, voteId);
        if (status == null) {
            return VoteStatus.OVER;
        }

        final VoteStatus voteStatus = VoteStatus.getStatus(status);

        if (voteStatus == null) {
            return VoteStatus.OVER;
        }

        return voteStatus;
    }

    @Override
    public void remove(String voteId) {
        redisHelperImpl.hashRemove(RedisKeysConstant.VOTE_STATUS_MAP, voteId);
    }
}
