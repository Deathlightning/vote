package xyz.hdzx.service.impl;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import xyz.hdzx.core.bean.Page;
import xyz.hdzx.core.bean.ResultBean;
import xyz.hdzx.core.model.VoteInfo;
import xyz.hdzx.core.model.VoteInfoExample;
import xyz.hdzx.core.model.VoteInfoExample.Criteria;
import xyz.hdzx.core.model.VoteItem;
import xyz.hdzx.core.model.VoteItemExample;
import xyz.hdzx.core.model.enums.DeleteRecordStatus;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.core.model.enums.VoteType;
import xyz.hdzx.core.param.SelectVoteParam;
import xyz.hdzx.core.vo.PluginInfo;
import xyz.hdzx.core.vo.VoteAllInfoVo;
import xyz.hdzx.core.vo.VoteItemInfoVo;
import xyz.hdzx.dao.mapper.VoteInfoMapper;
import xyz.hdzx.dao.mapper.VoteItemMapper;
import xyz.hdzx.service.VoteInfoService;
import xyz.hdzx.service.VoteUserClickScoreService;
import xyz.hdzx.service.function.Transform;

/**
 * @author whq Date: 2018-10-09 Time: 13:55
 */
@Slf4j
@Service("voteInfoServiceImpl")
public class VoteInfoServiceImpl implements VoteInfoService {

    @Resource
    private VoteInfoMapper voteInfoMapper;

    @Resource
    private VoteItemMapper voteItemMapper;

    @Resource
    private VoteUserClickScoreService voteUserClickScoreServiceImpl;

    @Setter
    @Getter
    @Value("${targetUrl}")
    private String targetUrl;

    @Override
    public ResultBean<Page<VoteAllInfoVo>> selectAll(SelectVoteParam param) {

        return getListResultBean(param, null);
    }

    @Override
    public ResultBean<Page<VoteAllInfoVo>> selectStarting(SelectVoteParam param) {

        return getListResultBean(param, VoteStatus.STARTING);
    }

    private ResultBean<Page<VoteAllInfoVo>> getListResultBean(
            SelectVoteParam param, VoteStatus starting) {
        VoteInfoExample example = new VoteInfoExample();

        Criteria criteria = example.createCriteria();

        criteria
                .andFrequencyIdEqualTo(param.getFrequencyId())
                .andProgramIdEqualTo(param.getProgramId())
                .andDelNotEqualTo(DeleteRecordStatus.DELETE.getValue());

        if (starting != null) {
            criteria.andStatusEqualTo(starting.getValue());
        }
        if (null != param.getModuleId()) {
            criteria.andInteractiveModuleIdEqualTo(param.getModuleId());
        }

        example.setDistinct(true);

        final List<VoteInfo> voteInfos = voteInfoMapper.selectByExample(example);
        log.info("voteInfo: {}", voteInfos);

        final Page<VoteAllInfoVo> result = getVoteAllInfoVos(param, voteInfos);

        log.info("result: {}", result);

        return new ResultBean<>(result);
    }

    private Page<VoteAllInfoVo> getVoteAllInfoVos(
            SelectVoteParam param, List<VoteInfo> voteInfoList) {

        List<VoteAllInfoVo> result =
                voteInfoList
                        .stream()
                        .map(
                                v -> {
                                    final VoteAllInfoVo apply = Transform.toVoteAllInfoVo.apply(v);
                                    final VoteItemExample itemsExample = new VoteItemExample();
                                    itemsExample
                                            .createCriteria()
                                            .andVoteIdEqualTo(v.getId())
                                            .andStatusNotEqualTo(Byte.valueOf("3"));
                                    final List<VoteItem> voteItems = voteItemMapper.selectByExample(itemsExample);
                                    final List<VoteItemInfoVo> voteItemInfoVos =
                                            voteItems
                                                    .stream()
                                                    .map(Transform.toVoteItemInfoVo)
                                                    .collect(Collectors.toList());
                                    apply.setUsers(voteItemInfoVos);
                                    return apply;
                                })
                        .collect(Collectors.toList());

        result =
                result
                        .stream()
                        .peek(
                                v ->
                                        v.setUsers(
                                                v.getUsers()
                                                        .stream()
                                                        // .filter(vv -> vv.getPoll() == 0)
                                                        .peek(
                                                                vv -> {
                                                                    if (vv.getPoll() == 0) {
                                                                        vv.setPoll(
                                                                                Integer.valueOf(
                                                                                        voteUserClickScoreServiceImpl
                                                                                                .getScore(v.getId(), vv.getId())
                                                                                                .toString()));
                                                                    }
                                                                })
                                                        .collect(Collectors.toList())))
                        .collect(Collectors.toList());
        log.info("result--search: {}", result);

        // 排序
        result =
                result
                        .stream()
                        .sorted((v1, v2) -> (int) (v2.getCreateTime().getTime() - v1.getCreateTime().getTime()))
                        .collect(Collectors.toList());


        return new Page<>(result, param.getCurrentPage(), param.getPageNum());
    }

    @Override
    public ResultBean<Page<VoteAllInfoVo>> selectComing(SelectVoteParam param) {
        return getListResultBean(param, VoteStatus.COMING);
    }

    @Override
    public ResultBean<Page<VoteAllInfoVo>> selectOver(SelectVoteParam param) {
        return getListResultBean(param, VoteStatus.OVER);
    }

    @Override
    public ResultBean<Page<VoteAllInfoVo>> selectBy(SelectVoteParam param) {

        log.info("param: {}", param);

        /*
         * 如果什么都没有选择，按照全部查询
         */
//    if ((StringUtils.isEmpty(param.getName()))
//        && (param.getStartTime() == null)
//        && (param.getEndTime() == null)) {
//      return selectAll(param);
//    }

        VoteInfoExample example = new VoteInfoExample();

        Criteria criteria = example.createCriteria();

        criteria
                .andFrequencyIdEqualTo(param.getFrequencyId())
                .andProgramIdEqualTo(param.getProgramId())
                .andDelNotEqualTo(DeleteRecordStatus.DELETE.getValue());

        if (null != param.getModuleId()) {
            criteria.andInteractiveModuleIdEqualTo(param.getModuleId());
        }

        if (StringUtils.isNotEmpty(param.getStatus())) {
            criteria.andStatusEqualTo(Byte.valueOf(param.getStatus()));
        }

        if (StringUtils.isNotEmpty(param.getName())) {
            criteria.andTitleLike(
                    "%" + param.getName()
                            .replace("%", "")
                            .replace("&", "")
                            .replace("|", "")
                            .trim()
                            + "%");
        }

        if (param.getStartTime() != null) {
            criteria.andStartTimeGreaterThanOrEqualTo(param.getStartTime());
        }

        if (param.getEndTime() != null) {
            criteria.andEndTimeLessThanOrEqualTo(param.getEndTime());
        }

        example.setDistinct(true);

        final List<VoteInfo> voteInfos = voteInfoMapper.selectByExample(example);

        log.info("voteInfo: {}", voteInfos);

        final Page<VoteAllInfoVo> result = getVoteAllInfoVos(param, voteInfos);

        log.info("result: {}", result);
        return new ResultBean<>(result);
    }

    @Override
    public VoteInfo selectById(String id) {

        return voteInfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public ResultBean deleteVoteInfo(String id) {

        VoteInfo voteInfo = new VoteInfo();
        voteInfo.setId(id);
        voteInfo.setDel(DeleteRecordStatus.DELETE.getValue());

        if (1 == voteInfoMapper.updateByPrimaryKeySelective(voteInfo)) {
            return new ResultBean<>(true);
        }

        return new ResultBean<>(false);
    }

    @Override
    public boolean updateVoteInfoById(VoteInfo voteInfo) {
        if (voteInfo != null) {
            final int i = voteInfoMapper.updateByPrimaryKeySelective(voteInfo);

            return i != 0;
        }

        return false;
    }

    @Override
    public List<PluginInfo> selectByFrequencyId(int frequencyId) {
        VoteInfoExample example = new VoteInfoExample();
        example.createCriteria().andFrequencyIdEqualTo(frequencyId).andEndTimeIsNotNull();

        final List<VoteInfo> voteInfos = voteInfoMapper.selectByExample(example);
        long nowTime = System.currentTimeMillis();
        long timeDifference = (long) 3 * 24 * 60 * 60 * 1000;//三天的毫秒数
        Iterator<VoteInfo> it = voteInfos.iterator();//遍历，清除所有创建时间早于三天的投票。
        while(it.hasNext()){
            VoteInfo voteInfo=it.next();
            long createTime=voteInfo.getCreateTime().getTime();
            if(nowTime-createTime>timeDifference ||voteInfo.getStatus()>2||voteInfo.getDel()==2){
                it.remove();
            }
        }
        return getPluginList(voteInfos);
    }

    @Override
    public List<VoteInfo> notStop() {

        VoteInfoExample example = new VoteInfoExample();
        example
                .createCriteria()
                .andStatusNotEqualTo(VoteStatus.OVER.getValue())
                .andTypeNotEqualTo((byte) VoteType.PK_VOTE.getValue());

        return voteInfoMapper.selectByExample(example);
    }

    @Override
    public List<PluginInfo> selectByModuleId(int moduleId) {
        VoteInfoExample example = new VoteInfoExample();
        example
                .createCriteria()
                .andInteractiveModuleIdEqualTo(moduleId)
                .andStatusNotEqualTo(VoteStatus.OVER.getValue());

        final List<VoteInfo> voteInfos = voteInfoMapper.selectByExample(example);

        log.info("voteInfos: {}", voteInfos);

        return getPluginList(voteInfos);
    }

    @Override
    public String getUrl(String id) {

        return targetUrl + "/goto?id=" + id;
    }

    private List<PluginInfo> getPluginList(List<VoteInfo> voteInfos) {
        return voteInfos
                .stream()
                .map(
                        v -> {
                            PluginInfo pluginInfo = new PluginInfo();
                            if (v.getStartTime() != null) {
                                pluginInfo.setStartTime(v.getStartTime());
                            } else {
                                pluginInfo.setStartTime(v.getCreateTime());
                            }
                            if (v.getEndTime() != null) {
                                pluginInfo.setEndTime(v.getEndTime());
                            } else {
                                pluginInfo.setEndTime(v.getCreateTime());
                            }
                            pluginInfo.setPluginType(v.getType());
                            pluginInfo.setPluginName(v.getTitle());
                            pluginInfo.setId(v.getId());
                            pluginInfo.setModuleName(v.getInteractiveModuleName());
                            return pluginInfo;
                        })
                .collect(Collectors.toList());
    }
}
