package xyz.hdzx.service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import xyz.hdzx.service.RedisHelper;
import xyz.hdzx.service.VoteClickNumberService;

/**
 * @author whq Date: 2018-09-28 Time: 12:35
 */
@Service("voteClickNumberServiceImpl")
public class VoteClickNumberServiceImpl extends RedisClearServiceImpl
  implements VoteClickNumberService {

  @Resource
  private RedisHelper<String, Long> redisHelperImpl;

  @Override
  public void set(String voteIdKey, long value) {
    redisHelperImpl.setValue(voteIdKey + "click", value);
  }

  @Override
  public Long get(String voteIdKey) {
    Number value = redisHelperImpl.getValue(voteIdKey + "click");
    return value == null ? 0 : value.longValue();
  }

  @Override
  public void incrValue(String voteIdKey) {

    redisHelperImpl.incrValue(voteIdKey + "click");
  }
}
