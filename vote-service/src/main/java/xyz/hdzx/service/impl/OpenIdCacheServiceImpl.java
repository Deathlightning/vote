package xyz.hdzx.service.impl;

import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import xyz.hdzx.service.OpenIdCacheService;
import xyz.hdzx.service.RedisHelper;

/**
 * @author whq
 */
@Service("openIdCacheServiceImpl")
public class OpenIdCacheServiceImpl implements OpenIdCacheService {

  @Resource
  private RedisHelper<String, String> redisHelperImpl;

  @Override
  public String getOpenId(String code) {
    return redisHelperImpl.getValue(code);
  }

  @Override
  public void put(String code, String openId) {
    redisHelperImpl.setValue(code, openId);
    redisHelperImpl.expire(code, 1, TimeUnit.HOURS);
  }
}
