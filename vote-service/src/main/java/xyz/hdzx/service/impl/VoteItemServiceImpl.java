package xyz.hdzx.service.impl;

import com.alibaba.fastjson.JSON;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import xyz.hdzx.core.entity.VoteItemExt;
import xyz.hdzx.core.model.VoteItem;
import xyz.hdzx.core.model.VoteItemExample;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.core.param.PKVoteItemParam;
import xyz.hdzx.core.redis.PKVoteConfigParamTo;
import xyz.hdzx.dao.mapper.VoteItemMapper;
import xyz.hdzx.service.VoteClickNumberService;
import xyz.hdzx.service.VoteItemService;
import xyz.hdzx.service.VoteStatusRedisService;
import xyz.hdzx.service.PKItemCacheService;
import xyz.hdzx.service.VoteUserClickScoreService;
import xyz.hdzx.service.task.DefaultGroupCache;

/** @author whq */
@Slf4j
@Service("voteItemServiceImpl")
public class VoteItemServiceImpl implements VoteItemService {

  @Resource private VoteItemMapper voteItemMapper;

  @Resource private PKItemCacheService pKItemCacheServiceImpl;

  @Resource private VoteStatusRedisService voteStatusRedisServiceImpl;

  @Resource private VoteUserClickScoreService voteUserClickScoreServiceImpl;

  @Override
  public void insertOneGroup4PK(List<PKVoteItemParam> params, String voteId) {

    // 以通过一个组id存入数据库
    final String groupId = UUID.randomUUID().toString().replace("-", "");
    final List<VoteItem> result =
        params
            .stream()
            .map(
                v -> {
                  log.info("v-insert: {}", v);
                  final VoteItem voteItem = JSON.parseObject(JSON.toJSONString(v), VoteItem.class);
                  voteItem.setName(v.getTitle());
                  voteItem.setPhotoUrl(v.getBgUrl());
                  voteItem.setVoteId(voteId);
                  voteItem.setData(groupId);
                  voteItem.setStatus(Byte.valueOf("1"));
                  log.info("voteItem: {}", voteItem);
                  voteItemMapper.insertSelective(voteItem);
                  return voteItem;
                })
            .collect(Collectors.toList());

    final List<PKVoteConfigParamTo> values =
        result
            .stream()
            .map(
                v -> {
                  // final PKVoteConfigParamTo pkVoteConfigParamTo = JSON
                  //   .parseObject(JSON.toJSONString(v), PKVoteConfigParamTo.class);
                  PKVoteConfigParamTo pkVoteConfigParamTo = new PKVoteConfigParamTo();
                  pkVoteConfigParamTo.setId(v.getId().toString());
                  pkVoteConfigParamTo.setBrief(v.getBrief());
                  pkVoteConfigParamTo.setBgUrl(v.getPhotoUrl());
                  pkVoteConfigParamTo.setInteractiveModuleId(
                      params.get(0).getInteractiveModuleId());
                  pkVoteConfigParamTo.setInteractiveModuleName(
                      params.get(0).getInteractiveModuleName());
                  pkVoteConfigParamTo.setContent(params.get(0).getContent());
                  pkVoteConfigParamTo.setGroupId(groupId);
                  pkVoteConfigParamTo.setVoteAmount(params.get(0).getVoteAmount());
                  pkVoteConfigParamTo.setTitle(v.getName());
                  return pkVoteConfigParamTo;
                })
            .collect(Collectors.toList());

    log.info("values: {}", values);

    // 缓存数据对象
    pKItemCacheServiceImpl.putOneGroup(voteId, values);
    // 设置状态
    voteStatusRedisServiceImpl.put(groupId, VoteStatus.COMING);
  }

  @Override
  public List<List<VoteItemExt>> getAllInfoByVoteId(String voteId) {

    VoteItemExample example = new VoteItemExample();
    example.createCriteria().andVoteIdEqualTo(voteId).andStatusEqualTo(Byte.valueOf("1"));
    // example.setOrderByClause("data");
    final Map<String, List<VoteItemExt>> collect =
        voteItemMapper
            .selectByExample(example)
            .stream()
            .map(
                v -> {
                  final VoteItemExt voteItem =
                      JSON.parseObject(JSON.toJSONString(v), VoteItemExt.class);
                  final VoteStatus voteStatus = voteStatusRedisServiceImpl.get(v.getData());
                  log.info("voteStatus --> {}", voteStatus.getValue());
                  voteItem.setPkStatus(voteStatus.getValue());
                  voteItem.setDesc(voteStatus.getDesc());

                  if (voteItem.getVoteNum() == 0) {
                    voteItem.setVoteNum(
                        Integer.valueOf(
                            voteUserClickScoreServiceImpl
                                .getScore(voteId, v.getId().toString())
                                .toString()));
                  }
                  return voteItem;
                })
            .collect(Collectors.groupingBy(VoteItem::getData));

    final String groupId = DefaultGroupCache.CACHE.get(voteId);

    log.info("collect.get(groupId): {}", collect.get(groupId));

    if (groupId != null && collect.get(groupId) != null) {
      collect.put(
          groupId,
          collect
              .get(groupId)
              .stream()
              .peek(v -> v.setDefaultGroup(true))
              .collect(Collectors.toList()));
    }

    return collect.keySet().stream().map(collect::get).collect(Collectors.toList());
  }

  @Override
  public void updateOneGroup4PK(List<PKVoteItemParam> params, String id, String groupId) {
    params.forEach(
        v -> {
          log.info("v-update: {}", v);
          final VoteItem voteItem = JSON.parseObject(JSON.toJSONString(v), VoteItem.class);
          voteItem.setName(v.getTitle());
          voteItem.setPhotoUrl(v.getBgUrl());

          VoteItemExample example = new VoteItemExample();
          example
              .createCriteria()
              .andVoteIdEqualTo(id)
              .andDataEqualTo(groupId)
              .andIdEqualTo(v.getId());

          voteItemMapper.updateByExampleSelective(voteItem, example);
        });

    List<PKVoteConfigParamTo> values =
        params
            .stream()
            .map(
                v -> {
                  PKVoteConfigParamTo pkVoteConfigParamTo = new PKVoteConfigParamTo();
                  pkVoteConfigParamTo.setId(id);
                  pkVoteConfigParamTo.setBrief(v.getBrief());
                  pkVoteConfigParamTo.setBgUrl(v.getBgUrl());
                  pkVoteConfigParamTo.setInteractiveModuleId(
                      params.get(0).getInteractiveModuleId());
                  pkVoteConfigParamTo.setInteractiveModuleName(
                      params.get(0).getInteractiveModuleName());
                  pkVoteConfigParamTo.setContent(params.get(0).getContent());
                  pkVoteConfigParamTo.setGroupId(groupId);
                  pkVoteConfigParamTo.setVoteAmount(params.get(0).getVoteAmount());
                  pkVoteConfigParamTo.setTitle(v.getTitle());
                  return pkVoteConfigParamTo;
                })
            .collect(Collectors.toList());
    log.info("values-update: {}", values);
    // 更新缓存
    pKItemCacheServiceImpl.putOneGroup(id, values);
  }

  @Override
  public int deleteOneGroup(String id, String groupId) {

    VoteItemExample example = new VoteItemExample();
    example.createCriteria().andVoteIdEqualTo(id).andDataEqualTo(groupId);

    VoteItem item = new VoteItem();
    // 删除
    item.setStatus(Byte.valueOf("3"));

    pKItemCacheServiceImpl.removeOneGroup(id, groupId);
    return voteItemMapper.updateByExampleSelective(item, example);
  }

  @Override
  public void updateByItemId(VoteItem voteItem) {
    voteItemMapper.updateByPrimaryKeySelective(voteItem);
  }

  @Override
  public List<VoteItem> getAllByVoteId(String id) {
    VoteItemExample exampl = new VoteItemExample();
    exampl.createCriteria().andVoteIdEqualTo(id);
    return voteItemMapper.selectByExample(exampl);
  }

  @Override
  public void insertOneUser(VoteItem voteItem) {
    voteItemMapper.insertSelective(voteItem);
  }

  @Override
  public String insertOneItem2PK(PKVoteItemParam param, String id) {

    if (StringUtils.isNotEmpty(param.getGroupId())) {
      // 以通过一个组id存入数据库
      insertVoteItem(param, id);
      return param.getGroupId();
    }

    String groupId = UUID.randomUUID().toString().replace("-", "");
    param.setGroupId(groupId);
    insertVoteItem(param, id);

    return groupId;
  }

  private void insertVoteItem(PKVoteItemParam param, String id) {
    // 以通过一个组id存入数据库
    log.info("v-insert: {}", param);
    final VoteItem voteItem = JSON.parseObject(JSON.toJSONString(param), VoteItem.class);
    voteItem.setName(param.getTitle());
    voteItem.setPhotoUrl(param.getBgUrl());
    voteItem.setVoteId(id);
    voteItem.setData(param.getGroupId());
    voteItem.setStatus(Byte.valueOf("1"));
    log.info("voteItem: {}", voteItem);
    voteItemMapper.insertSelective(voteItem);

    PKVoteConfigParamTo pkVoteConfigParamTo = new PKVoteConfigParamTo();
    pkVoteConfigParamTo.setId(voteItem.getId().toString());
    pkVoteConfigParamTo.setBrief(param.getBrief());
    pkVoteConfigParamTo.setBgUrl(param.getPhotoUrl());
    pkVoteConfigParamTo.setInteractiveModuleId(param.getInteractiveModuleId());
    pkVoteConfigParamTo.setInteractiveModuleName(param.getInteractiveModuleName());
    pkVoteConfigParamTo.setContent(param.getContent());
    pkVoteConfigParamTo.setGroupId(param.getGroupId());
    pkVoteConfigParamTo.setVoteAmount(param.getVoteAmount());
    pkVoteConfigParamTo.setTitle(param.getName());

    log.info("values: {}", pkVoteConfigParamTo);

    // 缓存数据对象
    pKItemCacheServiceImpl.putOneGroup(id, param.getGroupId(), pkVoteConfigParamTo);

    final VoteStatus voteStatus = voteStatusRedisServiceImpl.get(param.getGroupId());

    log.info("---------> {}", voteItem);
    if (voteStatus == VoteStatus.OVER) {
      // 设置状态
      log.info("新小组加入，设置为即将开始");
      voteStatusRedisServiceImpl.put(param.getGroupId(), VoteStatus.COMING);
    }
  }
}
