package xyz.hdzx.service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import xyz.hdzx.service.RedisHelper;
import xyz.hdzx.service.VoteViewNumberService;

/**
 * @author whq
 */
@Service("voteViewNumberServiceImpl")
public class VoteViewNumberServiceImpl extends RedisClearServiceImpl implements
  VoteViewNumberService {

  @Resource
  private RedisHelper<String, Long> redisHelperImpl;

  @Override
  public void set(String voteIdKey, long value) {
    redisHelperImpl.setValue(voteIdKey + "view", value);
  }

  @Override
  public Long get(String voteIdKey) {
    Number value = redisHelperImpl.getValue(voteIdKey + "view");
    return value == null ? 0L : value.longValue();
  }

  @Override
  public void incrValue(String voteIdKey) {

    redisHelperImpl.incrValue(voteIdKey + "view");
  }
}
