package xyz.hdzx.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import xyz.hdzx.core.redis.UserVoteAnalyzeDataTo;
import xyz.hdzx.service.RedisHelper;
import xyz.hdzx.service.VoteAnalyzeService;
import xyz.hdzx.service.VoteUserClickScoreService;

/**
 * @author whq Date: 2018-09-29 Time: 22:12
 */
@Service("voteAnalyzeServiceImpl")
public class VoteAnalyzeServiceImpl extends RedisClearServiceImpl implements VoteAnalyzeService {

    @Resource
    private RedisHelper<String, UserVoteAnalyzeDataTo> redisHelperImpl;

    @Resource
    private VoteUserClickScoreService voteUserClickScoreServiceImpl;

    @Override
    public synchronized void analyze(String voteId) {
        // 分析数据

        final List<TypedTuple<String>> allUserScore = voteUserClickScoreServiceImpl.getAll(voteId);

      if (allUserScore != null && !CollectionUtils.isEmpty(allUserScore)) {

            final double totalNumber =
                allUserScore
                    .stream()
                    .mapToDouble(
                        x -> x.getScore() != null && x.getScore() >= 0.0 ? x.getScore() : 0.0)
                    .sum();

            final TypedTuple<String> maxScoreUser = voteUserClickScoreServiceImpl
                .getMaxScore(voteId);

            allUserScore.forEach(
                user -> {
                    final String userId = user.getValue();
                    final double scale = notNull(user.getScore()) / totalNumber;

                    final UserVoteAnalyzeDataTo userVoteAnalyzeDataTo = new UserVoteAnalyzeDataTo();

                    userVoteAnalyzeDataTo.setScale(scale);
                    userVoteAnalyzeDataTo.setCurrentNumber(notNull(user.getScore()));

                    if (StringUtils.equals(userId, maxScoreUser.getValue())) {
                        userVoteAnalyzeDataTo.setMaximum(true);
                    } else {
                        userVoteAnalyzeDataTo.setMaximum(false);
                    }

                    redisHelperImpl.hashPut(voteId, userId, userVoteAnalyzeDataTo);
                });
        }
    }

    @Override
    public UserVoteAnalyzeDataTo getOneResult(String voteId, String openId) {
        // 更新数据
        analyze(voteId);

        return redisHelperImpl.hashGet(voteId, openId);
    }

    @Override
    public Map<String, UserVoteAnalyzeDataTo> getAllResult(String voteId) {
        // 更新数据
        analyze(voteId);

        return redisHelperImpl.hashFindAll(voteId);
    }

    private int notNull(Double value) {
        if (value == null) {
            return 0;
        }
        return value.intValue();
  }
}
