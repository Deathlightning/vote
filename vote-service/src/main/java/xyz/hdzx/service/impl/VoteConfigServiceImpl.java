package xyz.hdzx.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ScheduledExecutorService;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import xyz.hdzx.common.CommonConstant;
import xyz.hdzx.core.base.VoteItemInfoBase;
import xyz.hdzx.core.bean.CacheUserInfo;
import xyz.hdzx.core.model.VoteInfo;
import xyz.hdzx.core.model.VoteInfoExample;
import xyz.hdzx.core.model.VoteItem;
import xyz.hdzx.core.model.VoteItemExample;
import xyz.hdzx.core.model.enums.DeleteRecordStatus;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.core.model.enums.VoteType;
import xyz.hdzx.core.param.ConfigVoteParam;
import xyz.hdzx.core.param.PKVoteConfigParam;
import xyz.hdzx.core.param.VoteItemInfoParam;
import xyz.hdzx.core.redis.VoteConfigTo;
import xyz.hdzx.core.vo.VoteConfigVo;
import xyz.hdzx.core.vo.VoteItemInfoVo;
import xyz.hdzx.dao.mapper.VoteInfoMapper;
import xyz.hdzx.dao.mapper.VoteItemMapper;
import xyz.hdzx.service.CurrentVoteIdListService;
import xyz.hdzx.service.VoteClickNumberService;
import xyz.hdzx.service.VoteConfigCacheService;
import xyz.hdzx.service.VoteConfigService;
import xyz.hdzx.service.VoteInfoService;
import xyz.hdzx.service.VoteStatusRedisService;
import xyz.hdzx.service.VoteUserClickScoreService;
import xyz.hdzx.service.VoteViewNumberService;
import xyz.hdzx.service.function.Transform;
import xyz.hdzx.service.task.AsyncTask;

/** @author whq Date: 2018-10-06 Time: 13:46 */
@Slf4j
@Service("voteConfigServiceImpl")
public class VoteConfigServiceImpl implements VoteConfigService {

  @Resource private VoteInfoMapper voteInfoMapper;

  @Resource private VoteItemMapper voteItemMapper;

  @Resource private VoteConfigCacheService voteConfigCacheServiceImpl;

  @Resource private ScheduledExecutorService scheduledExecutorServiceImpl;

  @Resource private VoteStatusRedisService voteStatusRedisServiceImpl;

  @Resource private VoteInfoService voteInfoServiceImpl;

  @Resource private VoteUserClickScoreService voteUserClickScoreServiceImpl;

  @Resource private VoteViewNumberService voteViewNumberServiceImpl;

  @Resource private VoteClickNumberService voteClickNumberServiceImpl;

  @Resource private CurrentVoteIdListService currentVoteIdListServiceImpl;

  @Resource private AsyncTask asyncTask;

  @Override
  public boolean insertOneVote(
      CacheUserInfo userInfo, ConfigVoteParam configVoteParam, VoteStatus status) throws Exception {

    final String uuid = getUUid();
    configVoteParam.setId(uuid);
    log.info("voteId: {}", uuid);

    // if (isModuleExist(configVoteParam.getInteractiveModuleId())) { return false; }

    return isSuccess2Cache(userInfo, configVoteParam, status, false);
  }

  public String isModuleExist(int moduleId) {
    List<VoteInfo> votes = new ArrayList<>();
    if (moduleId > 0) {
      VoteInfoExample example1 = new VoteInfoExample();
      example1
          .createCriteria()
          .andInteractiveModuleIdEqualTo(moduleId)
          .andStatusNotEqualTo(VoteStatus.OVER.getValue());
      votes = voteInfoMapper.selectByExample(example1);
      log.info("模块id得到的数据：{}", votes);
      if (votes.size() > 1) {
        return CommonConstant.ERROR;
      }
    }
    return votes.size() == 0 ? CommonConstant.SUCCESS : votes.get(0).getId();
  }

  private Object setFields(
      CacheUserInfo userInfo,
      ConfigVoteParam configVoteParam,
      VoteStatus status,
      boolean isUpdate) {

    log.info("userInfo: {}", userInfo);

    final VoteInfo voteInfo = Transform.toVoteInfo.apply(configVoteParam);
    voteInfo.setStatus(status.getValue());

    voteInfo.setFrequencyId(userInfo.getCurrentFrequencyId());
    voteInfo.setProgramId(userInfo.getCurrentProgramId());
    voteInfo.setInteractiveModuleName(configVoteParam.getInteractiveModuleName());
    voteInfo.setInteractiveModuleId(configVoteParam.getInteractiveModuleId());

    log.info("bgUrl: {}", configVoteParam.getBgUrl());
    voteInfo.setPhotoUrl(configVoteParam.getBgUrl());

    int i;

    if (isUpdate) {
      voteInfo.setUpdaterid(userInfo.getId().toString());
      voteInfo.setUpdater(userInfo.getName());
      i = voteInfoMapper.updateByPrimaryKeySelective(voteInfo);
      log.info("更新数据：{}", voteInfo);

    } else {
      voteInfo.setCreator(userInfo.getName());
      voteInfo.setCreatorid(userInfo.getId().toString());
      voteInfo.setType(configVoteParam.getType());

      log.info("插入数据：{}", voteInfo);
      i = voteInfoMapper.insertSelective(voteInfo);
    }

    log.info("update result： {}", i);
    final String id = configVoteParam.getId();
    List<VoteItem> voteItemList = Transform.toVoteItem.apply(configVoteParam.getUsers());

    if (i > 0) {
      voteItemList.forEach(
          v -> {
            v.setVoteId(id);
            v.setStatus(Byte.valueOf("1"));
            log.info("item-info: {}", v);
            if (v.getId() != null) {
              log.info("update");
              voteItemMapper.updateByPrimaryKey(v);
            } else {
              log.info("update");
              voteItemMapper.insertSelective(v);
            }
            log.info("vote-item-id: {}", v.getId());
            if (!voteUserClickScoreServiceImpl.isExits(id, v.getId())) {
              voteUserClickScoreServiceImpl.add(id, v.getId().toString(), 0L);
            }
          });
      log.info("更新数据项：{}", voteItemList);
      return "OK";
    } else {

      return null;
    }
  }

  @Override
  public VoteConfigVo selectVoteById(String id) {

    final VoteInfo voteInfo = voteInfoMapper.selectByPrimaryKey(id);
    VoteItemExample example = new VoteItemExample();
    example.createCriteria().andVoteIdEqualTo(id);

    log.info("id：{}", id);
    final List<VoteItem> voteItems = voteItemMapper.selectByExample(example);
    log.info("voteItems: {}", voteItems);

    final List<VoteItemInfoVo> voteItemInfoVoList =
        voteItems
            .stream()
            .filter(Objects::nonNull)
            .map(v -> Transform.toVoteItemInfoVo.apply(v))
            .collect(Collectors.toList());

    final VoteConfigVo configVo = Transform.toVoteConfigVo.apply(voteInfo);
    configVo.setUsers(voteItemInfoVoList);

    return configVo;
  }

  @Override
  public boolean updateVoteById(CacheUserInfo userInfo, ConfigVoteParam configVoteParam)
      throws Exception {

    // 具体逻辑：
    // 删除之前所有的投票项，(重新插入新的投票项，更新投票信息--> cache中插入)
    final String id = configVoteParam.getId();

    VoteItemExample example = new VoteItemExample();
    example.createCriteria().andVoteIdEqualTo(id);

    // 拿到所有已经存在的投票项id
    final List<VoteItem> voteItems = voteItemMapper.selectByExample(example);
    final List<Integer> ids = voteItems.stream().map(VoteItem::getId).collect(Collectors.toList());

    // 得到新增的投票项
    final List<VoteItemInfoParam> resultUsers =
        configVoteParam
            .getUsers()
            .stream()
            .filter(v -> v.getId() == null || ids.contains(Integer.valueOf(v.getId())))
            .collect(Collectors.toList());

    List<Integer> paramIdList =
        configVoteParam
            .getUsers()
            .stream()
            .filter(v -> v.getId() != null)
            .map(VoteItemInfoBase::getId)
            .map(Integer::valueOf)
            .collect(Collectors.toList());

    // 删除参数中没有，但是数据库中存在的投票项
    final List<Integer> deleteIdList =
        ids.stream().filter(v -> !paramIdList.contains(v)).collect(Collectors.toList());

    // 删除新列表里面的用户
    if (!CollectionUtils.isEmpty(deleteIdList)) {
      log.info("删除已经不用的选项： {}", deleteIdList);
      example.clear();
      example.createCriteria().andIdIn(deleteIdList);
      voteItemMapper.deleteByExample(example);

      deleteIdList.forEach(
          v -> {
            voteUserClickScoreServiceImpl.removeOne(id, String.valueOf(v));
          });
    }
    // 需要重新更新的用户列表
    configVoteParam.setUsers(resultUsers);

    final VoteStatus voteStatus = voteStatusRedisServiceImpl.get(configVoteParam.getId());
    log.info("->>>>>>>>>: {}", voteStatus);

    return isSuccess2Cache(userInfo, configVoteParam, voteStatus, true);
  }

  private boolean isSuccess2Cache(
      CacheUserInfo userInfo, ConfigVoteParam configVoteParam, VoteStatus status, boolean isUpdate)
      throws Exception {

    if (checkParam(configVoteParam)) {
      throw new Exception("时间格式不正确");
    }

    if (setFields(userInfo, configVoteParam, status, isUpdate) != null) {
      final VoteConfigVo configVo = selectVoteById(configVoteParam.getId());

      log.info("configVo: {}", configVo);
      log.info("status: {}", status);
      starAsyncTask(voteConfigCacheServiceImpl, configVo);
      log.info("缓存开启成功");

      return true;
    }

    return false;
  }

  private boolean checkParam(ConfigVoteParam param) {

    if (param.getStartTime() == null
      && param.getType().intValue() == VoteType.PK_VOTE.getValue()) {
      return false;
    }
    log.info("startTime: {}", param.getStartTime());
    log.info("endTime: {}", param.getEndTime());
    log.info("now: {}", new Date());
    return !((param.getStartTime().getTime() > System.currentTimeMillis()
            && param.getEndTime().getTime() > param.getStartTime().getTime())
        || (param.getStartTime().getTime() < System.currentTimeMillis()
            && param.getEndTime().getTime() > System.currentTimeMillis()));
  }

  @Override
  public boolean deleteVoteById(String id) {

    VoteInfo voteInfo = new VoteInfo();
    voteInfo.setId(id);
    voteInfo.setDel(DeleteRecordStatus.DELETE.getValue());

    return voteInfoMapper.updateByPrimaryKeySelective(voteInfo) != 0;
  }

  @Override
  public String insertPKVote(CacheUserInfo userInfo, PKVoteConfigParam param) {

    VoteInfo voteInfo = new VoteInfo();

    final String uuid = getUUid();
    voteInfo.setId(uuid);
    log.info("voteId: {}", uuid);
    voteInfo.setPhotoUrl(param.getBgUrl());
    voteInfo.setStatus(VoteStatus.COMING.getValue());
    voteInfo.setTitle(param.getTitle());
    voteInfo.setBrief(param.getBrief());
    voteInfo.setVoteAmount((byte) param.getVoteAmount());
    voteInfo.setType(Byte.valueOf(param.getType()));
    voteInfo.setInteractiveModuleId(param.getInteractiveModuleId());
    voteInfo.setInteractiveModuleName(param.getInteractiveModuleName());
    voteInfo.setContent(param.getContent());
    voteInfo.setCreator(userInfo.getName());
    voteInfo.setVoteAmount(Byte.valueOf(String.valueOf(param.getVoteAmount())));
    voteInfo.setCreatorid(userInfo.getId().toString());
    voteInfo.setFrequencyId(userInfo.getCurrentFrequencyId());
    voteInfo.setProgramId(userInfo.getCurrentProgramId());

    // 插入PK投票信息，由于PK以模块id为单位，所以就特殊字符表示id信息
    voteStatusRedisServiceImpl.put(uuid, VoteStatus.COMING);

    final VoteConfigVo apply = Transform.toVoteConfigVo.apply(voteInfo);
    final VoteConfigTo to = Transform.toVoteConfigTo.apply(apply);
    voteConfigCacheServiceImpl.put(uuid, to);

    return 0 == voteInfoMapper.insertSelective(voteInfo) ? "" : uuid;
  }

  @Override
  public void changeVoteStatus(VoteStatus status, String id) {
    VoteInfo voteInfo = new VoteInfo();
    voteInfo.setId(id);
    voteInfo.setStatus(status.getValue());
    if (status == VoteStatus.STARTING) {
      voteInfo.setStartTime(new Date());
    } else if (status == VoteStatus.OVER) {
      voteInfo.setEndTime(new Date());
    }

    voteInfoMapper.updateByPrimaryKeySelective(voteInfo);
  }

  private void starAsyncTask(
      VoteConfigCacheService voteConfigCacheServiceImpl, VoteConfigVo configVo) {

    asyncTask.startCache(
        configVo,
        voteConfigCacheServiceImpl,
        scheduledExecutorServiceImpl,
        voteStatusRedisServiceImpl,
        voteInfoServiceImpl,
        voteInfoMapper,
        voteUserClickScoreServiceImpl,
        voteViewNumberServiceImpl,
        voteClickNumberServiceImpl,
        voteItemMapper,
        currentVoteIdListServiceImpl);
  }

  private String getUUid() {
    return UUID.randomUUID().toString().replace("-", "");
  }
}
