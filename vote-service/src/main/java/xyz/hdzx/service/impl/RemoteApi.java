package xyz.hdzx.service.impl;

import com.alibaba.fastjson.JSON;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Objects;
import javax.annotation.Resource;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import xyz.hdzx.core.bean.AccessTokenResultFromWx;
import xyz.hdzx.core.bean.AuthUrlResult;
import xyz.hdzx.core.bean.RestResult;
import xyz.hdzx.service.config.AppIdAndSecretAndUrl;

/** @author whq Date: 2018-10-11 Time: 9:35 */
@Slf4j
@Component
@PropertySource({"service-${spring.profiles.active}.properties",
  "application-${spring.profiles.active}.properties"
})
public class RemoteApi {

  @Value("${codeUrl}")
  private String getCodeUrl;

  @Value("${targetUrl}")
  private String target;

  @Getter
  @Setter
  @Value("${permission.base.url}")
  private String getPermissionAuthUrl;

  // /** 权限系统url */
  // private static final String PERMISSION_BASE_RUL = "http://ROLE-PERMISSION-CONTROL-SYSTEM/TU";

  /** 权限系统认证url */
  // private static final String PERMISSION_AUTH_URL = permissionBaseUrl + "/url";

  @Resource private RestTemplate restTemplate;

  public AuthUrlResult auth(String authentication, String method, String path) {
    AuthUrlResult authUrlResult = new AuthUrlResult();

    HttpHeaders headers = new HttpHeaders();
    headers.add("authentication", authentication);

    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.add("url", path.toLowerCase());
    params.add("method", method.toLowerCase());

    HttpEntity<MultiValueMap<String, String>> httpEntity =
        new HttpEntity<MultiValueMap<String, String>>(params, headers);

    log.info("{}", httpEntity);

    RestResult restResultResponseEntity =
        restTemplate.postForEntity(getPermissionAuthUrl, httpEntity, RestResult.class).getBody();

    if (restResultResponseEntity != null && restResultResponseEntity.isSuccess()) {
      String s = JSON.toJSONString(restResultResponseEntity.getData());
      authUrlResult = JSON.parseObject(s, AuthUrlResult.class);
    } else {
      authUrlResult.setSuccess(false);
    }

    return authUrlResult;
  }

  /**
   * @param forward 投票类型 + id
   * @param isEncode 是否编码
   * @return 返回一个请求路径
   */
  public String getCodeURLFromWx(String forward, boolean isEncode) {

    // https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx520c15f417810387&redirect_uri=https%3A%2F%2Fchong.qq.com%2Fphp%2Findex.php%3Fd%3D%26c%3DwxAdapter%26m%3DmobileDeal%26showwxpaytitle%3D1%26vb2ctag%3D4_2030_5_1194_60&response_type=code&scope=snsapi_base&state=123#wechat_redirect

    // forward = AppIdAndSecretAndUrl.URL + forward + "?";
    forward = target + forward + "?";
    if (isEncode) {
      try {
        forward = URLEncoder.encode(forward, "utf-8");
      } catch (UnsupportedEncodingException e) {
        log.error("编码不被支持：{}", e);
        return "";
      }
    }

    forward =
      // "a.weixin.hndt.com/wxcode.html?"
      getCodeUrl
        + "appid=wx9760b6876d5e339f"
        + "&scope=snsapi_base"
        + "&state=vote&redirect_uri="
            + forward;

    log.info("url: {}", forward);

    return forward;
  }

  /**
   * 后端通过code获取openId
   *
   * @return return
   */
  public String getOpenIdFromWx(String code) {

    String url = AppIdAndSecretAndUrl.ACCESS_TOKEN_WX + "?";
    final String AND = "&";

    url =
        url
            + "appid="
            + AppIdAndSecretAndUrl.DZ_APP_ID
            + AND
            + "secret="
            + AppIdAndSecretAndUrl.DZ_APP_SECRET
            + AND
            + "code="
            + code
            + AND
            + "grant_type=authorization_code";

    log.info("request URL: {}", url);

    AccessTokenResultFromWx body = request(url, AccessTokenResultFromWx.class);

    if (body == null) {
      return "fail";
    }

    return body.getOpenId();
  }

  /**
   * httpOk 发送请求
   *
   * @param url url
   * @param clazz clazz
   * @return Object
   */
  private <T> T request(String url, Class<T> clazz) {

    // 发送请求
    OkHttpClient okHttpClient = new OkHttpClient();
    Request request = new Request.Builder().url(url).build();
    Call call = okHttpClient.newCall(request);
    Response response;
    T body = null;
    try {
      body = clazz.newInstance();
      response = call.execute();
      String str = Objects.requireNonNull(response.body()).string();

      log.info("response: {}", response);
      log.info("str---WX: {}", str);

      body = JSON.parseObject(str, clazz);
      log.info("from wx: {}", body);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("数据转换异常-IllegalAccessException: {}", e);
    } catch (InstantiationException e) {
      log.error("数据转换异常-InstantiationException: {}", e);
    }
    return body;
  }

  public String getGetCodeUrl() {
    return getCodeUrl;
  }

  public void setGetCodeUrl(String getCodeUrl) {
    this.getCodeUrl = getCodeUrl;
  }
}
