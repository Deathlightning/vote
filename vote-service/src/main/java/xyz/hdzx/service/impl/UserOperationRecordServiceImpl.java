package xyz.hdzx.service.impl;

import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import xyz.hdzx.core.redis.VoteConfigTo;
import xyz.hdzx.service.RedisHelper;
import xyz.hdzx.service.UserOperationRecordService;
import xyz.hdzx.service.VoteConfigCacheService;

/**
 * @author whq Date: 2018-09-27 Time: 20:46
 */
@Slf4j
@Service("userOperationRecordServiceImpl")
public class UserOperationRecordServiceImpl extends RedisClearServiceImpl
    implements UserOperationRecordService {

  @Resource
  private RedisHelper<String, String> redisHelperImpl;

  @Resource
  private VoteConfigCacheService voteConfigCacheServiceImpl;

  @Override
  public synchronized boolean isAllow(String voteId, String openId, String groupId) {

    if (redisHelperImpl.isExistInZSet(voteId + "users", openId)
      || redisHelperImpl.isExistInZSet(groupId + "users", openId)) {

      VoteConfigTo voteConfigTo = voteConfigCacheServiceImpl.get(voteId);

      if (groupId != null) {
        final long amount = redisHelperImpl.zSetGetScore(groupId + "users", openId);
        if (voteConfigTo != null && voteConfigTo.getVoteAmount() > amount) {
          redisHelperImpl.zSetAddScore(groupId + "users", 1L, openId);
          return true;
        } else {
          return false;
        }
      }

      final long amount = redisHelperImpl.zSetGetScore(voteId + "users", openId);
      if (voteConfigTo != null && voteConfigTo.getVoteAmount() > amount) {
        log.info("voteConfigTo: {}", voteConfigTo);
        log.info("VoteAmount: {}", voteConfigTo.getVoteAmount());

        redisHelperImpl.zSetAddScore(voteId + "users", 1L, openId);
        return true;
      } else {
        return false;
      }
    }

    // 不存在则加入
    addRecord(voteId, openId, groupId);
    return true;
  }

  @Override
  public void addRecord(String voteId, String openId, String groupId) {

    if (groupId != null) {
      redisHelperImpl.zSetPush(groupId + "users", 1L, openId);
    }
    redisHelperImpl.zSetPush(voteId + "users", 1L, openId);
  }
}
