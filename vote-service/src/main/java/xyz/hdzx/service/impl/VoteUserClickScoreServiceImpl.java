package xyz.hdzx.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;
import org.springframework.stereotype.Service;
import xyz.hdzx.service.RedisHelper;
import xyz.hdzx.service.VoteUserClickScoreService;

/**
 * @author whq Date: 2018-09-28 Time: 13:34
 */
@Service("voteUserClickScoreServiceImpl")
public class VoteUserClickScoreServiceImpl extends RedisClearServiceImpl
  implements VoteUserClickScoreService {

  @Resource
  private RedisHelper<String, String> redisHelperImpl;

  @Override
  public void add(String voteId, String userId, Long score) {
    redisHelperImpl.zSetPush(voteId + "score", score, userId);
  }

  @Override
  public synchronized void addAll(String voteId, String... userIds) {
    for (String id : userIds) {
      redisHelperImpl.zSetPush(voteId + "score", 0L, id);
    }
  }

  @Override
  public List<TypedTuple<String>> getAll(String voteId) {
    return new ArrayList<>(redisHelperImpl.zSetAll(voteId + "score"));
  }

  @Override
  public Long getScore(String voteId, String userId) {
    return redisHelperImpl.zSetGetScore(voteId + "score", userId);
  }

  @Override
  public TypedTuple<String> getMaxScore(String voteId) {

    return redisHelperImpl.zSetGetMaxScore(voteId + "score");
  }

  @Override
  public void incrScore(String voteId, String userId) {

    if (getScore(voteId, userId) == null) {
      add(voteId, userId, 1L);
      return;
    }

    redisHelperImpl.zSetAddScore(voteId + "score", 1L, userId);
  }

  @Override
  public void addScore(String voteId, String userId, Long score) {
    if (getScore(voteId, userId) == null) {
      return;
    }

    redisHelperImpl.zSetRemove(voteId + "score", userId);
    redisHelperImpl.zSetAddScore(voteId + "score", score, userId);
  }

  @Override
  public void removeOne(String voteId, String userId) {
    redisHelperImpl.zSetRemove(voteId + "score", userId);
  }

  @Override
  public boolean isExits(String id, Integer userId) {
    return redisHelperImpl.isExistInZSet(id + "score", String.valueOf(userId));
  }
}
