package xyz.hdzx.service.impl;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import xyz.hdzx.common.RedisKeysConstant;
import xyz.hdzx.service.CurrentVoteIdListService;
import xyz.hdzx.service.RedisHelper;

/**
 * @author whq Date: 2018-09-28 Time: 12:56
 */
@Service("currentVoteIdListServiceImpl")
public class CurrentVoteIdListServiceImpl implements CurrentVoteIdListService {

    @Resource
    private RedisHelper<String, String> redisHelperImpl;

    @Override
    public void add(String voteId) {
        redisHelperImpl.listPush(RedisKeysConstant.VOTE_ID_LIST, voteId);
    }

    @Override
    public List<String> get() {
        return redisHelperImpl.listFindAll(RedisKeysConstant.VOTE_ID_LIST);
    }

    @Override
    public void remove(String voteId) {
        redisHelperImpl.listRemove(RedisKeysConstant.VOTE_ID_LIST, 1, voteId);
    }

  @Override
  public void removeAll(List<String> list) {
    list.forEach(v -> redisHelperImpl.listRemove(RedisKeysConstant.VOTE_ID_LIST, 1, v));

  }

  @Override
    public void removeAll() {
        redisHelperImpl.remove(RedisKeysConstant.VOTE_ID_LIST);
    }
}
