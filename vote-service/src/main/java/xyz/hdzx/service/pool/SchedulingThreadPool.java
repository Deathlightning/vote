package xyz.hdzx.service.pool;

import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Component;
import xyz.hdzx.service.config.SchedulingConfig;

/**
 * @author whq Date: 2018-09-30 Time: 14:50
 */
@Slf4j
@Configuration
public class SchedulingThreadPool implements SchedulingConfigurer {

    @Resource
    private SchedulingConfig schedulingConfig;

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        scheduledTaskRegistrar.setTaskScheduler(taskScheduler());
    }

    @Bean
    public ThreadPoolTaskScheduler taskScheduler() {
        //创建一个线程池调度器
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        // 设置线程池容量
        scheduler.setPoolSize(schedulingConfig.getCorePoolSize());
        //线程名前缀
        scheduler.setThreadNamePrefix("scheduling-whq-");
        //等待时常
        scheduler.setAwaitTerminationSeconds(schedulingConfig.getKeepAliveSeconds());

        scheduler.setErrorHandler(throwable -> log.error("调度任务发生异常", throwable));
        //当调度器shutdown被调用时等待当前被调度的任务完成
        scheduler.setWaitForTasksToCompleteOnShutdown(true);
        //设置当任务被取消的同时从当前调度器移除的策略
        scheduler.setRemoveOnCancelPolicy(true);

        log.info("scheduling bean");

        return scheduler;
    }
}

