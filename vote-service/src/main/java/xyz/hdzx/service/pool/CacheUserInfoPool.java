package xyz.hdzx.service.pool;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import xyz.hdzx.core.bean.CacheUserInfo;

/**
 * @author whq Date: 2018-10-11 Time: 11:53
 */

public enum CacheUserInfoPool {
    /**
     * 管理员信息
     */
    INFO;

    private Map<String, CacheUserInfo> map = new ConcurrentHashMap<>();

    public void put(String key, CacheUserInfo user) {
        map.put(key, user);
    }

    public CacheUserInfo get(String key) {
        return map.get(key);
    }

}
