package xyz.hdzx.service.pool;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import xyz.hdzx.service.config.SchedulingConfig;

/**
 * @author whq Date: 2018-10-11 Time: 14:43
 */
@Slf4j
@EnableAsync
@Configuration
public class NativeSchedulingThreadPool {

    @Resource
    private SchedulingConfig schedulingConfig;


    @Bean(name = "schedule")
    public ScheduledExecutorService myTaskAsyncPool() {

        return new ScheduledThreadPoolExecutor(schedulingConfig.getCorePoolSize(),
            new BasicThreadFactory.Builder().namingPattern("scheduling-pool-%d").daemon(true)
                .build());

        // return Executors
        //     .newScheduledThreadPool(schedulingConfig.getCorePoolSize());
    }

}
