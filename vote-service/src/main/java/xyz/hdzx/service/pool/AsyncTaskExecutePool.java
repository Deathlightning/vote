package xyz.hdzx.service.pool;

import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import xyz.hdzx.service.config.AsyncThreadConfig;

/**
 * @author whq Date: 2018-09-30 Time: 14:51
 */
@Slf4j
@EnableAsync
@Configuration
public class AsyncTaskExecutePool implements AsyncConfigurer {

    @Resource
    private AsyncThreadConfig asyncThreadConfig;

    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //核心线程池大小
        executor.setCorePoolSize(asyncThreadConfig.getCorePoolSize());
        //最大线程数
        executor.setMaxPoolSize(asyncThreadConfig.getMaxPoolSize());
        //队列容量
        executor.setQueueCapacity(asyncThreadConfig.getQueueCapacity());
        //活跃时间
        executor.setKeepAliveSeconds(asyncThreadConfig.getKeepAliveSeconds());
        //线程名字前缀
        executor.setThreadNamePrefix("Async-Whq-");
        executor.setWaitForTasksToCompleteOnShutdown(true);

        // setRejectedExecutionHandler：当pool已经达到max size的时候，如何处理新任务
        // CallerRunsPolicy：不在新线程中执行任务，而是由调用者所在的线程来执行
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();

        log.info("async bean");

        return executor;
    }

    /**
     * 异步任务中异常处理
     */
    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {

        return (Throwable arg0, Method arg1, Object... arg2) -> {
            log.error("exception method:" + arg1.getName());
            log.error(arg0.getMessage());
            log.error("error: {}", arg0);
        };
    }
}