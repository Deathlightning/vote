package xyz.hdzx.service;

import xyz.hdzx.core.redis.VoteConfigTo;

/**
 * @author whq Date: 2018-09-29 Time: 21:44
 */
public interface VoteConfigCacheService {

    /**
     * 存储投票配置，在投票开启后读取数据库存入 前端模板页面拿到这个对象开始渲染
     *
     * @param key 投票唯一表示
     * @param voteConfigTo to对象
     */
    void put(String key, VoteConfigTo voteConfigTo);

    /**
     * 从缓存中读取数据，开始渲染数据
     *
     * @param key 投票唯一表示
     * @return to对象
     */
    VoteConfigTo get(String key);

    /**
     * 投票结束后清理相关数据
     *
     * @param key 投票唯一表示
     */
    void clear(String key);

}
