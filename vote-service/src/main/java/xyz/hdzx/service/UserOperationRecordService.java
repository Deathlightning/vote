package xyz.hdzx.service;

/**
 * @author whq Date: 2018-09-27 Time: 20:46
 */
public interface UserOperationRecordService {

    /**
     * 判断是否当前用户是否投过票, 如果投票允许多次，就进行更新次数
     * @param openId 用户openId
     * @param voteId voteId
     * @return 是否达到投票限制次数，如果没有达到限制，则返回true，并且score加1，否则返回false
     */
    boolean isAllow(String voteId, String openId, String groupId);

    /**
     * 添加一个用户投票记录
     * @param openId openId
     * @param voteId voteId
     */
    void addRecord(String voteId, String openId, String groupId);

    /**
     * 清理一个投票的所有用户记录
     * @param voteId voteId
     */
    void clear(String voteId);
}
