package xyz.hdzx.service.task;

import java.util.concurrent.ScheduledExecutorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import xyz.hdzx.core.redis.VoteConfigTo;
import xyz.hdzx.core.vo.VoteConfigVo;
import xyz.hdzx.dao.mapper.VoteInfoMapper;
import xyz.hdzx.dao.mapper.VoteItemMapper;
import xyz.hdzx.service.CurrentVoteIdListService;
import xyz.hdzx.service.VoteClickNumberService;
import xyz.hdzx.service.VoteConfigCacheService;
import xyz.hdzx.service.VoteInfoService;
import xyz.hdzx.service.VoteItemService;
import xyz.hdzx.service.VoteStatusRedisService;
import xyz.hdzx.service.VoteUserClickScoreService;
import xyz.hdzx.service.VoteViewNumberService;
import xyz.hdzx.service.function.Transform;

/**
 * @author whq Date: 2018-10-08 Time: 14:28
 */
@Slf4j
@Component
public class AsyncTask {

  @Async
  public void startCache(
    VoteConfigVo voteConfigVo,
    VoteConfigCacheService voteConfigCacheServiceImpl,
    ScheduledExecutorService scheduledExecutorServiceImpl,
    VoteStatusRedisService voteStatusRedisServiceImpl,
    VoteInfoService voteInfoServiceImpl,
    VoteInfoMapper voteInfoMapper,
    VoteUserClickScoreService voteUserClickScoreServiceImpl,
    VoteViewNumberService voteVieNumberServiceImpl,
    VoteClickNumberService voteClickNumberServiceImpl,
    VoteItemMapper voteItemMapper,
    CurrentVoteIdListService currentVoteIdListServiceImpl) {

    log.info(
      "1; {}, 2: {}, 3: {}, 4: {}",
      voteConfigVo,
      voteConfigCacheServiceImpl,
      scheduledExecutorServiceImpl,
      voteStatusRedisServiceImpl);

    VoteConfigTo voteConfigTo = Transform.toVoteConfigTo.apply(voteConfigVo);

    voteConfigCacheServiceImpl.put(voteConfigVo.getId(), voteConfigTo);

    // 开启定时任务
    SchedulingTask.startSchedulingTask(
      scheduledExecutorServiceImpl,
      voteConfigTo,
      voteStatusRedisServiceImpl,
      voteInfoServiceImpl,
      voteInfoMapper,
      voteUserClickScoreServiceImpl,
      voteVieNumberServiceImpl,
      voteClickNumberServiceImpl,
      voteItemMapper,
      currentVoteIdListServiceImpl);
  }
}
