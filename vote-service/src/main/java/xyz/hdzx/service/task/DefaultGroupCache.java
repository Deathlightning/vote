package xyz.hdzx.service.task;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author whq
 */
public enum  DefaultGroupCache {
  CACHE;

  private static final Map<String, String> map = new ConcurrentHashMap<>(1);

  public void put(String voteId, String groupId){
    map.put(voteId, groupId);
  }

  public String get(String voteId){
    return map.get(voteId);
  }

  public void remove(String voteId){
    map.remove(voteId);
  }

}
