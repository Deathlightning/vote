package xyz.hdzx.service.task;

import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;
import org.springframework.stereotype.Component;
import xyz.hdzx.common.StartVoteFuturePool;
import xyz.hdzx.common.StartVoteFuturePool.ScheduledType;
import xyz.hdzx.core.bean.Page;
import xyz.hdzx.core.model.VoteInfo;
import xyz.hdzx.core.model.VoteItem;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.core.redis.VoteConfigTo;
import xyz.hdzx.core.vo.VoteItemInfoVo;
import xyz.hdzx.dao.mapper.VoteInfoMapper;
import xyz.hdzx.dao.mapper.VoteItemMapper;
import xyz.hdzx.service.CurrentVoteIdListService;
import xyz.hdzx.service.UserOperationRecordService;
import xyz.hdzx.service.VoteClickNumberService;
import xyz.hdzx.service.VoteInfoService;
import xyz.hdzx.service.VoteItemService;
import xyz.hdzx.service.VoteStatusRedisService;
import xyz.hdzx.service.VoteUserClickScoreService;
import xyz.hdzx.service.VoteViewNumberService;
import xyz.hdzx.service.impl.CurrentVoteIdListServiceImpl;
import xyz.hdzx.service.impl.VoteItemServiceImpl;

/** @author whq Date: 2018-10-08 Time: 14:28 */
@Slf4j
@Component
public class SchedulingTask {

  /**
   * 开始投票定时任务
   *
   * @param scheduledExecutorServiceImpl 线程池
   * @param voteConfigTo 存储对象
   * @param voteStatusRedisServiceImpl 投票状态
   * @param voteInfoMapper 更新数据库状态
   * @param voteItemMapper 更新投票组
   */
  static void startSchedulingTask(
      ScheduledExecutorService scheduledExecutorServiceImpl,
      VoteConfigTo voteConfigTo,
      VoteStatusRedisService voteStatusRedisServiceImpl,
      VoteInfoService voteInfoServiceImpl,
      VoteInfoMapper voteInfoMapper,
      VoteUserClickScoreService voteUserClickScoreServiceImpl,
      VoteViewNumberService voteVieNumberServiceImpl,
      VoteClickNumberService voteClickNumberServiceImpl,
      VoteItemMapper voteItemMapper,
      CurrentVoteIdListService currentVoteIdListServiceImpl) {

    // 如果是pk投票跳过
    if ("2".equals(voteConfigTo.getType())){
      return;
    }

    log.info(
      "voteConfigTo.getStartTime().getTime() > System.currentTimeMillis(): {} > {}",
      voteConfigTo.getStartTime().getTime(),
      System.currentTimeMillis());

    if (voteConfigTo.getStartTime().getTime() > System.currentTimeMillis()) {

      startScheduleTask(
          scheduledExecutorServiceImpl,
          voteConfigTo,
          voteStatusRedisServiceImpl,
          voteInfoServiceImpl,
          voteInfoMapper,
          voteUserClickScoreServiceImpl,
          voteVieNumberServiceImpl,
          voteClickNumberServiceImpl,
          voteItemMapper,
          currentVoteIdListServiceImpl);
    } else {

      log.info(
        "updateStopTask: {} > {}",
        voteConfigTo.getStartTime().getTime(),
        System.currentTimeMillis());

      updateStopTask(
          scheduledExecutorServiceImpl,
          voteConfigTo,
          voteStatusRedisServiceImpl,
          voteInfoServiceImpl,
          voteUserClickScoreServiceImpl,
          voteVieNumberServiceImpl,
          voteClickNumberServiceImpl,
          voteItemMapper);
    }
  }

  private static void startScheduleTask(
      ScheduledExecutorService scheduledExecutorServiceImpl,
      VoteConfigTo voteConfigTo,
      VoteStatusRedisService voteStatusRedisServiceImpl,
      VoteInfoService voteInfoServiceImpl,
      VoteInfoMapper voteInfoMapper,
      VoteUserClickScoreService voteUserClickScoreServiceImpl,
      VoteViewNumberService voteVieNumberServiceImpl,
      VoteClickNumberService voteClickNumberServiceImpl,
      VoteItemMapper voteItemMapper,
      CurrentVoteIdListService currentVoteIdListServiceImpl) {
    ScheduledFuture scheduledFuture =
        StartVoteFuturePool.get(voteConfigTo.getId(), ScheduledType.START);

    // 如果已经存在，取消原本的定时任务，开启新的定时任务
    if (scheduledFuture != null) {
      scheduledFuture.cancel(true);
    }

    final ScheduledFuture<?> startSchedule =
        scheduledExecutorServiceImpl.schedule(
            () -> {

              // 开启投票
              voteStatusRedisServiceImpl.put(voteConfigTo.getId(), VoteStatus.STARTING);
              log.info("投票开启");

              VoteInfo voteInfo = new VoteInfo();
              voteInfo.setStatus(VoteStatus.STARTING.getValue());
              voteInfo.setId(voteConfigTo.getId());

              log.info("voteInfo: {}", voteInfo);
              voteInfoMapper.updateByPrimaryKeySelective(voteInfo);

              currentVoteIdListServiceImpl.add(voteConfigTo.getId());

              // 开启结束定时任务
              stopVoteSchedulingTask(
                  scheduledExecutorServiceImpl,
                  voteConfigTo,
                  voteStatusRedisServiceImpl,
                  voteInfoServiceImpl,
                  voteUserClickScoreServiceImpl,
                  voteVieNumberServiceImpl,
                  voteClickNumberServiceImpl,
                  voteItemMapper);
              log.info("定时结束任务开启");

              // 清理future缓存
              StartVoteFuturePool.remove(voteConfigTo.getId(), ScheduledType.START);
            },
            voteConfigTo.getStartTime().getTime() - System.currentTimeMillis(),
            TimeUnit.MILLISECONDS);

    voteStatusRedisServiceImpl.put(voteConfigTo.getId(), VoteStatus.COMING);

    log.info("开始时间缓存");
    // 保存开始缓存
    StartVoteFuturePool.push(voteConfigTo.getId(), startSchedule, ScheduledType.START);
    log.info("保存开始定时线程");
  }

  private static void updateStopTask(
      ScheduledExecutorService scheduledExecutorServiceImpl,
      VoteConfigTo voteConfigTo,
      VoteStatusRedisService voteStatusRedisServiceImpl,
      VoteInfoService voteInfoServiceImpl,
      VoteUserClickScoreService voteUserClickScoreServiceImpl,
      VoteViewNumberService voteVieNumberServiceImpl,
      VoteClickNumberService voteClickNumberServiceImpl,
      VoteItemMapper voteItemMapper) {
    ScheduledFuture scheduledFuture =
        StartVoteFuturePool.get(voteConfigTo.getId(), ScheduledType.END);

    // 如果已经存在，取消原本的定时任务，开启新的定时任务
    if (scheduledFuture != null) {
      scheduledFuture.cancel(true);
    }

    // 更新结束定时任务
    stopVoteSchedulingTask(
        scheduledExecutorServiceImpl,
        voteConfigTo,
        voteStatusRedisServiceImpl,
        voteInfoServiceImpl,
        voteUserClickScoreServiceImpl,
        voteVieNumberServiceImpl,
        voteClickNumberServiceImpl,
        voteItemMapper);
    log.info("定时结束任务开启");
  }

  /**
   * 停止投票定时任务
   *
   * @param scheduledExecutorServiceImpl 线程池
   * @param voteConfigTo 存储对象
   * @param voteStatusRedisServiceImpl 投票状态
   * @param voteItemMapper 更新投票组
   */
  private static void stopVoteSchedulingTask(
      ScheduledExecutorService scheduledExecutorServiceImpl,
      VoteConfigTo voteConfigTo,
      VoteStatusRedisService voteStatusRedisServiceImpl,
      VoteInfoService voteInfoServiceImpl,
      VoteUserClickScoreService voteUserClickScoreServiceImpl,
      VoteViewNumberService voteVieNumberServiceImpl,
      VoteClickNumberService voteClickNumberServiceImpl,
      VoteItemMapper voteItemMapper) {

    final ScheduledFuture<?> endSchedule =
        scheduledExecutorServiceImpl.schedule(
            () -> {

              //  更新投票状态
              VoteInfo voteInfo = new VoteInfo();

              final TypedTuple<String> maxScore =
                  voteUserClickScoreServiceImpl.getMaxScore(voteConfigTo.getId());
              // todo 处理数据
              voteInfo.setId(voteConfigTo.getId());
              log.info("voteConfigTo.getId(): {}", voteInfo.getId());
              voteInfo.setStatus(VoteStatus.OVER.getValue());
              voteInfo.setMaxClick(
                  maxScore.getScore() == null ? 0 : maxScore.getScore().intValue());
              voteInfo.setMaxName(
                  voteConfigTo
                      .getUsers()
                      .stream()
                      .filter(v -> v.getId().equals(maxScore.getValue()))
                      .collect(Collectors.toList())
                      .get(0)
                      .getName());
              voteInfo.setTotalClick(
                  voteClickNumberServiceImpl.get(voteConfigTo.getId()).intValue());
              voteInfo.setMaxUserId(
                  Integer.valueOf(maxScore.getValue() == null ? "0" : maxScore.getValue()));
              voteInfo.setViewNum(voteVieNumberServiceImpl.get(voteConfigTo.getId()).intValue());
              voteInfoServiceImpl.updateVoteInfoById(voteInfo);
              log.info("投票停止成功： {}", voteInfo);

              // 存储投票项的信息
              List<VoteItemInfoVo> users = voteConfigTo.getUsers();
              users.forEach(
                  v -> {
                    // 写入数据库
                    VoteItem voteItem = new VoteItem();
                    voteItem.setVoteNum(
                        voteUserClickScoreServiceImpl
                            .getScore(voteConfigTo.getId(), v.getId())
                            .intValue());
                    voteItem.setId(Integer.valueOf(v.getId()));
                    voteItemMapper.updateByPrimaryKeySelective(voteItem);
                  });

              // 停止投票
              voteStatusRedisServiceImpl.put(voteConfigTo.getId(), VoteStatus.OVER);
              log.info("停止投票完成");

              // 清理future缓存
              StartVoteFuturePool.remove(voteConfigTo.getId(), ScheduledType.END);

              log.info("清理future缓存");
            },
            voteConfigTo.getEndTime().getTime() - System.currentTimeMillis(),
            TimeUnit.MILLISECONDS);

    StartVoteFuturePool.push(voteConfigTo.getId(), endSchedule, ScheduledType.END);
    log.info("放入结束定时缓存");
  }
}
