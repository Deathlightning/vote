package xyz.hdzx.service.task;

import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import xyz.hdzx.core.model.VoteInfo;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.core.model.enums.VoteType;
import xyz.hdzx.core.redis.VoteConfigTo;
import xyz.hdzx.service.CurrentVoteIdListService;
import xyz.hdzx.service.PKItemCacheService;
import xyz.hdzx.service.UserOperationRecordService;
import xyz.hdzx.service.VoteAnalyzeService;
import xyz.hdzx.service.VoteClickNumberService;
import xyz.hdzx.service.VoteConfigCacheService;
import xyz.hdzx.service.VoteConfigService;
import xyz.hdzx.service.VoteInfoService;
import xyz.hdzx.service.VoteStatusRedisService;
import xyz.hdzx.service.VoteUserClickScoreService;
import xyz.hdzx.service.VoteViewNumberService;

/** @author whq */
@Slf4j
@Component
public class SchedulingClear {

  @Resource private UserOperationRecordService userOperationRecordServiceImpl;

  @Resource private VoteClickNumberService voteClickNumberServiceImpl;

  @Resource private VoteViewNumberService voteViewNumberServiceImpl;

  @Resource private VoteConfigCacheService voteConfigCacheServiceImpl;

  @Resource private VoteUserClickScoreService voteUserClickScoreServiceImpl;

  @Resource private VoteAnalyzeService voteAnalyzeServiceImpl;

  @Resource private PKItemCacheService pKItemCacheServiceImpl;

  @Resource private CurrentVoteIdListService currentVoteIdListServiceImpl;

  @Resource private VoteInfoService voteInfoServiceImpl;

  @Resource private VoteStatusRedisService voteStatusRedisServiceImpl;

  @Scheduled(cron = "0 0 2 * * ?")
  public void clearCache() {
    log.info("clear redis cache");

    List<String> voteIdList = getNotOverVoteList();

    if (voteIdList == null) {
      return;
    }
    clear(voteIdList);

    currentVoteIdListServiceImpl.removeAll(voteIdList);
  }

  private List<String> getNotOverVoteList() {
    List<String> voteIdList = currentVoteIdListServiceImpl.get();
    if (CollectionUtils.isEmpty(voteIdList)) {
      return null;
    }
    voteIdList =
        voteIdList
            .stream()
            .filter(v -> voteStatusRedisServiceImpl.get(v) != VoteStatus.OVER)
            .collect(Collectors.toList());
    return voteIdList;
  }

  @Scheduled(fixedRate = 30 * 1000)
  public void dealExceptionalVote() {

    List<VoteInfo> voteIdList = voteInfoServiceImpl.notStop();
    voteIdList = voteIdList.stream()
      .filter(v -> v.getType() != VoteType.PK_VOTE.getValue()).collect(Collectors.toList());

    if (voteIdList == null) {
      return;
    }

    voteIdList.forEach(
        v -> {
          final VoteConfigTo configTo = voteConfigCacheServiceImpl.get(v.getId());

          if (configTo != null){
            if (voteStatusRedisServiceImpl.get(v.getId()) == VoteStatus.COMING) {
              // 应该开始但是没有开始
              if (configTo.getStartTime().getTime() < System.currentTimeMillis()
                && configTo.getEndTime().getTime() > System.currentTimeMillis()) {
                log.info("处理COMING");
                udpateStatus(v, VoteStatus.STARTING);
              } else if (configTo.getEndTime() != null
                && (configTo.getEndTime().getTime() < System.currentTimeMillis())) {
                // 应该结束但是没有结束
                udpateStatus(v, VoteStatus.OVER);
              }
            } else if (voteStatusRedisServiceImpl.get(v.getId()) == VoteStatus.STARTING) {
              if (configTo.getEndTime() != null
                && (configTo.getEndTime().getTime() < System.currentTimeMillis())) {
                // 应该结束但是没有结束
                udpateStatus(v, VoteStatus.OVER);
              }
            }
          }
        });
  }

  private void udpateStatus(VoteInfo v, VoteStatus voteStatus) {
    voteStatusRedisServiceImpl.put(v.getId(), voteStatus);
    v.setStatus(voteStatus.getValue());
    v.setViewNum(Math.toIntExact(voteViewNumberServiceImpl.get(v.getId())));
    v.setTotalClick(Math.toIntExact(voteClickNumberServiceImpl.get(v.getId())));
    voteInfoServiceImpl.updateVoteInfoById(v);
  }

  private void clear(List<String> voteIdList) {

    voteIdList.forEach(
        v -> {
          userOperationRecordServiceImpl.clear(v);
          voteClickNumberServiceImpl.clear(v);
          voteViewNumberServiceImpl.clear(v);

          voteUserClickScoreServiceImpl.clear(v);
          voteAnalyzeServiceImpl.clear(v);
          final VoteConfigTo voteConfigTo = voteConfigCacheServiceImpl.get(v);
          if ("2".equals(voteConfigTo.getType())) {
            pKItemCacheServiceImpl.clear(v);
          }
          voteConfigCacheServiceImpl.clear(v);
        });
  }
}
