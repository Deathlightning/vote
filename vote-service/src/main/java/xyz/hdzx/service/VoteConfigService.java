package xyz.hdzx.service;

import xyz.hdzx.core.bean.CacheUserInfo;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.core.param.ConfigVoteParam;
import xyz.hdzx.core.param.PKVoteConfigParam;
import xyz.hdzx.core.vo.VoteConfigVo;

/**
 * 用于读写数据库配置信息
 *
 * @author whq Date: 2018-10-06 Time: 13:41
 */
public interface VoteConfigService {

  /**
   * 增加一个投票
   *
   * @param userInfo
   * @param configVoteParam 投票信息
   * @param status 投票的状态
   * @return 是否成功
   */
  boolean insertOneVote(CacheUserInfo userInfo, ConfigVoteParam configVoteParam, VoteStatus status)
      throws Exception;

  /**
   * 查询一个投票信息
   *
   * @param id 投票id
   * @return 投票信息Vo对象
   */
  VoteConfigVo selectVoteById(String id);

  /**
   * 更新投票信息
   *
   * @param userInfo
   * @param configVoteParam 投票配置对象
   * @return 是否成功
   */
  boolean updateVoteById(CacheUserInfo userInfo, ConfigVoteParam configVoteParam)
      throws Exception;

  /**
   * @param id id
   * @return 是否删除成功
   */
  boolean deleteVoteById(String id);

  String insertPKVote(CacheUserInfo userInfo, PKVoteConfigParam param);

  void changeVoteStatus(VoteStatus starting, String id);

  String isModuleExist(int moduleId);
}
