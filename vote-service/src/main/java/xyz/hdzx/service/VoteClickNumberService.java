package xyz.hdzx.service;

/**
 * @author whq Date: 2018-09-28 Time: 12:31
 */
public interface VoteClickNumberService {

    /**
     * 设置投票的点击数,后台修改数据
     * @param voteIdKey voteId
     * @param value value
     */
    void set(String voteIdKey, long value);

    /**
     * 得到投票的点击数据
     * @param voteIdKey voteId
     * @return value
     */
    Long get(String voteIdKey);

    /**
     * 原子性操作，增加一个点击量
     * @param voteIdKey voteId
     */
    void incrValue(String voteIdKey);

    /**
     * 清理一个投票的点击数据
     * @param voteIdKey voteId
     */
    void clear(String voteIdKey);
}
