package xyz.hdzx.service.function;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import xyz.hdzx.core.model.VoteInfo;
import xyz.hdzx.core.model.VoteItem;
import xyz.hdzx.core.model.enums.DeleteRecordStatus;
import xyz.hdzx.core.param.ConfigVoteParam;
import xyz.hdzx.core.param.VoteItemInfoParam;
import xyz.hdzx.core.redis.VoteConfigTo;
import xyz.hdzx.core.vo.VoteAllInfoVo;
import xyz.hdzx.core.vo.VoteConfigVo;
import xyz.hdzx.core.vo.VoteItemInfoVo;

/** @author whq Date: 2018-10-07 Time: 13:46 */
public class Transform {

  /** ConfigVoteParam to VoteInfo */
  public static Function<ConfigVoteParam, VoteInfo> toVoteInfo =
      p -> {
        VoteInfo v = new VoteInfo();
        v.setTitle(p.getTitle());
        v.setContent(p.getContent());
        v.setBrief(p.getBrief());
        v.setType(p.getType());
        v.setStartTime(p.getStartTime());
        v.setEndTime(p.getEndTime());
        v.setDel(DeleteRecordStatus.COMMON.getValue());
        v.setVoteAmount(Byte.valueOf(String.valueOf(p.getVoteAmount())));
        v.setPhotoUrl(p.getBgUrl());
        v.setId(p.getId());

        return v;
      };

  /** VoteItemInfoParam to VoteItem */
  public static Function<List<VoteItemInfoParam>, List<VoteItem>> toVoteItem =
      i ->
          i.stream()
              .map(
                  item -> {
                    VoteItem v = new VoteItem();
                    if (item.getId() != null ){
                      v.setId(Integer.valueOf(item.getId()));
                    }

                    v.setName(item.getName());
                    v.setPhotoUrl(item.getPhotoUrl());
                    v.setUid(item.getUid());
                    v.setBrief(item.getBrief());
                    v.setStatus(Byte.valueOf("0"));
                    return v;
                  })
              .collect(Collectors.toList());

  /** VoteInfo to VoteConfigVo */
  public static Function<VoteInfo, VoteConfigVo> toVoteConfigVo =
      voteInfo -> {
        VoteConfigVo configVo = new VoteConfigVo();
        configVo.setId(voteInfo.getId());
        configVo.setBgUrl(voteInfo.getPhotoUrl());
        configVo.setBrief(voteInfo.getBrief());
        configVo.setContent(voteInfo.getContent());
        configVo.setModuleName(voteInfo.getInteractiveModuleName());
        configVo.setCreator(voteInfo.getCreator());
        configVo.setStatus(voteInfo.getStatus().toString());
        configVo.setType(voteInfo.getType().toString());
        configVo.setTitle(voteInfo.getTitle());
        configVo.setStartTime(voteInfo.getStartTime());
        configVo.setEndTime(voteInfo.getEndTime());
        configVo.setVoteAmount(voteInfo.getVoteAmount());
        return configVo;
      };

  /** VoteInfo to VoteAllInfoVo */
  public static Function<VoteInfo, VoteAllInfoVo> toVoteAllInfoVo =
      voteInfo -> {
        VoteAllInfoVo voteAllInfoVo = new VoteAllInfoVo();
        voteAllInfoVo.setId(voteInfo.getId());
        voteAllInfoVo.setBgUrl(voteInfo.getPhotoUrl());
        voteAllInfoVo.setBrief(voteInfo.getBrief());
        voteAllInfoVo.setContent(voteInfo.getContent());
        voteAllInfoVo.setModuleName(voteInfo.getInteractiveModuleName());
        voteAllInfoVo.setCreator(voteInfo.getCreator());
        voteAllInfoVo.setStatus(voteInfo.getStatus().toString());
        voteAllInfoVo.setType(voteInfo.getType().toString());
        voteAllInfoVo.setTitle(voteInfo.getTitle());
        voteAllInfoVo.setStartTime(voteInfo.getStartTime());
        voteAllInfoVo.setEndTime(voteInfo.getEndTime());
        voteAllInfoVo.setTotalNumber(voteInfo.getTotalClick());
        voteAllInfoVo.setVoteAmount(voteInfo.getVoteAmount());
        voteAllInfoVo.setModuleId(
          voteInfo.getInteractiveModuleId() == 0 ? null : voteInfo.getInteractiveModuleId());
        voteAllInfoVo.setCreateTime(voteInfo.getCreateTime());

        return voteAllInfoVo;
      };

  /** VoteItem to VoteItemInfoVo */
  public static Function<VoteItem, VoteItemInfoVo> toVoteItemInfoVo =
      v -> {
        VoteItemInfoVo voteItemInfoVo = new VoteItemInfoVo();
        voteItemInfoVo.setPoll(v.getVoteNum() == null ? 0 : v.getVoteNum());
        voteItemInfoVo.setStatus(v.getStatus());
        voteItemInfoVo.setId(v.getId().toString());
        voteItemInfoVo.setPhotoUrl(v.getPhotoUrl());
        voteItemInfoVo.setName(v.getName());
        voteItemInfoVo.setBrief(v.getBrief());
        return voteItemInfoVo;
      };

  public static Function<VoteConfigVo, VoteConfigTo> toVoteConfigTo =
      vote -> {
        VoteConfigTo to = new VoteConfigTo();
        to.setVoteAmount(vote.getVoteAmount());
        to.setUsers(vote.getUsers());
        to.setBgUrl(vote.getBgUrl());
        to.setBrief(vote.getBrief());
        to.setContent(vote.getContent());
        to.setModuleName(vote.getModuleName());
        to.setStatus(vote.getStatus());
        to.setType(vote.getType());
        to.setId(vote.getId());
        to.setTitle(vote.getTitle());
        to.setStartTime(vote.getStartTime());
        to.setEndTime(vote.getEndTime());
        return to;
      };
}
