package xyz.hdzx.service;

/**
 * @author whq
 */
public interface OpenIdCacheService {

  String getOpenId(String code);

  void put(String code, String openId);
}
