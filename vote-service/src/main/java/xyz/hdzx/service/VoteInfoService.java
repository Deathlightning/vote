package xyz.hdzx.service;

import java.util.List;

import org.apache.ibatis.annotations.Update;
import xyz.hdzx.core.annotations.ParamValid;
import xyz.hdzx.core.bean.Page;
import xyz.hdzx.core.bean.ResultBean;
import xyz.hdzx.core.model.VoteInfo;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.core.param.SelectVoteParam;
import xyz.hdzx.core.vo.PluginInfo;
import xyz.hdzx.core.vo.VoteAllInfoVo;

/**
 * @author whq Date: 2018-10-06 Time: 13:56
 */
public interface VoteInfoService {

    /**
     * 查询所有投票信息,需要分页
     *
     * @param param 分页和当前管理员条件
     * @return 所有投票信息
     */
    ResultBean<Page<VoteAllInfoVo>> selectAll(SelectVoteParam param);

    /**
     * 查询当前栏目下的数据
     *
     * @param param 分页和当前管理员条件
     * @return 查询结果
     */
    @ParamValid(notValidParam = {"endTime", "startTime", "name"})
    ResultBean<Page<VoteAllInfoVo>> selectStarting(SelectVoteParam param);

    /**
     * 查询投票即将开始的数据列表
     *
     * @param param 分页和当前管理员条件
     * @return 结果
     */
    @ParamValid(notValidParam = {"endTime", "startTime", "name"})
    ResultBean<Page<VoteAllInfoVo>> selectComing(SelectVoteParam param);

    /**
     * 查询投票已经结束的数据列表
     *
     * @param param 分页和当前管理员条件
     * @return 结果
     */
    @ParamValid(notValidParam = {"endTime", "startTime", "name"})
    ResultBean<Page<VoteAllInfoVo>> selectOver(SelectVoteParam param);

    /**
     * 根据条件筛选
     *
     * @param param 筛选条件
     * @return 结果
     */
    @ParamValid(notValidParam = {"endTime", "startTime", "name"})
    ResultBean<Page<VoteAllInfoVo>> selectBy(SelectVoteParam param);

    VoteInfo selectById(String id);

    ResultBean deleteVoteInfo(String id);

    boolean updateVoteInfoById(VoteInfo voteInfo);

    List<PluginInfo> selectByFrequencyId(int id);

    List<VoteInfo> notStop();

    List<PluginInfo> selectByModuleId(int moduleId);

    String getUrl(String id);
}
