package xyz.hdzx.web.filter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import xyz.hdzx.core.bean.AuthUrlResult;
import xyz.hdzx.core.bean.CacheUserInfo;
import xyz.hdzx.service.impl.RemoteApi;
import xyz.hdzx.service.pool.CacheUserInfoPool;

/**
 * @author whq Date: 2018-10-10 Time: 22:13
 */
@Slf4j
@Component
public class AccessTokenVerifyInterceptor extends HandlerInterceptorAdapter {

    @Resource
    private RemoteApi remoteApi;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
        Object handler)
        throws Exception {
        boolean flag = false;

        // 处理跨域
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Expose-Headers", "*");
        response.setHeader("Access-Control-Allow-Headers",
            "Content-Type,Content-Length, Authorization, Accept,X-Requested-With, authentication");
        response.setHeader("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
        response.setHeader("X-Powered-By", "Tomcat");
        response.setHeader("Content-Type", "text/html;charset=UTF-8");
        response.setHeader("maxAge", "10000");

        request.setAttribute("Access-Control-Request-Headers", "*");


        String authentication = request.getHeader("authentication");
        String method = request.getMethod().toLowerCase();
        String path = request.getServletPath().toLowerCase();

      log.info("method: {}", method);
      log.info("path: {}", path);

        log.info("authentication： " + authentication);

        // 释放跨域的option方法
        if (method.equalsIgnoreCase(RequestMethod.OPTIONS.toString())) {
            return true;
        }

        if (StringUtils.isNotEmpty(authentication)) {
            AuthUrlResult authUrlResult = remoteApi.auth(authentication, method, path);
            flag = authUrlResult.isSuccess();
            if (flag) {
                // 成功
                log.info("认证成功");

                // 加入缓存池
                cacheUserInfo(authUrlResult);

              response.setHeader("authentication", authUrlResult.getAuthentication());
                request.setAttribute("uid", authUrlResult.getId());
            } else {
                log.info("认证失败");
                response.setStatus(HttpStatus.FORBIDDEN.value());
                response.getWriter().print("认证错误");
            }
        } else {
            log.info("authentication不存在");
            response.setStatus(HttpStatus.FORBIDDEN.value());
        }
        return flag;
    }

    private void cacheUserInfo(AuthUrlResult authUrlResult) {
        // 缓存管理员信息
        final CacheUserInfo cacheUserInfo = new CacheUserInfo();

        cacheUserInfo.setId(authUrlResult.getId())
            .setCurrentFrequencyId(authUrlResult.getCurrentFrequencyId())
            .setCurrentProgramId(authUrlResult.getCurrentProgramId())
            .setName(authUrlResult.getName())
            .setType(authUrlResult.getType());


        CacheUserInfoPool.INFO.put(authUrlResult.getId().toString(), cacheUserInfo);
    }
}

