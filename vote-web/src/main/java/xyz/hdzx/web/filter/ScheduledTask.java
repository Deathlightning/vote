package xyz.hdzx.web.filter;

import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import xyz.hdzx.core.model.VoteInfo;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.service.VoteInfoService;

/** @author whq */
@Slf4j
@Component
public class ScheduledTask {

  @Resource private VoteInfoService voteInfoServiceImpl;

  // @Scheduled(fixedDelay = 10 * 60000)
  // public void clearExceptionCloseVote() {
  //   log.info("check vote stop time");
  //
  //   final List<VoteInfo> voteInfoList = voteInfoServiceImpl.notStop();
  //   final long now = System.currentTimeMillis();
  //   voteInfoList
  //       .stream()
  //       .filter(
  //           v ->
  //               (v.getStartTime().getTime() < now && v.getEndTime().getTime() < now)
  //                   || v.getEndTime().getTime() < now)
  //       .forEach(
  //           v -> {
  //             v.setStatus(VoteStatus.OVER.getValue());
  //             voteInfoServiceImpl.updateVoteInfoById(v);
  //           });
  // }
}
