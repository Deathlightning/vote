package xyz.hdzx.web.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.core.param.UserVoteParam;
import xyz.hdzx.core.redis.PKVoteConfigParamTo;
import xyz.hdzx.service.OpenIdCacheService;
import xyz.hdzx.service.PKItemCacheService;
import xyz.hdzx.service.UserOperationRecordService;
import xyz.hdzx.service.VoteClickNumberService;
import xyz.hdzx.service.VoteConfigCacheService;
import xyz.hdzx.service.VoteStatusRedisService;
import xyz.hdzx.service.VoteUserClickScoreService;
import xyz.hdzx.service.impl.RemoteApi;

/**
 * @author whq Date: 2018-10-09 Time: 18:46
 */
@Slf4j
@RestController
@RequestMapping("/vote")
@Api(tags = "投票判断状态，投票次数接口")
public class VoteController {

  @Resource
  private UserOperationRecordService userOperationRecordServiceImpl;

  @Resource
  private VoteClickNumberService voteClickNumberServiceImpl;

  @Resource
  private OpenIdCacheService openIdCacheServiceImpl;

  @Resource
  private VoteUserClickScoreService voteUserClickScoreServiceImpl;

  @Resource
  private VoteConfigCacheService voteConfigCacheServiceImpl;

  @Resource
  private VoteStatusRedisService voteStatusRedisServiceImpl;

  @Resource
  private PKItemCacheService pKItemCacheServiceImpl;

  @Resource
  private RemoteApi remoteApi;

  @PostMapping("/{id}")
  public String vote(@PathVariable("id") String id, @RequestBody UserVoteParam body) {

    log.info("body: {}", body);
    final VoteStatus voteStatus = voteStatusRedisServiceImpl.get(id);

    // 判断投票是否截止
    if (voteStatus == VoteStatus.OVER) {
      // 截止页面
      return "当前投票已经结束";
    }

    if (voteStatus != VoteStatus.STARTING) {
      // 未开始页面
      return "当前投票没有开始";
    }

    String openIdFromWx = openIdCacheServiceImpl.getOpenId(body.getCode());

    if (StringUtils.isEmpty(openIdFromWx)) {
      openIdFromWx = remoteApi.getOpenIdFromWx(body.getCode());
      if (StringUtils.isNotEmpty(openIdFromWx)) {
        // 缓存openId
        openIdCacheServiceImpl.put(body.getCode(), openIdFromWx);
      } else {
        return "登陆超时，请超重新登陆";
      }
    }

    final String voteItemId =
      body.getVoteItemId().substring(body.getVoteItemId().lastIndexOf("-") + 1);

    log.info("voteItemId: {}", voteItemId);

    // 判断是否可行, 增加投票数量, 增加点击量
    boolean allow;

    // 不是pk投票，根据openId限制
    if (!"2".equals(voteConfigCacheServiceImpl.get(id).getType())) {
      allow = userOperationRecordServiceImpl.isAllow(id, openIdFromWx, null);
      log.info("非pk投票");
    } else {

      final Map<String, List<PKVoteConfigParamTo>> allGroup =
        pKItemCacheServiceImpl.getAllGroup(id);

      // 循环遍历拿到所有投票项中唯一开始的的投票项
      final List<String> startingVote =
        allGroup
          .keySet()
          .stream()
          .filter(v -> voteStatusRedisServiceImpl.get(v) == VoteStatus.STARTING)
          .collect(Collectors.toList());
      if (startingVote.size() == 0){
        return "投票即将开始";
      }

      final String groupId =
        allGroup
          .keySet()
          .stream()
          .filter(
            v ->
              allGroup
                .get(v)
                .stream()
                .map(PKVoteConfigParamTo::getId)
                .collect(Collectors.toList())
                .contains(voteItemId))
          .collect(Collectors.toList())
          .get(0);
      log.info("pk组投票：{}", groupId);
      // 根据投票组id限制
      allow = userOperationRecordServiceImpl.isAllow(id, openIdFromWx, groupId);
    }

    if (allow) {
      log.info("allow...: ", openIdFromWx);

      log.info("voteItemId: {}", voteItemId);
      voteUserClickScoreServiceImpl.incrScore(id, voteItemId);

      // 增加投票点击量
      voteClickNumberServiceImpl.incrValue(id);

      return "投票成功";
    } else {
      return "你已经投过票了";
      // return "你的投票次数已经到达上限";
    }
  }
}
