package xyz.hdzx.web.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import xyz.hdzx.common.CommonConstant;
import xyz.hdzx.core.annotations.ParamValid;
import xyz.hdzx.core.bean.CacheUserInfo;
import xyz.hdzx.core.bean.RestResult;
import xyz.hdzx.core.bean.ResultBean;
import xyz.hdzx.core.entity.VoteItemExt;
import xyz.hdzx.core.model.VoteInfo;
import xyz.hdzx.core.model.VoteItem;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.core.param.ConfigVoteParam;
import xyz.hdzx.core.param.PKVoteConfigParam;
import xyz.hdzx.service.VoteClickNumberService;
import xyz.hdzx.service.VoteConfigService;
import xyz.hdzx.service.VoteInfoService;
import xyz.hdzx.service.VoteItemService;
import xyz.hdzx.service.VoteStatusRedisService;
import xyz.hdzx.service.VoteUserClickScoreService;
import xyz.hdzx.service.VoteViewNumberService;
import xyz.hdzx.service.pool.CacheUserInfoPool;
import xyz.hdzx.service.task.DefaultGroupCache;

/**
 * 用于配置投票信息
 *
 * @author whq Date: 2018-09-19 Time: 21:35
 */
@Slf4j
@Api(
    value = "用于配置投票信息",
    tags = {"修改配置", "保存配置", "发布投票"})
@RestController
@RequestMapping("/config")
public class VoteConfigController {

  @Resource private VoteConfigService voteConfigServiceImpl;

  @Resource private VoteStatusRedisService voteStatusRedisServiceImpl;

  @Resource private VoteUserClickScoreService voteUserClickScoreServiceImpl;

  @Resource private VoteClickNumberService voteClickNumberServiceImpl;

  @Resource private VoteViewNumberService voteViewNumberServiceImpl;

  @Resource private VoteInfoService voteInfoServiceImpl;

  @Resource private VoteItemService voteItemServiceImpl;

  /**
   * 修改配置，不存储type值
   *
   * @param configVoteParam 修改配置 type值不使用
   * @return 是否成功
   */
  // @ParamValid(
  //     notValidParam = {
  //       "type",
  //       "content",
  //       "interactiveModuleName",
  //       "interactiveModuleId",
  //       "bgUrl",
  //       "updateTime",
  //       "createTime",
  //       "startTime",
  //       "endTime"
  //     })
  @ApiOperation(value = "修改配置")
  @ApiImplicitParams(@ApiImplicitParam(value = "configVoteParam", paramType = "ConfigVoteParam"))
  @PutMapping("/modify")
  public ResultBean modifyConfig(
      HttpServletRequest request, @RequestBody ConfigVoteParam configVoteParam) {

    CacheUserInfo userInfo =
        CacheUserInfoPool.INFO.get(String.valueOf(request.getAttribute("uid")));

    ResultBean bean = new ResultBean();

    final String moduleExist =
        voteConfigServiceImpl.isModuleExist(configVoteParam.getInteractiveModuleId());
    if (moduleExist.equals(CommonConstant.ERROR)
        || (!moduleExist.equals(CommonConstant.SUCCESS)
            && !moduleExist.equals(configVoteParam.getId()))) {
      bean.setCode(ResultBean.FAIL);
      bean.setMsg("当前互动模块下已有投票发起");
      return bean;
    }

    try {
      if (voteConfigServiceImpl.updateVoteById(userInfo, configVoteParam)) {
        bean.setCode(ResultBean.SUCCESS);
      } else {
        bean.setCode(ResultBean.FAIL);
        bean.setMsg("修改数据错误");
      }
    } catch (Exception e) {
      log.error("error: {}", e);
      bean.setMsg(e.getMessage());
      bean.setCode(ResultBean.FAIL);
    }
    return bean;
  }

  /**
   * 保存临时配置
   *
   * @param configVoteParam 保存配置
   * @return 是否成功
   */
  @Deprecated
  @ParamValid(
      notValidParam = {
        "id",
        "content",
        "bgUrl",
        "interactiveModuleName",
        "interactiveModuleId",
        "createTime",
        "updateTime"
      })
  @ApiOperation(value = "保存配置")
  @ApiImplicitParams(@ApiImplicitParam(value = "configVoteParam", paramType = "ConfigVoteParam"))
  @RequestMapping(value = "/save", method = RequestMethod.POST)
  public Object saveConfig(
      HttpServletRequest request, @RequestBody ConfigVoteParam configVoteParam) {

    CacheUserInfo userInfo =
        CacheUserInfoPool.INFO.get(String.valueOf(request.getAttribute("uid")));

    ResultBean bean = new ResultBean();

    final String moduleExist =
      voteConfigServiceImpl.isModuleExist(configVoteParam.getInteractiveModuleId());
    if (moduleExist.equals(CommonConstant.ERROR)
      || (!moduleExist.equals(CommonConstant.SUCCESS)
      && !moduleExist.equals(configVoteParam.getId()))) {
      bean.setCode(ResultBean.FAIL);
      bean.setMsg("当前互动模块下已有投票发起");
      return bean;
    }

    try {
      if (voteConfigServiceImpl.insertOneVote(userInfo, configVoteParam, VoteStatus.COMING)) {
        bean.setCode(ResultBean.SUCCESS);
        log.info("修改投票： {}", configVoteParam);
      } else {
        bean.setCode(ResultBean.FAIL);
        bean.setMsg("保存数据错误");
      }
    } catch (Exception e) {
      log.error("error: {}", e);
      bean.setMsg(e.getMessage());
      bean.setCode(ResultBean.FAIL);
    }
    return bean;
  }

  /**
   * 只有发布投票才开始创建一个定时开启任务
   *
   * @param configVoteParam 投票配置
   * @return 是否成功
   */
  // @ParamValid(
  //     notValidParam = {
  //       "id",
  //       "orderByClause",
  //       "offset",
  //       "currentPage",
  //       "bgUrl",
  //       "interactiveModuleName",
  //       "interactiveModuleId",
  //       "createTime",
  //       "updateTime"
  //     })
  @ApiOperation(value = "发布投票")
  @ApiImplicitParams(@ApiImplicitParam(value = "configVoteParam", paramType = "ConfigVoteParam"))
  @PostMapping("/release")
  public ResultBean releaseVote(
      HttpServletRequest request, @RequestBody ConfigVoteParam configVoteParam) {

    CacheUserInfo userInfo =
        CacheUserInfoPool.INFO.get(String.valueOf(request.getAttribute("uid")));

    ResultBean bean = new ResultBean();

    final String moduleExist =
      voteConfigServiceImpl.isModuleExist(configVoteParam.getInteractiveModuleId());
    if (moduleExist.equals(CommonConstant.ERROR)
      || (!moduleExist.equals(CommonConstant.SUCCESS)
      && !moduleExist.equals(configVoteParam.getId()))) {
      bean.setCode(ResultBean.FAIL);
      bean.setMsg("当前互动模块下已有投票发起");
      return bean;
    }

    try {
      if (voteConfigServiceImpl.insertOneVote(userInfo, configVoteParam, VoteStatus.COMING)) {
        bean.setCode(ResultBean.SUCCESS);
        log.info("发布投票： {}", configVoteParam);
      } else {
        bean.setCode(ResultBean.FAIL);
        log.info("发布投票失败", configVoteParam);
      }
    } catch (Exception e) {
      log.error("error: {}", e);
      bean.setMsg(e.getMessage());
      bean.setCode(ResultBean.FAIL);
    }
    return bean;
  }

  @PostMapping("/release-pk")
  public ResultBean releasePKVote(
      HttpServletRequest request, @RequestBody PKVoteConfigParam param) {

    CacheUserInfo userInfo =
        CacheUserInfoPool.INFO.get(String.valueOf(request.getAttribute("uid")));

    ResultBean bean = new ResultBean();

    final String moduleExist =
      voteConfigServiceImpl.isModuleExist(param.getInteractiveModuleId());
    if (!moduleExist.equals(param.getId())) {
      if (!moduleExist.equals(CommonConstant.SUCCESS)) {

        bean.setCode(ResultBean.FAIL);
        bean.setMsg("当前互动模块下已有投票发起");
        return bean;
      } else if (moduleExist.equals(CommonConstant.ERROR)) {

        bean.setCode(ResultBean.FAIL);
        bean.setMsg("当前互动模块下已有投票发起");
        return bean;
      }
    } else if (moduleExist.equals(CommonConstant.ERROR)) {

      bean.setCode(ResultBean.FAIL);
      bean.setMsg("当前互动模块下已有投票发起");
      return bean;
    }

    String id = voteConfigServiceImpl.insertPKVote(userInfo, param);

    if (StringUtils.isNotEmpty(id)) {
      bean.setMsg(id);
    } else {
      bean.setCode(ResultBean.FAIL);
      bean.setMsg("添加投票错误");
    }

    return bean;
  }

  @ApiOperation("开始投票")
  @PutMapping("/start/{id}")
  public Object startPkVote(@PathVariable("id") String id) {

    final String groupId = DefaultGroupCache.CACHE.get(id);
    final ResultBean<Object> bean = new ResultBean<>();

    if (StringUtils.isEmpty(groupId)) {
      bean.setMsg("当前投票没有开启默认组，请先开启");
      bean.setCode(ResultBean.FAIL);
      return bean;
    }

    voteStatusRedisServiceImpl.put(id, VoteStatus.STARTING);
    voteConfigServiceImpl.changeVoteStatus(VoteStatus.STARTING, id);

    return new ResultBean<>("投票已经开始");
  }

  @ApiOperation("停止PK投票")
  @PutMapping("/stop/{id}")
  public Object stopPKVote(@PathVariable("id") String id) {
    voteStatusRedisServiceImpl.put(id, VoteStatus.OVER);

    // voteConfigServiceImpl.changeVoteStatus(VoteStatus.OVER, id);
    log.info("停止投票完成");

    //  更新投票状态
    VoteInfo voteInfo = new VoteInfo();

    final TypedTuple<String> maxScore = voteUserClickScoreServiceImpl.getMaxScore(id);
    // todo 处理数据
    voteInfo.setId(id);
    voteInfo.setStatus(VoteStatus.OVER.getValue());
    if (maxScore != null) {
      voteInfo.setMaxClick(maxScore.getScore() == null ? 0 : maxScore.getScore().intValue());
    }
    voteInfo.setTotalClick(voteClickNumberServiceImpl.get(id).intValue());
    voteInfo.setViewNum(voteViewNumberServiceImpl.get(id).intValue());
    voteInfoServiceImpl.updateVoteInfoById(voteInfo);

    final List<List<VoteItemExt>> allInfoByVoteId = voteItemServiceImpl.getAllInfoByVoteId(id);

    ArrayList<VoteItem> arrayList = new ArrayList<>();
    allInfoByVoteId.forEach(arrayList::addAll);

    log.info("allInfoByVoteId: {}", allInfoByVoteId);
    log.info("arrayList: {}", arrayList);
    arrayList.forEach(
        v -> {
          v.setVoteNum(voteUserClickScoreServiceImpl.getScore(id, v.getId().toString()).intValue());
          voteItemServiceImpl.updateByItemId(v);
        });

    return new RestResult();
  }
}
