package xyz.hdzx.web.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.hdzx.core.bean.CacheUserInfo;
import xyz.hdzx.core.bean.RestResult;
import xyz.hdzx.core.bean.ResultBean;
import xyz.hdzx.core.model.VoteInfo;
import xyz.hdzx.core.model.VoteItem;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.core.redis.UserVoteAnalyzeDataTo;
import xyz.hdzx.core.redis.VoteConfigTo;
import xyz.hdzx.core.vo.VoteItemInfoVo;
import xyz.hdzx.service.VoteAnalyzeService;
import xyz.hdzx.service.VoteClickNumberService;
import xyz.hdzx.service.VoteConfigCacheService;
import xyz.hdzx.service.VoteInfoService;
import xyz.hdzx.service.VoteItemService;
import xyz.hdzx.service.VoteUserClickScoreService;
import xyz.hdzx.service.VoteViewNumberService;
import xyz.hdzx.service.impl.VoteStatusRedisServiceImpl;
import xyz.hdzx.service.pool.CacheUserInfoPool;
import xyz.hdzx.service.task.DefaultGroupCache;

/** @author whq */
@Slf4j
@RestController
@RequestMapping("/operation")
@Api(tags = "投票更新接口")
public class OperationController {

  @Resource private VoteInfoService voteInfoServiceImpl;

  @Resource private VoteStatusRedisServiceImpl voteStatusRedisServiceImpl;

  @Resource private VoteConfigCacheService voteConfigCacheServiceImpl;

  @Resource private VoteClickNumberService voteClickNumberServiceImpl;

  @Resource private VoteViewNumberService voteViewNumberServiceImpl;

  @Resource private VoteUserClickScoreService voteUserClickScoreServiceImpl;

  @Resource private VoteAnalyzeService voteAnalyzeServiceImpl;

  @Resource private VoteItemService voteItemServiceImpl;

  @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
  public Object delete(@PathVariable("id") String id) {

    return voteInfoServiceImpl.deleteVoteInfo(id);
  }

  @PutMapping("/stop/{id}")
  public Object stop(HttpServletRequest request, @PathVariable("id") String id) {

    log.info("stop -> voteId: {}", id);
    ResultBean<Object> bean = new ResultBean<>();

    CacheUserInfo userInfo =
        CacheUserInfoPool.INFO.get(String.valueOf(request.getAttribute("uid")));

    final VoteStatus voteStatus = voteStatusRedisServiceImpl.get(id);

    if (voteStatus == VoteStatus.OVER){
      bean.setMsg("投票已经停止过了");
      bean.setCode(ResultBean.FAIL);
      return bean;
    }

    voteStatusRedisServiceImpl.put(id, VoteStatus.OVER);

    final Long clickNum = voteClickNumberServiceImpl.get(id);
    final Long viewNum = voteViewNumberServiceImpl.get(id);
    final List<TypedTuple<String>> all = voteUserClickScoreServiceImpl.getAll(id);

    VoteInfo voteInfo = new VoteInfo();
    voteInfo.setId(id);
    voteInfo.setStatus(VoteStatus.OVER.getValue());
    voteInfo.setEndTime(new Date());
    voteInfo.setViewNum(viewNum.intValue());
    voteInfo.setTotalClick(Integer.valueOf(clickNum.toString()));
    voteInfo.setUpdater(userInfo.getName());
    voteInfo.setUpdaterid(userInfo.getId().toString());

    final VoteConfigTo voteConfigTo = voteConfigCacheServiceImpl.get(id);

    // 如果为pk投票
    if ("2".equals(voteConfigTo.getType())) {
      return stopPK(voteInfo, all);
    }

    final Map<String, UserVoteAnalyzeDataTo> allAnalyze = voteAnalyzeServiceImpl.getAllResult(id);

    if (all != null && all.size() > 0) {
      final Double score = all.get(0).getScore();

      if (score == null) {
        voteInfo.setViewNum(0);
      } else {
        voteInfo.setMaxUserId(score.intValue());
      }
    } else {
      voteInfo.setViewNum(0);
    }

    final List<String> collect =
        allAnalyze
            .keySet()
            .stream()
            .filter(v -> allAnalyze.get(v).isMaximum())
            .collect(Collectors.toList());

    if (!CollectionUtils.isEmpty(collect)) {

      log.info("collect: {}", collect);
      final List<VoteItemInfoVo> maxInfo =
          voteConfigCacheServiceImpl
              .get(id)
              .getUsers()
              .stream()
              .filter(v -> v.getId().equals(allAnalyze.get(collect.get(0)).getId()))
              .collect(Collectors.toList());

      log.info("maxInfo: {}", maxInfo);
      if (!CollectionUtils.isEmpty(maxInfo)) {
        voteInfo.setMaxName(maxInfo.get(0).getName());
        voteInfo.setMaxClick(maxInfo.get(0).getPoll());
        voteInfo.setMaxUserId(Integer.valueOf(maxInfo.get(0).getId()));
      }
    }
    voteConfigCacheServiceImpl.clear(id);
    log.info("voteInfo: {}", voteInfo);
    if (!voteInfoServiceImpl.updateVoteInfoById(voteInfo)) {
      bean.setMsg("数据更新失败");
      bean.setCode(ResultBean.FAIL);
      return bean;
    }

    bean.setMsg("success");
    bean.setCode(ResultBean.SUCCESS);
    return bean;
  }

  private Object stopPK(VoteInfo voteInfo, List<TypedTuple<String>> all) {
    voteConfigCacheServiceImpl.clear(voteInfo.getId());
    voteInfoServiceImpl.updateVoteInfoById(voteInfo);

    all.forEach(
        v -> {
          VoteItem item = new VoteItem();
          item.setId(Integer.valueOf(Objects.requireNonNull(v.getValue())));
          item.setVoteNum(v.getScore() == null ? 0 : v.getScore().intValue());
          voteItemServiceImpl.updateByItemId(item);
        });

    final ResultBean<Object> bean = new ResultBean<>();
    bean.setMsg("success");
    bean.setCode(ResultBean.SUCCESS);

    return bean;
  }

  @ApiOperation("修改票数")
  @PutMapping("/poll/{id}")
  public Object modifyPoll(
      @PathVariable("id") String id,
      @RequestParam("itemId") String itemId,
      @RequestParam("poll") Long poll) {
    final VoteStatus voteStatus = voteStatusRedisServiceImpl.get(id);
    log.info("voteStatus: {}", voteStatus);
    ResultBean bean = new ResultBean();

    if (poll == null || poll > Integer.MAX_VALUE || poll < 0) {
      bean.setCode(ResultBean.FAIL);
      bean.setMsg("票数不正确，请检查是否在合理范围内");
      return bean;
    }

    if (voteStatus != VoteStatus.STARTING || itemId == null) {
      bean.setCode(ResultBean.FAIL);
      bean.setMsg("当前投票暂时没有开启，不能修改");
      return bean;
    }

    voteUserClickScoreServiceImpl.addScore(id, itemId, poll);

    return bean;
  }

  @ApiOperation("设置默认组")
  @GetMapping("/default")
  public Object setDefaultGroup(
      @RequestParam("voteId") String voteId, @RequestParam("groupId") String groupId) {

    final RestResult restResult = new RestResult();

    if (StringUtils.isEmpty(voteId) || StringUtils.isEmpty(groupId)){
      restResult.setSuccess(false);
      restResult.setMessage("数据不能为空");
      return restResult;
    }

    DefaultGroupCache.CACHE.put(voteId, groupId);

    log.info("DefaultGroupCache.CACHE.put({}, {})", voteId, groupId);

    restResult.setSuccess(true);
    restResult.setData(groupId);
    return restResult;
  }
}
