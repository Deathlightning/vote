package xyz.hdzx.web.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.hdzx.core.bean.RestResult;
import xyz.hdzx.core.bean.ResultBean;
import xyz.hdzx.core.model.VoteItem;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.core.param.OneGroupParam;
import xyz.hdzx.core.param.PKVoteItemParam;
import xyz.hdzx.core.param.VoteItemInfoParam;
import xyz.hdzx.core.redis.PKVoteConfigParamTo;
import xyz.hdzx.core.redis.VoteConfigTo;
import xyz.hdzx.service.PKItemCacheService;
import xyz.hdzx.service.VoteConfigCacheService;
import xyz.hdzx.service.VoteItemService;
import xyz.hdzx.service.VoteStatusRedisService;
import xyz.hdzx.service.VoteUserClickScoreService;
import xyz.hdzx.service.function.Transform;

/**
 * @author whq
 */
@Slf4j
@RestController
@Api(tags = "投票项接口")
public class VoteItemController {

  @Resource
  private VoteItemService voteItemServiceImpl;

  @Resource
  private VoteStatusRedisService voteStatusRedisServiceImpl;

  @Resource
  private VoteUserClickScoreService voteUserClickScoreServiceImpl;

  @Resource
  private VoteConfigCacheService voteConfigCacheServiceImpl;

  @Resource
  private PKItemCacheService pKItemCacheServiceImpl;

  @ApiOperation("添加PK投票组")
  @PostMapping("/add/{id}")
  public Object addGroup2Pk(@PathVariable("id") String id, @RequestBody PKVoteItemParam params) {
    final VoteStatus voteStatus = voteStatusRedisServiceImpl.get(id);

    final ResultBean<Object> bean = new ResultBean<>();
    if (voteStatus == VoteStatus.OVER) {

      bean.setCode(ResultBean.FAIL);
      bean.setMsg("投票已经结束");
      return bean;
    }

    final String groupId = voteItemServiceImpl.insertOneItem2PK(params, id);
    bean.setData(groupId);
    return bean;
  }

  @ApiOperation("修改PK小组")
  @PutMapping("/modify/{id}/{groupId}")
  public Object modify(
    @PathVariable("id") String id,
    @PathVariable("groupId") String groupId,
    @RequestBody OneGroupParam params) {
    final VoteStatus voteStatus = voteStatusRedisServiceImpl.get(id);

    log.info("params: {}", params);
    final ResultBean<Object> bean = new ResultBean<>();
    if (voteStatus == VoteStatus.OVER || CollectionUtils.isEmpty(params.getParams())) {
      bean.setCode(ResultBean.FAIL);
      bean.setMsg("投票已经结束或者数组为空");
      return bean;
    }
    voteItemServiceImpl.updateOneGroup4PK(params.getParams(), id, groupId);
    // todo: 更新缓存
    return bean;
  }

  @ApiOperation("删除一个小组")
  @DeleteMapping("/delete/{id}/{groupId}")
  public Object deleteONeGroup(
    @PathVariable("id") String id, @PathVariable("groupId") String groupId) {

    final int deleteOneGroup = voteItemServiceImpl.deleteOneGroup(id, groupId);

    return new ResultBean<>(deleteOneGroup);
  }

  @ApiOperation("开启一组投票")
  @PutMapping("/next")
  public Object nextGroup(
    @RequestParam("currentId") String currentId, @RequestParam("next") String nextId) {
    final ResultBean<Object> bean = new ResultBean<>();

    if (StringUtils.isNotEmpty(currentId)) {
      // 结束上一个投票
      voteStatusRedisServiceImpl.put(currentId, VoteStatus.OVER);
    }

    // 开始下一个投票
    final VoteStatus voteStatus = voteStatusRedisServiceImpl.get(nextId);
    if (voteStatus != VoteStatus.COMING) {
      bean.setCode(ResultBean.FAIL);
      bean.setMsg("该投票已经结束");
      return bean;
    }
    voteStatusRedisServiceImpl.put(nextId, VoteStatus.STARTING);

    return bean;
  }

  @ApiOperation("/添加一个用户自己上传的投票信息")
  @PostMapping("/upload/{voteId}")
  public Object uploadUserItemInfo(
    @PathVariable("voteId") String voteId, @RequestBody VoteItemInfoParam param) {

    ResultBean bean = new ResultBean();

    final VoteStatus voteStatus = voteStatusRedisServiceImpl.get(voteId);
    if (voteStatus == VoteStatus.OVER) {

      bean.setCode(ResultBean.FAIL);
      bean.setMsg("当前投票已经结束");
      return bean;
    }

    final ArrayList<VoteItemInfoParam> list = new ArrayList<>();
    list.add(param);
    final VoteConfigTo voteConfigTo = voteConfigCacheServiceImpl.get(voteId);
    voteConfigTo
      .getUsers()
      .add(Transform.toVoteItemInfoVo.apply(Transform.toVoteItem.apply(list).get(0)));
    voteConfigCacheServiceImpl.put(voteId, voteConfigTo);

    VoteItem voteItem = new VoteItem();
    voteItem.setName(param.getName());
    voteItem.setPhotoUrl(param.getPhotoUrl());
    voteItem.setStatus(Byte.valueOf("1"));
    voteItem.setUid(param.getUid());
    voteItem.setBrief(param.getBrief());

    voteItemServiceImpl.insertOneUser(voteItem);

    bean.setMsg("success");
    return bean;
  }

  @ApiOperation("/删除一个用户自己上传的投票信息")
  @PostMapping("/delete/{voteId}/{itemId}")
  public Object deleteUserItemInfo(
    @PathVariable("voteId") String voteId, @PathVariable("itemId") int itemId) {

    ResultBean bean = new ResultBean();

    final VoteStatus voteStatus = voteStatusRedisServiceImpl.get(voteId);
    if (voteStatus == VoteStatus.OVER) {

      bean.setCode(ResultBean.FAIL);
      bean.setMsg("当前投票已经结束");
      return bean;
    }

    final ArrayList<VoteItemInfoParam> list = new ArrayList<>();
    list.removeIf(v -> v.getId().equals(String.valueOf(itemId)));
    final VoteConfigTo voteConfigTo = voteConfigCacheServiceImpl.get(voteId);
    voteConfigTo
      .getUsers()
      .add(Transform.toVoteItemInfoVo.apply(Transform.toVoteItem.apply(list).get(0)));
    voteConfigCacheServiceImpl.put(voteId, voteConfigTo);

    VoteItem voteItem = new VoteItem();
    voteItem.setId(itemId);
    voteItem.setStatus(Byte.valueOf("3"));

    voteItemServiceImpl.updateByItemId(voteItem);

    bean.setMsg("success");
    return bean;
  }

  @ApiOperation("关闭一个小组")
  @PutMapping("/items/stop/{itemId}")
  public Object stopOneGroup(@PathVariable("itemId") String itemId) {

    final ResultBean<Object> bean = new ResultBean<>();
    final VoteStatus voteStatus = voteStatusRedisServiceImpl.get(itemId);
    if (voteStatus != VoteStatus.STARTING) {
      bean.setCode(ResultBean.FAIL);
      bean.setMsg("该投票没有开始");
      return bean;
    }
    voteStatusRedisServiceImpl.put(itemId, VoteStatus.OVER);

    return bean;
  }

  @ApiOperation("重置数据")
  @PutMapping("/reset/{id}/{itemId}")
  public Object reset(@PathVariable("id") String id, @PathVariable("itemId") String groupId) {

    log.info("id: {}, groupId: {}", id, groupId);

    voteStatusRedisServiceImpl.put(groupId, VoteStatus.COMING);

    List<PKVoteConfigParamTo> oneGroup = pKItemCacheServiceImpl.getOneGroup(id, groupId);
    oneGroup =
      oneGroup
        .stream()
        .peek(
          v -> {
            v.setPoll(0);
            v.setScala(0);
          })
        .collect(Collectors.toList());

    oneGroup.forEach(v -> voteUserClickScoreServiceImpl.addScore(id, v.getId(), 0L));

    return new RestResult();
  }
}
