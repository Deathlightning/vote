package xyz.hdzx.web.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author whq
 */
@Slf4j
@Controller
@Api(tags = "投票重定向接口")
public class RedirectController {

  @Resource
  private SelectVoteController selectVoteController;

  @ApiOperation("查询投票的分享url")
  @GetMapping("/goto")
  public String gotoUrl(@RequestParam("id") String id) {
    log.info("url: {}", selectVoteController.shareUrl(id).getData());
    return "redirect:https://" + selectVoteController.shareUrl(id).getData().toString();
  }
}
