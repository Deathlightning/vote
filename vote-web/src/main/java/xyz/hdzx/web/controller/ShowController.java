package xyz.hdzx.web.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.core.redis.PKVoteConfigParamTo;
import xyz.hdzx.core.redis.VoteConfigTo;
import xyz.hdzx.core.vo.VoteItemInfoVo;
import xyz.hdzx.service.PKItemCacheService;
import xyz.hdzx.service.VoteClickNumberService;
import xyz.hdzx.service.VoteConfigCacheService;
import xyz.hdzx.service.VoteStatusRedisService;
import xyz.hdzx.service.VoteUserClickScoreService;
import xyz.hdzx.service.VoteViewNumberService;
import xyz.hdzx.service.task.DefaultGroupCache;

/**
 * 用于显示投票页面
 *
 * @author whq Date: 2018-09-19 Time: 21:36
 */
@Slf4j
@Controller
@Api(tags = "投票展示接口")
public class ShowController {

    @Resource
    private VoteStatusRedisService voteStatusRedisServiceImpl;

    @Resource
    private VoteUserClickScoreService voteUserClickScoreServiceImpl;

    @Resource
    private VoteConfigCacheService voteConfigCacheServiceImpl;

    @Resource
    private VoteViewNumberService voteViewNumberServiceImpl;

    @Resource
    private PKItemCacheService pKItemCacheServiceImpl;

    @GetMapping("/common/{voteId}")
    public ModelAndView common(
            ModelAndView mv,
            @PathVariable("voteId") String voteId,
            @RequestParam("code") String code,
            @RequestParam("state") String state) {

        if (checkParam(mv, voteId, code, state)) {
            return mv;
        }

        VoteConfigTo voteConfigTo = voteConfigCacheServiceImpl.get(voteId);

        if (voteConfigTo == null) {
            // 错误页面
            log.error("voteConfigTo为null");
            mv.addObject("info", "投票已经结束");
            return mv;
        }

        List<VoteItemInfoVo> users = voteConfigTo.getUsers();
        users =
                users
                        .stream()
                        // .filter(v -> v.getStatus() == 1)
                        .peek(
                                v -> {
                                    log.info("vote-item-id: {}", v.getId());
                                    Long score = voteUserClickScoreServiceImpl.getScore(voteId, v.getId());

                                    log.info("vote-item-{} score: {}", v.getId(), score);
                                    if (score == null) {
                                        score = 0L;
                                    }
                                    v.setPoll(Integer.valueOf(score.toString()));
                                })
                        .collect(Collectors.toList());

        log.info("users: {}", users);

        voteConfigTo.setUsers(users);

        mv.addObject(voteConfigTo);
        mv.addObject("code", code);
        mv.addObject("voteStatus", voteStatusRedisServiceImpl.get(voteId) == VoteStatus.STARTING);

        mv.setViewName("common");
        return mv;
    }


    @GetMapping("/pk/{id}")
    public ModelAndView pk(
            ModelAndView mv,
            @PathVariable("id") String id,
            @RequestParam("code") String code,
            @RequestParam("state") String state) {

        //验证投票状态
        if (checkParam(mv, id, code, state)) {
            return mv;
        }

        // 拿到缓存投票的信息
        VoteConfigTo voteConfig = voteConfigCacheServiceImpl.get(id);

        //  不存在则返回错误页面
        if (null == voteConfig) {
            // 错误页面
            log.error("voteConfigTo为null");
            return mv;
        }
        log.info("voteConfig-pk: {}", voteConfig);

        // 拿到pk投票的所有组
        final Map<String, List<PKVoteConfigParamTo>> allGroup = pKItemCacheServiceImpl.getAllGroup(id);

        log.info("allGroup: {}", allGroup);

        // 循环遍历拿到所有投票项中唯一开始的的投票项
        final List<String> startingVote =
                allGroup
                        .keySet()
                        .stream()
                        .filter(v -> voteStatusRedisServiceImpl.get(v) == VoteStatus.STARTING)
                        .collect(Collectors.toList());
        // 开启投票按钮
        mv.addObject("isStart", true);

        List<PKVoteConfigParamTo> startingVoteList;
        // 是否存在开启项，没有则启用默认组
        if (startingVote.size() == 0) {
            startingVoteList = allGroup.get(DefaultGroupCache.CACHE.get(id));

            if (startingVoteList == null) {
                mv.addObject("info", "投票即将开始, 请敬请期待");
                return mv;
            }
            // 关闭投票按钮
            mv.addObject("isStart", false);
        } else {
            startingVoteList = allGroup.get(startingVote.get(0));
        }
        log.info("startingVoteList: {}", startingVoteList);


        double sum = 0;
        for (PKVoteConfigParamTo user : startingVoteList) {
            // 得到用户点击量
            Long score = voteUserClickScoreServiceImpl.getScore(id, user.getId());
            user.setPoll(score.intValue());
            sum += user.getPoll();
        }

        if (sum != 0) {
            double finalSum = sum;
            startingVoteList =
                    startingVoteList
                            .stream()
                            .peek(
                                    v -> {
                                        double value = ((int) (v.getPoll() / finalSum * 10000)) / 100.0;
                                        v.setScala(value);
                                    })
                            .collect(Collectors.toList());
        } else {
            startingVoteList =
                    startingVoteList.stream().peek(v -> v.setScala(0)).collect(Collectors.toList());
        }

        mv.addObject("voteConfig", voteConfig);
        mv.addObject("users", startingVoteList);
        mv.addObject("code", code);
        mv.addObject("moduleId", voteConfig.getModuleId());
        mv.setViewName("votepks");
        return mv;
    }

    @GetMapping("/user/{id}")
    public ModelAndView user(
            ModelAndView mv,
            @PathVariable("id") String id,
            @RequestParam("code") String code,
            @RequestParam("state") String state) {

        return common(mv, id, code, state);
    }

    //对参数及投票状态进行验证
    private boolean checkParam(ModelAndView mv, String voteId, String code, String state) {
        log.info("voteId: {}", voteId);
        log.info("code: {}", code);
        log.info("state: {}", state);

        mv.setViewName("over");

        // 增加点击量
        voteViewNumberServiceImpl.incrValue(voteId);

        if (StringUtils.isEmpty(code) || StringUtils.isEmpty(state) || !state.equals("vote")) {
            log.error("code或者vote不正确");
            return true;
        }

        // 判断投票是否截止
        if (voteStatusRedisServiceImpl.get(voteId) == VoteStatus.OVER) {
            log.error("投票已经截止-over");
            mv.addObject("info", "投票已经结束");
            return true;
        }
        return false;
    }
}
