package xyz.hdzx.web.controller;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.hdzx.core.vo.PluginInfo;
import xyz.hdzx.service.VoteInfoService;

/**
 * @author whq
 */
@Slf4j
@RestController
@RequestMapping("/api")
@Api(tags = "投票对活动接口")
public class ApiController {

    @Resource
    private VoteInfoService voteInfoServiceImpl;

    @Resource
    private SelectVoteController selectVoteController;

    @GetMapping("/voteInfo/{id}")
    public Object getVoteInfoList(@PathVariable("id") int id) {

        final List<PluginInfo> pluginInfos = voteInfoServiceImpl.selectByFrequencyId(id);
        return pluginInfos
                .stream()
                .peek(v -> v.setUrl(String.valueOf(selectVoteController.getVoteUrl(v.getId()).getData())))
                .collect(Collectors.toList());
    }

    @GetMapping("/voteInfoBy/{moduleId}")
    public Object getVoteInfoListByModule(@PathVariable("moduleId") int moduleId) {

        log.info("module: {}", moduleId);

        final List<PluginInfo> pluginInfos = voteInfoServiceImpl.selectByModuleId(moduleId);

        return pluginInfos
                .stream()
                .peek(
                        v ->
                                v.setUrl(
                                        String.valueOf(selectVoteController.getVoteUrl(v.getId(), true).getData())))
                .collect(Collectors.toList());
    }
}
