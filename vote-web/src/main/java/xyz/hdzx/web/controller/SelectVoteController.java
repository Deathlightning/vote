package xyz.hdzx.web.controller;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.hdzx.common.FileUtil;
import xyz.hdzx.core.bean.ResultBean;
import xyz.hdzx.core.entity.VoteItemExt;
import xyz.hdzx.core.model.VoteInfo;
import xyz.hdzx.core.model.VoteItem;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.core.model.enums.VoteType;
import xyz.hdzx.core.param.SelectVoteParam;
import xyz.hdzx.core.redis.UserVoteAnalyzeDataTo;
import xyz.hdzx.core.vo.UserVoteAnalyzeDataVo;
import xyz.hdzx.service.VoteAnalyzeService;
import xyz.hdzx.service.VoteInfoService;
import xyz.hdzx.service.VoteItemService;
import xyz.hdzx.service.VoteStatusRedisService;
import xyz.hdzx.service.impl.RemoteApi;

/**
 * @author whq Date: 2018-10-09 Time: 13:48
 */
@Slf4j
@Api("查询数据接口")
@RestController
public class SelectVoteController {

  @Resource
  private RemoteApi remoteApi;

  @Resource
  private VoteInfoService voteInfoServiceImpl;

  @Resource
  private VoteAnalyzeService voteAnalyzeServiceImpl;

  @Resource
  private VoteStatusRedisService voteStatusRedisServiceImpl;

  @Resource
  private VoteItemService voteItemServiceImpl;

  @ApiOperation("查询已经开始的投票")
  @ApiImplicitParams(@ApiImplicitParam(value = "param", paramType = "SelectVoteParam"))
  @PostMapping("/start")
  public Object getStartingVote(@RequestBody SelectVoteParam param) {

    return voteInfoServiceImpl.selectStarting(param);
  }

  @ApiOperation("查询即将开始的投票")
  @ApiImplicitParams(@ApiImplicitParam(value = "param", paramType = "SelectVoteParam"))
  @PostMapping("/coming")
  public Object getComingVote(@RequestBody SelectVoteParam param) {

    return voteInfoServiceImpl.selectComing(param);
  }

  @ApiOperation("查询已经结束的投票")
  @ApiImplicitParams(@ApiImplicitParam(value = "param", paramType = "SelectVoteParam"))
  @PostMapping("/over")
  public Object getOverVote(@RequestBody SelectVoteParam param) {

    return voteInfoServiceImpl.selectOver(param);
  }

  @ApiOperation("查询投票的分享url")
  @GetMapping("/url/{id}")
  public ResultBean<Object> getVoteUrl(@PathVariable("id") String id) {
    ResultBean<Object> bean = new ResultBean<>();

    final VoteStatus voteStatus = voteStatusRedisServiceImpl.get(id);
    if (VoteStatus.OVER == voteStatus){
      bean.setCode(ResultBean.FAIL);
      bean.setMsg("投票已经截止");
      return bean;
    }
    // 得到一个简单投票url路径
    String url = voteInfoServiceImpl.getUrl(id);

    bean.setData(url);
    return bean;
  }

  ResultBean<Object> shareUrl(String id) {
    return getVoteUrl(id, true);
  }

  ResultBean<Object> getVoteUrl(String id, boolean isEncode) {
    ResultBean<Object> bean = new ResultBean<>();
    bean.setCode(ResultBean.FAIL);

    if (StringUtils.isEmpty(id)) {
      bean.setMsg("id不能空");
      return bean;
    }

    final VoteInfo voteInfo = voteInfoServiceImpl.selectById(id);

    if (voteInfo == null) {
      bean.setMsg("不存在该投票");
      return bean;
    }

    String suffix = "/";
    switch (VoteType.getType(voteInfo.getType())) {
      case COMMON_VOTE:
        suffix += "common/";
        break;
      case PK_VOTE:
        suffix += "pk/";
        break;
      case USER_DEFINITION_VOTE:
        suffix += "user/";
        break;
    }

    if (StringUtils.isNotEmpty(suffix)) {
      suffix += id;
      String url = remoteApi.getCodeURLFromWx(suffix, isEncode);

      bean.setCode(ResultBean.SUCCESS);
      bean.setData(url);
      return bean;
    }

    return bean;
  }
  @ApiOperation("导出数据")
  @GetMapping("/export/{id}")
  public void exportData(@PathVariable("id") String id, HttpServletResponse response) {
    final Map<String, UserVoteAnalyzeDataTo> allResult = voteAnalyzeServiceImpl.getAllResult(id);
    final VoteInfo voteInfo = voteInfoServiceImpl.selectById(id);
    final Map<Integer, String> allInfoByVoteId =
      voteItemServiceImpl
        .getAllByVoteId(id)
        .stream()
        .collect(Collectors.toMap(VoteItem::getId, VoteItem::getName));

    log.info("voteInfo: {}", voteInfo);
    log.info("allResult: {}", allResult);
    log.info("allInfoByVoteId: {}", allInfoByVoteId);
    log.info("id: {}", id);

    List<UserVoteAnalyzeDataVo> userVoteAnalyzeDataVos =
      JSON.parseArray(JSON.toJSONString(allResult.values()), UserVoteAnalyzeDataVo.class);

    final List<UserVoteAnalyzeDataVo> result =
      allResult
        .keySet()
        .stream()
        .map(
          v -> {
            UserVoteAnalyzeDataVo vo = new UserVoteAnalyzeDataVo();
            vo.setCurrentNumber(allResult.get(v).getCurrentNumber());
            vo.setName(allInfoByVoteId.get(Integer.valueOf(v)));
            vo.setScale(allResult.get(v).getScale() * 100);
            return vo;
          })
        .collect(Collectors.toList());

    log.info("result: {}", result);
    FileUtil.exportExcel(
      result,
      voteInfo.getTitle(),
      "1",
      UserVoteAnalyzeDataVo.class,
      voteInfo.getTitle() + "统计表.xls",
      true,
      response);
  }

  @ApiOperation("返回用户点击分析结果")
  @GetMapping("/analyze/{id}")
  public Object analyze(@PathVariable("id") String id) {

    final Map<String, UserVoteAnalyzeDataTo> allResult = voteAnalyzeServiceImpl.getAllResult(id);

    return new ResultBean<>(allResult.values());
  }

  @ApiOperation("根据条件进行搜索")
  @ApiImplicitParams(@ApiImplicitParam(value = "param", paramType = "SelectVoteParam"))
  @PostMapping("/search")
  public Object searchVote(@RequestBody SelectVoteParam param) {
    log.info("param: {}", param);

    return voteInfoServiceImpl.selectBy(param);
  }

  @ApiOperation("根据id查询投票信息")
  @GetMapping("/info/{id}")
  public Object getInfo(@PathVariable("id") String id) {
    return new ResultBean<>(voteInfoServiceImpl.selectById(id));
  }

  @ApiOperation("根据投票id查询投票项--只有Pk投票使用")
  @GetMapping("/items/{voteId}")
  public Object getItemsById(@PathVariable("voteId") String voteId) {
    final ResultBean<Object> bean = new ResultBean<>();

    final VoteInfo voteInfo = voteInfoServiceImpl.selectById(voteId);
    log.info("voteInfo: {}", voteInfo);

    if (voteInfo != null && voteInfo.getType() != VoteType.PK_VOTE.getValue()) {
      bean.setCode(ResultBean.FAIL);
      bean.setMsg("不是pk投票");
      return bean;
    }

    final List<List<VoteItemExt>> allInfoByVoteId = voteItemServiceImpl.getAllInfoByVoteId(voteId);
    log.info("allInfoByVoteId: {}", allInfoByVoteId);
    bean.setData(allInfoByVoteId);
    return bean;
  }
}
