package xyz.hdzx.web.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import xyz.hdzx.core.bean.ResultBean;
import xyz.hdzx.core.model.VoteInfo;
import xyz.hdzx.core.model.VoteItem;
import xyz.hdzx.core.model.enums.VoteStatus;
import xyz.hdzx.core.model.enums.VoteType;
import xyz.hdzx.core.redis.PKVoteConfigParamTo;
import xyz.hdzx.core.redis.UserVoteAnalyzeDataTo;
import xyz.hdzx.core.redis.VoteConfigTo;
import xyz.hdzx.core.vo.AnalyzeVo;
import xyz.hdzx.core.vo.AnalyzeVo.Item;
import xyz.hdzx.service.PKItemCacheService;
import xyz.hdzx.service.VoteAnalyzeService;
import xyz.hdzx.service.VoteClickNumberService;
import xyz.hdzx.service.VoteConfigCacheService;
import xyz.hdzx.service.VoteInfoService;
import xyz.hdzx.service.VoteItemService;
import xyz.hdzx.service.VoteStatusRedisService;
import xyz.hdzx.service.VoteViewNumberService;

/**
 * @author whq
 */
@Slf4j
@RestController
@Api(tags = "投票分析接口")
public class AnalyzeController {

  @Resource
  private VoteClickNumberService voteClickNumberServiceImpl;

  @Resource
  private VoteViewNumberService voteViewNumberServiceImpl;

  @Resource
  private VoteAnalyzeService voteAnalyzeServiceImpl;

  @Resource
  private VoteConfigCacheService voteConfigCacheServiceImpl;

  @Resource
  private VoteStatusRedisService voteStatusRedisServiceImpl;

  @Resource
  private VoteItemService voteItemServiceImpl;

  @Resource
  private VoteInfoService voteInfoServiceImpl;

  @Resource
  private PKItemCacheService pKItemCacheServiceImpl;

  @GetMapping("/data/{id}")
  public Object getCommonVote(@PathVariable("id") String voteId) {

    if (voteStatusRedisServiceImpl.get(voteId) == VoteStatus.OVER) {
      log.info("over: db");
      return fromDB(voteId);
    }
    final VoteConfigTo config = voteConfigCacheServiceImpl.get(voteId);

    if ("2".equals(config.getType())) {
      log.info("pk: info");
      return pkVoteInfo(voteId);
    }
    log.info("common: info");

    final Map<String, UserVoteAnalyzeDataTo> allResult =
      voteAnalyzeServiceImpl.getAllResult(voteId);

    final List<VoteItem> allInfoByVoteId = voteItemServiceImpl.getAllByVoteId(voteId);
    final Map<Integer, String> id2Name =
      allInfoByVoteId.stream().collect(Collectors.toMap(VoteItem::getId, VoteItem::getName));

    final Long clickNum = voteClickNumberServiceImpl.get(voteId);
    final Long viewNum = voteViewNumberServiceImpl.get(voteId);

    final List<Item> items =
      allResult
        .keySet()
        .stream()
        .map(
          v ->
            new Item(
              id2Name.get(Integer.valueOf(v)),
              allResult.get(v).getCurrentNumber(),
              allResult.get(v).getScale()))
        .collect(Collectors.toList());

    final AnalyzeVo vo =
      AnalyzeVo.builder()
        .ClickNum(clickNum.intValue())
        .endTime(config.getEndTime())
        .startTime(config.getStartTime())
        .title(config.getTitle())
        .viewNum(viewNum.intValue())
        .items(items)
        .build();

    log.info("vo-cache: {}", vo);

    return new ResultBean<>(vo);
  }

  private Object pkVoteInfo(String voteId) {

    ResultBean<AnalyzeVo> bean = new ResultBean<>();

    final VoteConfigTo config = voteConfigCacheServiceImpl.get(voteId);
    // if (String.valueOf(VoteType.PK_VOTE.getValue()).equals(config.getType())) {
    //   bean.setCode(ResultBean.FAIL);
    //   bean.setMsg("这不是pk投票");
    //   return bean;
    // }

    // groupId to info
    final Map<String, List<PKVoteConfigParamTo>> allGroup =
      pKItemCacheServiceImpl.getAllGroup(voteId);

    log.info("allGroup: {}", allGroup);

    final Long clickNum = voteClickNumberServiceImpl.get(voteId);
    final Long viewNum = voteViewNumberServiceImpl.get(voteId);

    final List<List<Item>> result =
      allGroup
        .keySet()
        .stream()
        .map(
          v ->
            allGroup
              .get(v)
              .stream()
              .map(
                vv -> {
                  log.info("info-item: {}", v);
                  Item item = new Item();
                  item.setName(vv.getTitle());
                  item.setPoll(vv.getPoll());
                  item.setScale(vv.getScala());
                  return item;
                })
              .collect(Collectors.toList()))
        .collect(Collectors.toList());

    final AnalyzeVo vo =
      AnalyzeVo.builder()
        .ClickNum(clickNum.intValue())
        .endTime(config.getEndTime())
        .startTime(config.getStartTime())
        .title(config.getTitle())
        .viewNum(viewNum.intValue())
        .lists(result)
        .build();

    bean.setData(vo);
    log.info("vo-cache-pk: {}", vo);
    return bean;
  }

  private Object fromDB(String voteId) {

    final Map<String, UserVoteAnalyzeDataTo> allResult =
      voteAnalyzeServiceImpl.getAllResult(voteId);

    final List<VoteItem> allInfoByVoteId = voteItemServiceImpl.getAllByVoteId(voteId);
    final Map<Integer, String> id2Name =
      allInfoByVoteId.stream().collect(Collectors.toMap(VoteItem::getId, VoteItem::getName));

    log.info("id2Name: {}", id2Name);
    log.info("allResult: {}", allResult);

    final VoteInfo voteInfo = voteInfoServiceImpl.selectById(voteId);

    final List<Item> items =
      allResult
        .keySet()
        .stream()
        .map(
          v ->
            new Item(
              id2Name.get(Integer.valueOf(v)),
              allResult.get(v).getCurrentNumber(),
              allResult.get(v).getScale()))
        .collect(Collectors.toList());

    final AnalyzeVo vo =
      AnalyzeVo.builder()
        .title(voteInfo.getTitle())
        .ClickNum(voteInfo.getTotalClick())
        .endTime(voteInfo.getEndTime())
        .startTime(voteInfo.getStartTime())
        .title(voteInfo.getTitle())
        .viewNum(voteInfo.getViewNum())
        .items(items)
        .build();

    log.info("vo-db: {}", vo);
    return new ResultBean<>(vo);
  }
}
