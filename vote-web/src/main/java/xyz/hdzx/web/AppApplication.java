package xyz.hdzx.web;

import static org.springframework.boot.SpringApplication.run;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import xyz.hdzx.service.config.AsyncThreadConfig;
import xyz.hdzx.service.config.SchedulingConfig;

/**
 * @author whq Date: 2018-09-19 Time: 19:37
 */
@PropertySource({"service-${spring.profiles.active}.properties",
    "application-${spring.profiles.active}.properties"
})
@EnableConfigurationProperties({AsyncThreadConfig.class, SchedulingConfig.class} )
@EnableAsync
@EnableScheduling
@ComponentScan("xyz.hdzx")
@MapperScan("xyz.hdzx")
@EnableAspectJAutoProxy
@EnableSwagger2
@EnableDiscoveryClient
@EnableEurekaClient
@SpringBootApplication(scanBasePackages = {"xyz.hdzx"})
public class AppApplication {

    public static void main(String[] args) {
        run(AppApplication.class);
    }

    @Bean
    @LoadBalanced
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
    
}
