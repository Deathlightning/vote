package xyz.hdzx.web.exception;

/**
 * @author whq Date: 2018-10-07 Time: 13:18
 */
public class ParamException extends Throwable {

    public ParamException() {
    }

    public ParamException(String message) {
        super(message);
    }
}
