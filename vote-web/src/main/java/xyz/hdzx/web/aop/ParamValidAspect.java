package xyz.hdzx.web.aop;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import xyz.hdzx.core.annotations.KeyValue;
import xyz.hdzx.core.annotations.ParamValid;
import xyz.hdzx.core.bean.ResultBean;
import xyz.hdzx.web.exception.ParamException;

/**
 * @author whq Date: 2018-10-06 Time: 17:34
 */
@Slf4j
@Aspect
@Component
public class ParamValidAspect {

    public ParamValidAspect() {
        System.out.println("paramValidAspect");
    }

    @Pointcut(value = "@annotation(paramValid)", argNames = "paramValid")
    public void cutParamValid(ParamValid paramValid) {
    }

    @Around(value = "cutParamValid(paramValid)", argNames = "joinPoint,paramValid")
    public Object paramValid(ProceedingJoinPoint joinPoint, ParamValid paramValid)
        throws Throwable {

        // todo: 方法参数校验
        ResultBean<?> result = valid(joinPoint, paramValid);

        if (result != null) {
            return result;
        }

        log.info("参数检验正确");

        return joinPoint.proceed();
    }

    private ResultBean<?> valid(ProceedingJoinPoint joinPoint, ParamValid paramValid) {

        final Object[] args = joinPoint.getArgs();
        // 拿到非请求和响应参数
        final List<Object> params = Arrays.stream(args)
            .filter(v -> !((v instanceof HttpServletRequest) || (v instanceof HttpServletResponse)))
            .collect(Collectors
                .toList());

        Map<String, Map<String, Object>> class2Fields = new HashMap<>(5);

        params.forEach(p ->
            class2Fields
                .put(p.getClass().getSimpleName().toLowerCase(), getAllFields(p, paramValid)));

        Set<Object> errorValue = new HashSet<>();

        params.forEach(p -> {
            final Map<String, Object> fields = class2Fields
                .get(p.getClass().getSimpleName().toLowerCase());

            errorValue
                .addAll(fields.keySet().stream()
                    .filter(v -> (Objects.isNull(fields.get(v))) || ("".equals(fields.get(v))))
                    .collect(Collectors.toSet()));

            // todo: 以后删除
            if (!CollectionUtils.isEmpty(errorValue)) {
                System.out.println(errorValue);
            }

            for (KeyValue keyValue : paramValid.value()) {
                if (StringUtils.isEmpty(keyValue.key())) {
                    continue;
                }

                final Object value = fields.get(keyValue.key());
                // todo：待完善
                switch (keyValue.type()) {
                    case EQ:
                    case GE:
                    case GT:
                    case LE:
                    case LT:
                    case NOT_EMPTY:
                    default:
                        if (StringUtils.isEmpty((CharSequence) value)) {
                            errorValue.add(keyValue.key());
                        }
                }
            }
        });
        if (!errorValue.isEmpty()) {
            return new ResultBean<>(new ParamException(paramValid.message()));
        }

        return null;
    }

    /**
     * 得到一个类的所有属性
     *
     * @param object 参数对象
     * @param paramValid 注解，用来获取不检查对象
     * @return 属性和值对应的map
     */
    private Map<String, Object> getAllFields(Object object,
        ParamValid paramValid) {

        if (object == null) {
            return null;
        }

        final Set<Object> notValidParam = Arrays.stream(paramValid.notValidParam())
            .map(String::toLowerCase).collect(Collectors.toSet());

        Map<String, Object> field2Value = new HashMap<>(50);

        Class<?> clazz = object.getClass();

        for (; clazz != Object.class; clazz = clazz.getSuperclass()) {
            final Field[] selfFields = clazz.getDeclaredFields();

            Arrays.stream(selfFields)
                .filter(f -> !notValidParam.contains(f.getName().toLowerCase())).forEach(f -> {

                f.setAccessible(true);
                try {
                    field2Value.put(f.getName().toLowerCase(), f.get(object));
                } catch (IllegalAccessException e) {
                    log.error("获取属性值错误: {}", e);
                }
            });
        }

        return field2Value;
    }

}