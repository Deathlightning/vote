package xyz.hdzx.web.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import xyz.hdzx.web.filter.AccessTokenVerifyInterceptor;

/**
 * @author whq Date: 2018-10-10 Time: 22:15
 */
@Slf4j
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

  @Bean
  AccessTokenVerifyInterceptor accessTokenVerifyInterceptor() {
    return new AccessTokenVerifyInterceptor();
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry
      .addInterceptor(accessTokenVerifyInterceptor())
      // 单个星号只能拦截／后边一级的url路径
      // 多级的要配置成两个星号（/**）
      // .addPathPatterns("/**")
      // 不拦截的url规则
      .excludePathPatterns(
        "/robots.txt",
        "/sockjs-node/**",
        "",
        "/actuator/info",
        "/error",
        "/static/**",
        // swagger资源
        "/swagger-ui.html",
        "/swagger-resources/**",
        "/webjars/**",
        "/images/**",
        "/v2/**",
        "/configuration/*",
        // 投票页面
        "/common/**",
        "/pk/**",
        "/goto/**",
        "/js/**",
        "/css/**",
        "/img/**",
        "/title.ico",
        "/vote/**",
        "/export/**",
        "/api/**");
  }
}
