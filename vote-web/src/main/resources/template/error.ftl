<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="utf-8">
    <title>签到</title>
</head>

<body>

<script>

    //微信浏览器中，alert弹框不显示域名
    (function () {
        //先判断是否为微信浏览器
        var ua = window.navigator.userAgent.toLowerCase();
        if (ua.match(/MicroMessenger/i) == 'micromessenger') {
            //重写alert方法，alert()方法重写，不能传多余参数
            window.alert = function (name) {
                var iframe = document.createElement("IFRAME");
                iframe.style.display = "none";
                iframe.setAttribute("src", 'data:text/plain');
                document.documentElement.appendChild(iframe);
                window.frames[0].window.alert(name);
                iframe.parentNode.removeChild(iframe);
            }
        }
    })();

    //微信浏览器中，confirm弹框不显示域名
    (function () {
        //先判断是否为微信浏览器
        var ua = window.navigator.userAgent.toLowerCase();
        if (ua.match(/MicroMessenger/i) == 'micromessenger') {
            //重写confirm方法，confirm()方法重写，不能传多余参数
            window.confirm = function (message) {
                var iframe = document.createElement("IFRAME");
                iframe.style.display = "none";
                iframe.setAttribute("src", 'data:text/plain,');
                document.documentElement.appendChild(iframe);
                var alertFrame = window.frames[0];
                var result = alertFrame.window.confirm(message);
                iframe.parentNode.removeChild(iframe);
                return result;
            };
        }
    })();

</script>
<!--小程序和公众号都可以点确定回去-->
<script type="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.3.2.js"></script>
<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script th:inline="javascript">
    alert("抱歉，投票异常，请关闭页面");
    wx.miniProgram.navigateBack({});
    if (typeof window.WeixinJSBridge == "undefined") {
        $(document).on("WeixinJSBridgeReady", function() {
            WeixinJSBridge.invoke('closeWindow',{},function(){

            });
        });
    } else {
        WeixinJSBridge.invoke('closeWindow',{},function(){
        });
    }


</script>



</body>

</html>