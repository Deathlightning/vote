<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- 本地CSS文件 -->
  <link rel="stylesheet" href="https://hudong.hndt.com/h5/cdn/vote/css/votepk.css">
  <title>${voteConfig.title}</title>
</head>

<body>
<div style="display:flex;align-items:center" onclick="returnPage()">
    <img style="width: 30px; height:30px ; " src="https://hudong.hndt.com/h5/cdn/vote/img/icon-back.png">
    <span style="font-size: x-large; color: white;">返回</span>
</div>
<!--两个人PK-->
<div class="container">
  <div class="info-warpper">
    <!-- 用户信息面板  -->
        <#list users as user>
          <div class="info-warpper-3">
            <div class="image-border2">
              <#if user.bgUrl ?? >
                <img class="image1" src="${user.bgUrl}" alt="${user.title}">
              <#else>
                <img class="image1" src="https://hudong.hndt.com/h5/cdn/logo_300.png" alt="河南电台">
              </#if>
            </div>
            <div class="center" style="">
              <div class="info">姓名：${user.title}</div>
              <#if isStart>
                <button class="button-vote" id="vote-item-${user.id}">投TA一票</button>
              </#if>
              <div class="progrss-warpper2">
                <small class="rate-left1">${user.scala}%</small>
                <div class="progress progress-right">
                  <div class="progress progress-left" style="width: ${user.scala}%;"></div>
                </div>
              </div>
            </div>
          </div>
        </#list>
  </div>
</div>

<div class="text-info">
  <small>下一位挑战者是谁？？</small>
  <button class="button-vote2" onclick="refreshPage()">刷新</button>
</div>

<!-- 弹幕展示区 -->
<#--<section id="dm">-->
<#--<div class="dm_mask"></div>-->
<#--<div class="dm_show">-->
<#--</div>-->
<#--</section>-->
<#--<section id="promulgate">-->
<#--<input type="text" class="pro-text">-->
<#--<span class="pro-put">发送</span>-->
<#--</section>-->
<#--<footer>河南广播云提供技术支持</footer>-->
<#--</div>-->

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.wilddog.com/js/client/current/wilddog.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script type="application/javascript">
    function refreshPage() {
        window.location.href=window.location.href+"&test="+Math.ceil(Math.random()*10);
        //10000*Math.random();
        // window.location.reload();
    }
</script>
<script type="text/javascript">
  <#--var moduleId = ${moduleId};-->
  // if (moduleId != null) {
  //   console.log(moduleId)
  // }
  $(function () {
    // 创建一个野狗对象
    var ref = new Wilddog("https://chenwg-demo1.wilddogio.com/");
    var arr = [];
    //点击发送按钮将数据添加到野狗云
    $(".pro-put")
    .on("click", function () {
      var mess = $(".pro-text").val();
      ref.child("message").push(mess);
      $(".pro-text").val("");
    })
    //点击清空按钮将数据从野狗云和页面清除
    .next()
    .on("click", function () {
      ref.remove();
      arr = [];
      $(".dm_show").empty();
    });
    //按键点击回车执行发送操作
    $(".pro-text").on("keydown", function (e) {
      if (e.keyCode == 13) {
        $(".pro-put").trigger("click");
      }
    });
    //监听云端数据变更，云端数据变化，弹幕框里数据也跟着变化。
    ref.child("message").on("child_added", function (snapshot) {
      var text = snapshot.val();
      arr.push(text);
      var textObj = $('<div class="dm_mess">' + text + "</div>");
      $(".dm_show").append(textObj);
      // moveObj(textObj);
    });

    ref.on("child_removed", function () {
      arr = [];
      $(".dm_show").empty();
    });

    //按照时间规则显示弹幕内容。
    //获取dm_mask对于当前窗口的相对偏移
    var topMin = $(".dm_mask").offset().top + 30;
    var topMax = topMin + $(".dm_mask").height() - 30;
    var _top = topMin;

    //定义弹幕移动
    var moveObj = function (obj) {
      // 设置目标位置
      var _left = $(".dm_mask").width() - obj.width();
      //高度不断往下不重叠
      _top = _top + 50;
      // 如果高度超出最底下-50范围则返回到顶部
      if (_top > topMax - 50) {
        _top = topMin;
      }
      //设置弹幕的css值
      obj.css({
        left: _left,
        top: _top,
        color: getRandomColor()
      });
      // 设置弹幕不同时间出现
      var time = 10000 + 10000 * Math.random();
      // 弹幕动画,动画完成则从dom删除
      obj.animate({
            // left: "-" + _left + "px"
            left: 60 + "px"
          },
          time,
          function () {
            obj.remove();
          }
      );
    };

    //定义随机颜色函数
    var getRandomColor = function () {
      return (
          "#" +
          (function (h) {
            return new Array(7 - h.length).join("0") + h;
          })(((Math.random() * 0x1000000) << 0).toString(16))
      );
    };

    //每3000毫秒后开始随机抽取弹幕打上
    var getAndRun = function () {
      if (arr.length > 0) {
        var n = Math.floor(Math.random() * arr.length + 1) - 1;
        var textObj = $('<div class="dm_mess">' + arr[n] + "</div>");
        $(".dm_show").append(textObj);
        moveObj(textObj);
      }

      setTimeout(getAndRun, 3000);
    };

    jQuery.fx.interval = 50;
    getAndRun();
  });

  $(document).on("click", ".button-vote", function () {
    var id = this.id;
    $.post({
      url: "../vote/${voteConfig.id}",
      contentType: "application/json;charset=utf-8",
      data: JSON.stringify({voteItemId: id, code: "${code}"}),
      success: function (result) {
        alert(result);
        window.location.reload();
      },
      error: function (error) {
        alert(error);
      }
    });
  });
  //返回小程序页面
  function returnPage() {
      wx.miniProgram.navigateBack({});
      console.log("返回互动页面");
  }
</script>
</body>

</html>