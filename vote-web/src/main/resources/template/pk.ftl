<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>${voteConfig.title}</title>

  <style>
    body {
      margin: 0;
      padding: 0;
      background-color: #1d0030;
    }

    .container .info-warpper {
      text-align: center;
    }

    .info-warpper-1 {
      margin-top: 20%;
      display: inline-block;
    }

    .info-warpper-2 {
      margin-left: 10%;
      display: inline-block;
    }

    .image-border {
      border: 2px #00ffcc solid;
      padding: 2px;
      border-radius: 50%;
    }

    .image1 {
      width: 120px;
      height: 120px;
      border: 2px #cc3366 solid;
      border-radius: 60%;
    }

    .info {
      margin: 10% 0;
      color: #ffffff;
    }

    .button-vote {
      width: 120px;
      border-radius: 20px;
      height: 30px;
      line-height: 15px;
      padding-bottom: 4px;
      background-color: #cc0066;
      color: #ffffff;
    }

    .container .progrss-warpper {
      margin-top: 5%;
      color: #ffffff;
      text-align: center;
    }

    .progress {
      height: 20px;
    }

    .rate-left {
      margin-right: 55%;
    }

    .progress-left {
      background: #66ffcc;
      border-top-left-radius: 20px;
      border-bottom-left-radius: 20px;
    }

    .progress-right {
      width: 70%;
      margin-right: auto;
      margin-left: auto;
      background: #cc0099;
      border-radius: 20px;
    }

    .text-info {
      margin-top: 5%;
      margin-left: 15%;
      color: #ffffff;
      font-size: 20px;
    }
  </style>

</head>

<body>
<div class="container">
  <div class="info-warpper">
    <div class="info-warpper-1">
      <div class="image-border">
        <img class="image1"
             src="${user1.bgUrl}"
             alt="${user1.title}">
      </div>
      <div class="info">房主：${user1.title}</div>
      <button class="button-vote" id="vote-item-${user1.id}">投TA一票</button>
    </div>
    <div class="info-warpper-2">
      <div class="image-border">
        <img class="image1"
             src="${user2.bgUrl}"
             alt="${user2.title}">
      </div>
      <div class="info">挑战者：${user2.title}</div>
      <button class="button-vote" id="vote-item-${user2.id}">投TA一票</button>
    </div>
  </div>
  <div class="progrss-warpper">
    <#if user1.scala == 0 && user2.scala == 0>
      <small class="rate-left">50%</small>
      <small class="rate-right">50%</small>
      <div class="progress progress-right">
        <!-- width为右边进度 -->
        <div class="progress progress-left" style="width: 50%;"></div>
      </div>
    <#else>
    <small class="rate-left">${user1.scala}%</small>
    <small class="rate-right">${user2.scala}%</small>
        <div class="progress progress-right">
          <!-- width为右边进度 -->
          <div class="progress progress-left" style="width: ${user1.scala}%;"></div>
        </div>
    </#if>
  </div>
  <div class="text-info">
    <small>下一位挑战者是谁？？</small>
  </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script
    src="https://code.jquery.com/jquery-3.3.1.js"
    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>

<script type="text/javascript">
  $(document).on("click", ".button-vote", function () {
    var id = this.id;
    $.post({
      url: "../vote/${voteConfig.id}",
      contentType: "application/json;charset=utf-8",
      data: JSON.stringify({voteItemId: id, code: "${code}"}),
      success: function (result) {
        alert(result);
        window.location.reload();
      },
      error: function (error) {
        alert(error);
      }
    });
  });
</script>
</body>

</html>