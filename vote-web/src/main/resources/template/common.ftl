<!DOCTYPE html>
<!-- saved from url=(0053)https://a.weixin.hndt.com/h5/2018dianshang/index.html -->
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <script src="https://hudong.hndt.com/h5/cdn/vote/js/hm.js"></script>
  <link rel="stylesheet" href="https://hudong.hndt.com/h5/cdn/vote/css/app-7b311174b5.css">
  <link rel="stylesheet" href="https://hudong.hndt.com/h5/cdn/vote/css/weui.min.css">
  <title>${voteConfigTo.title}</title>
  <link rel="shortcut icon" href="https://a.weixin.hndt.com/boom/favicon.ico" type="image/x-icon">
  <style>
    body.ignore .g-banner {
      width: 100%;
      background: url(${voteConfigTo.bgUrl}) 50% no-repeat;
      background-size: cover
    }
  </style>
  <style>
    .black_overlay {
      display: none;
      position: absolute;
      top: 0%;
      left: 0%;
      width: 100%;
      height: 100%;
      background-color: black;
      z-index: 1001;
      -moz-opacity: 0.8;
      opacity: .80;
      filter: alpha(opacity=80);
    }

    .white_content {
      display: none;
      position: fixed;
      top: calc(10% - 16px);
      left: calc(10% - 16px);
      width: 80%;
      height: 80%;
      border: 16px solid lightblue;
      background-color: white;
      z-index: 1002;
      overflow: auto;
    }

    .white_content_small {
      display: none;
      position: absolute;
      top: 20%;
      left: 30%;
      width: 40%;
      height: 50%;
      border: 16px solid lightblue;
      background-color: white;
      z-index: 1002;
      overflow: auto;
    }

    .vote_btn {
      width: 100%;
      height: 100%;
      border: 1px;
      color: #FFFFFF;
      padding: 4px 16px;
      background-color: rgba(0, 0, 0, 0);
    }
  </style>
</head>

<body class="ignore">
<div style="display:flex;align-items:center" onclick="returnPage()">
  <img style="width: 30px; height:30px ; " src="https://hudong.hndt.com/h5/cdn/vote/img/icon-back.png">
  <span style="font-size: x-large;">返回</span>
</div>
<div class="g-hd">
  <a href="http://www.hndt.com/" class="link">
  </a>
</div>
<div class="g-banner"></div>
<!-- <div aspectratio w-16-9 class="img">
    <img aspectratio-content src="./img/banner.png" alt="">
</div> -->
<div class="g-m">
  <h2 class="title">活动简介</h2>
  <p class="m-bd" id="markdown"></p>
</div>
<div class="g-bd">
  <h2 class="title">参与者</h2>
  <ul class="list-wrap clearfix">
    <#list voteConfigTo.users as user>
      <li class="list">
        <div class="avatar-wrap">
          <a onclick="ShowDiv('MyDiv','fade',${user.id})">
            <#if user.photoUrl ?? >
              <img src="${user.photoUrl}" alt="${user.name}" class="avatar">
            <#else>
              <img src="https://hudong.hndt.com/h5/cdn/logo_300.png" alt="河南电台" class="avatar">
            </#if>
            <div hidden id="${user.id}">${user.brief}</div>
          </a>
        </div>
        <div class="text-wrap">
          <h3 class="name">${user.name}</h3>
          <div class="ticket-wrap">
            <span class="ticket-num">票数：${user.poll}</span>
          </div>
          <#if voteStatus>
             <div class="link" id="vote-item-${user.id}">
               <button type="submit" class="vote_btn" id="vote-item-${user.id}">投 票</button>
             </div>
          </#if>
        </div>
      </li>
    </#list>
  </ul>
</div>

<div class="g-download">
  <a href="http://www.hndt.com/app/download/index.html" class="down">下载河南广播APP</a>
  <p class="desc">关注身边发生的事 河南人自己的APP</p>
</div>


<div class="g-ft">
  <div class="m-version">
    <a href="http://www.hndt.com/index.mb.html">触摸版</a> |
    <a href="http://www.hndt.com/">电脑版</a> |
    <a href="http://www.hndt.com/index.mb.html">APP版</a>
  </div>
  <div class="m-copyright">
    手机河南广播网 ICP：豫ICP备14003020号
  </div>
</div>

<div class="g-ft-pc">
  <p class="para">Copyright © 2017 hndt Corporation,All Rights Reserved</p>
  <p class="para">河南广播网 版权所有</p>
</div>

<!--弹出层时背景层DIV-->
<div id="fade" class="black_overlay">
</div>

<div id="MyDiv" class="white_content">

  <div style="text-align: right; cursor: default; height: 40px;">
    <span style="font-size: 16px;" onclick="CloseDiv('MyDiv','fade')">关闭</span>
  </div>
  <p id="content"></p>
</div>
</body>

<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/marked/0.5.1/marked.js"></script>
<script src="https://cdn.bootcss.com/showdown/1.8.6/showdown.min.js"></script>
<script type="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.3.2.js"></script>
<script type="text/javascript">
  $(document).on("click", ".vote_btn", function () {
    var id = this.id;
    $.post({
      url: "../vote/${voteConfigTo.id}",
      contentType: "application/json;charset=utf-8",
      data: JSON.stringify({
        voteItemId: id,
        code: "${code}"
      }),
      success: function (result) {
        if (Object.prototype.toString.call(result) === "[object String]") {
          console.log(result);
          alert(result);
          window.location.reload();
        }
      },
      error: function (error) {
        if (Object.prototype.toString.call(error) === "[object String]") {
          console.error(error);
          alert(error);
        }
      }
    });
  });

  $(document).ready(function () {
    var html = '${voteConfigTo.content}';
    $("#markdown").html(html);
  });

  function ShowDiv(show_div, bg_div, userid) {
    document.getElementById(show_div).style.display = 'block';
    document.getElementById(bg_div).style.display = 'block';
    var bgdiv = document.getElementById(bg_div);
    bgdiv.style.width = document.body.scrollWidth; // bgdiv.style.height = $(document).height();
    $("#" + bg_div).height($(document).height());
    var brief = $("#" + userid).html();
    $("#content").html(brief);
  }

  //关闭弹出层
  function CloseDiv(show_div, bg_div) {
    document.getElementById(show_div).style.display = 'none';
    document.getElementById(bg_div).style.display = 'none';
  }

  //返回小程序页面
  function returnPage() {
    wx.miniProgram.navigateBack({});
    console.log("返回互动页面");
  }
</script>

</html>