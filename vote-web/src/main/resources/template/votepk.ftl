<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- 本地CSS文件 -->
  <link rel="stylesheet" href="http://hudong.hndt.com/h5/cdn/vote/css/votepk.css">
  <title>vote-pk</title>
</head>

<body>
<!--两个人PK-->
<div class="container">
  <div class="info-warpper">
    <!-- 用户信息面板  -->
    <div class="info-warpper-1">
      <div class="image-border">
        <img class="image1"
             src="http://www.linhuiyin.top:8888/group1/M00/00/00/rBAF7VvNkdqACaV4AAEswFMahqs728.jpg"
             alt="whq">
      </div>
      <div class="info">房主：吴欢庆</div>
      <button class="button-vote">投TA一票</button>
    </div>
    <div class="info-warpper-2">
      <div class="image-border">
        <img class="image1"
             src="http://www.linhuiyin.top:8888/group1/M00/00/00/rBAF7VvNkdqACaV4AAEswFMahqs728.jpg"
             alt="whq">
      </div>
      <div class="info">挑战者：吴欢庆</div>
      <button class="button-vote">投TA一票</button>
    </div>
  </div>
  <!-- pk进度条 -->
  <div class="progrss-warpper">
    <small class="rate-left">80%</small>
    <small class="rate-right">20%</small>
    <div class="progress progress-right">
      <!-- width为右边进度 -->
      <div class="progress progress-left" style="width: 80%;"></div>
    </div>
  </div>
  <div class="text-info">
    <small>下一位挑战者是谁？？</small>
  </div>
  <!-- 弹幕展示区 -->
  <section id="dm">
    <!-- 展板 -->
    <div class="dm_mask"></div>
    <!-- 内容 -->
    <div class="dm_show">
      <!-- <div>text message</div>  -->
    </div>
  </section>
  <!-- 弹幕发布区 -->
  <section id="promulgate">
    <input type="text" class="pro-text">
    <span class="pro-put">发送</span>
    <!-- <span class="pro-res">清屏</span> -->
  </section>
  <footer>河南广播云提供技术支持</footer>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdn.wilddog.com/js/client/current/wilddog.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script type="text/javascript">
  $(function () {
    // 创建一个野狗对象
    var ref = new Wilddog("https://chenwg-demo1.wilddogio.com/");
    var arr = [];
    //点击发送按钮将数据添加到野狗云
    $(".pro-put")
    .on("click", function () {
      var mess = $(".pro-text").val();
      ref.child("message").push(mess);
      $(".pro-text").val("");
    })
    //点击清空按钮将数据从野狗云和页面清除
    .next()
    .on("click", function () {
      ref.remove();
      arr = [];
      $(".dm_show").empty();
    });
    //按键点击回车执行发送操作
    $(".pro-text").on("keydown", function (e) {
      if (e.keyCode == 13) {
        $(".pro-put").trigger("click");
      }
    });
    //监听云端数据变更，云端数据变化，弹幕框里数据也跟着变化。
    ref.child("message").on("child_added", function (snapshot) {
      var text = snapshot.val();
      arr.push(text);
      var textObj = $('<div class="dm_mess">' + text + "</div>");
      $(".dm_show").append(textObj);
      moveObj(textObj);
    });

    ref.on("child_removed", function () {
      arr = [];
      $(".dm_show").empty();
    });

    //按照时间规则显示弹幕内容。
    //获取dm_mask对于当前窗口的相对偏移
    var topMin = $(".dm_mask").offset().top + 30;
    var topMax = topMin + $(".dm_mask").height() - 30;
    var _top = topMin;

    //定义弹幕移动
    var moveObj = function (obj) {
      // 设置目标位置
      var _left = $(".dm_mask").width() - obj.width();
      //高度不断往下不重叠
      _top = _top + 50;
      // 如果高度超出最底下-50范围则返回到顶部
      if (_top > topMax - 50) {
        _top = topMin;
      }
      //设置弹幕的css值
      obj.css({
        left: _left,
        top: _top,
        color: getRandomColor()
      });
      // 设置弹幕不同时间出现
      var time = 10000 + 10000 * Math.random();
      // 弹幕动画,动画完成则从dom删除
      obj.animate(
          {
            // left: "-" + _left + "px"
            left: 60 + "px"
          },
          time,
          function () {
            obj.remove();
          }
      );
    };

    //定义随机颜色函数
    var getRandomColor = function () {
      return (
          "#" +
          (function (h) {
            return new Array(7 - h.length).join("0") + h;
          })(((Math.random() * 0x1000000) << 0).toString(16))
      );
    };

    //每3000毫秒后开始随机抽取弹幕打上
    var getAndRun = function () {
      if (arr.length > 0) {
        var n = Math.floor(Math.random() * arr.length + 1) - 1;
        var textObj = $('<div class="dm_mess">' + arr[n] + "</div>");
        $(".dm_show").append(textObj);
        moveObj(textObj);
      }

      setTimeout(getAndRun, 3000);
    };

    jQuery.fx.interval = 50;
    getAndRun();
  });

</script>
</body>

</html>